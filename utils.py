## after tfort 23Q1, move this to cutverbs.utils

def flatten_paradigm(paradigm):
    flat = _flatten_paradigm(paradigm, {})
    return synthetic_only(flat)

def _flatten_paradigm(paradigm, accumulator, mom_features = ()):
    for f, content in paradigm.items():
        if f == 'meta':
            continue
        if type(f) == str:
            f = (f,)
        if type(content) == dict:
            _flatten_paradigm(content, accumulator, mom_features + f)
        else:
            if mom_features:
                f = mom_features + f
            accumulator[f] = content
    return accumulator

def synthetic_only(paradigm):
    return {ff: form for ff, form in paradigm.items() if len(form.split()) == 1}



# from paradig/utils.py
