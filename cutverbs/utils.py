
def walk(paradigm, f_above=None):
    if f_above is None: f_above = []
    for k, v in paradigm.items():
        if k == 'meta':
            continue
        elif type(v) == str or type(v) == list: # lists are used for items with multiple exponents
            yield k, v, paradigm, f_above
        else:
            yield from walk(v, f_above + [k]) # create a new list, dont change f_above, we still need it
