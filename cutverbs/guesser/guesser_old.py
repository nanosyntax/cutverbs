from paradig.guesser.paradigm_subtracting_guesser import guess_by_subtracting_known_paradigms


def guess_stem_and_suffixes(wparadigm, lg):
    return rl_or_lr(wparadigm, lg)  # liberal strategy, needs only one guesser to succeed
    #return rl_and_lr(wparadigm, lg) # restrictive strategy requiring both guessers to agree
    #return lr_only(wparadigm, lg)   # older way of guessing, exclusively based on stem-maximising

def rl_or_lr(wparadigm, lg):
    """This guessing strategy first try suffix-subtraction, and if that doesn't work, tries stem-maximisation"""
    closest_matches = []
    success, data = guess_by_subtracting_known_paradigms(wparadigm, lg)
    if success:
        existing_p, suppletion_type, stem, stems_pdgm, sparadigm = data
        # just return it, after reording the returns correctly?
    else:
    #if not existing_p:
        closest_matches = stems_pdgm
        stem, sparadigm = lg.guesser.guess_stem_and_suffixes(wparadigm)
        existing_p = find_existing_paradigm_for(stem, sparadigm, wparadigm, lg)
        if existing_p:
            print(f'    ************* RL failed! *********************')
    return existing_p, stem, sparadigm, closest_matches

def rl_and_lr(wparadigm, lg):
    """This guessing strategy requires both suffix-subtraction and stem-maximisation to agree"""
    closest_matches = []
    rl = guess_by_subtracting_known_paradigms(wparadigm, lg)
    if rl[2]:  stem_rl, stems_pdgm,      existing_p_rl, sparadigm = rl
    else:      stem_rl, closest_matches, existing_p_rl, sparadigm = rl
    stem_lr, sparadigm = lg.guesser.guess_stem_and_suffixes(wparadigm)
    existing_p_lr = find_existing_paradigm_for(stem_lr, sparadigm, wparadigm, lg)
    if existing_p_lr != existing_p_rl:
        print(f'  ***** rl={existing_p_rl} but lr={existing_p_lr}')
        return None
    if stem_lr != stem_rl:
        print(f'  ***** stem rl={stem_rl} but lr={stem_lr}')
        return None
    return existing_p_rl, stem_rl, sparadigm, closest_matches

def lr_only(wparadigm, lg):
    # older way of guessing, exclusively based on stem-maximising
    stem, sparadigm = lg.guesser.guess_stem_and_suffixes(wparadigm)
    existing_p = find_existing_paradigm_for(stem, sparadigm, wparadigm, lg)
    return existing_p, stem, sparadigm, []

### --------------

def find_existing_paradigm_for(stem, suffix_paradigm, word_paradigm, lg):
    existing_p = compare_to_existing_paradigms(suffix_paradigm, word_paradigm.category(), lg)
    if not existing_p:
        existing_p = which_paradigm_derives(stem, word_paradigm, lg)
    return existing_p

def which_paradigm_derives(stem, surface_forms, lg):
    for pname, suff_pdgm in lg.paradigms.items():
        if paradigm_derives(suff_pdgm, stem, surface_forms, lg):
            return pname
    return None

def paradigm_derives(suff_pdgm, stem, surface_forms, lg):
    def suff_derives_word(ff, word, suffix):
        return suffix is not None and lg.apply_morphophonology(stem, suffix, ff) == word
        #return suffix is not None and lg.apply_phonology(stem + suffix) == word
    return surface_forms.matches(suff_derives_word, suff_pdgm) 

def compare_to_existing_paradigms(new_paradigm, ncateg, lg):
    for paradigm_name, old_paradigm in lg.paradigms.items():
        if old_paradigm.category() == ncateg and are_dicts_same(old_paradigm, new_paradigm):
            return paradigm_name
    return False


def are_dicts_same(reference_pdgm, other_dict):
    reference_dict = reference_pdgm.forms # temp adjust for using old bare dicts
    if len(other_dict) - len(reference_dict) > 1:  return False
    for feature in reference_dict:
        if feature != 'meta' and reference_dict[feature] != other_dict[feature]:
            return False
    r_doublets = reference_pdgm.meta.get('doublets')
    o_doublets = other_dict.get('meta').get('doublets')
    return r_doublets == o_doublets
