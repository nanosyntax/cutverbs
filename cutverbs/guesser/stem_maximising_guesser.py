import copy

# quick port of Monika's guesser to a class & stripped of Cz-specific code
#   subclass it in various languages for lg-specific shortcuts

class StemMaximisingGuesser():
    def guess(self, word_paradigm):
        categ = word_paradigm.category()
        guessing_forms = self.guessing_forms(word_paradigm, categ)
        stem = self.guess_stem(guessing_forms)
        new_paradigm, stem, stem_alternations = self.guess_suffixes(word_paradigm, stem)
        stem = self.handle_stem_alternations(stem, stem_alternations)
        return stem, new_paradigm

    def guess_stem(self, word_list):
        """
        Takes list of chosen word forms of a declined noun
        and then returns a possible stem based on comparing these word forms.
        """
        i = self.find_first_diff_char(word_list)
        if i is None:
            return min(word_list) # min returns first str in alphabetical order
        return word_list[0][:i]

    def guess_suffixes(self, declined_word, stem):
        new_paradigm = {}
        stem_alternations = {}
        # def handle_entry(_ff, suffix, word):
        #     seen[0] += 1   # ???? assigning directly to seen complains about uninitialized var, but assigning into it doesnt...
        #     stem = remove_suffix(word, suffix) if word else None
        #     if stem is not None:
        #         stems[stem] += 1
        #     return stem
        # stem_paradigm = sparadigm.transform_with(handle_entry, wparadigm)

        for feature, word_form in declined_word.forms.items():
            guessed_suffix = self.guess_suffix(word_form, stem, stem_alternations)
            if guessed_suffix is not None: # None when an optional form is absent
                new_paradigm[feature] = guessed_suffix
        #doublets = getattr(declined_word, 'doublets', []) # hasattr

        meta = new_paradigm['meta'] = copy.deepcopy(declined_word.meta) # word_form = meta dict
        doublets = meta.get('doublets')
        if doublets:
            for ffs, dlist in doublets.items():
                if type(dlist) == list:
                    doublets[ffs] = [self.guess_suffix(w, stem, stem_alternations) for w in dlist]
                else:
                    doublets[ffs] = self.guess_suffix(dlist, stem, stem_alternations)

        return new_paradigm, stem, stem_alternations

    def handle_stem_alternations(self, stem, stem_alternations):
        return stem

    def guess_suffix(self, word, stem, stem_alternations):
        suffix_candidate = remove_prefix(word, stem)
        if suffix_candidate == word:
            suffix_candidate, stem, stem_alternations = self.guess_suffix_irregular(word, stem, stem_alternations)
        return suffix_candidate

    def guess_suffix_irregular(self, word, stem, stem_alternations):
        pass # implement in lg-specific sublcasses


    def guessing_forms(self, wparadigm, categ):
        return wparadigm.keys()

    def find_first_diff_char(self, word_list):
        """
        Returns index of the first character in which word froms differ. 
        If there are no differences between the shortest one
        and coresponding parts of other strings, return value is None.
        """
        min_length = min(len(word_form) for word_form in word_list)
        for i in range(min_length):
            current_char = word_list[0][i]
            for word_form in word_list[1:]:              
                if word_form[i] != current_char:
                    return i
        return None


def remove_prefix(word, prefix): # removeprefix requires python 3.9 -- in the meantime use this...
    if word.startswith(prefix):
        return word[len(prefix):]
    return word

class WordParadigm():
    def __init__(self, word_list):
        self.word_list = word_list
    
    def category(self):
        return "V"


if __name__ == '__main__':
    guesser = StemMaximisingGuesser()
    test_paradigm = WordParadigm({('1', 'sg'): 'jdu', ('1', 'pl'): 'jdeme', ('2', 'sg'): 'jdeš', ('2', 'pl'): 'jdete', ('3', 'sg'): 'jde', ('3', 'pl'): 'jdou'})
    guesser.guess(test_paradigm)