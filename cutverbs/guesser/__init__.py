# copied from /guesser.py

class Guesser():

    def guess_stem_and_paradigm(self, conjugated_verb, paradigms, lg):
        candidates = []
        for paradigm_name, paradigm in paradigms.items():
            if stem := self.belongs_to_paradigm(conjugated_verb, paradigm, lg):
                candidates.append( (stem, paradigm_name) )
        if len(candidates) == 0:
            return None, None
        if len(candidates) == 1:
            return candidates[0]
        # multiple possible paradigms: choose the one with the shortest stem
        stems = [stem for stem, _ in candidates]
        shortest_stem = min(stems, key = lambda s: len(s))
        # double-check for the unlikely case that multiple paradigms use that shortest stem
        winners = [(stem, paradigm_name) for stem, paradigm_name in candidates if stem == shortest_stem]
        if len(winners) > 1:
            raise ValueError('Unknown case, please examine and fix')
        return winners[0]


    def guess_paradigm(self, conjugated_verb, paradigms, lg):
        return self.guess_stem_and_paradigm(conjugated_verb, paradigms, lg)[1]


    # -- private

    def belongs_to_paradigm(self, conjugated_verb, paradigm, lg):
        roots_dict = {}
        for features, verb_ipa in conjugated_verb.items():
            suffix_ipa = paradigm[features]
            if not verb_ipa.endswith(suffix_ipa):
                return(False)
            roots_dict[features] = verb_ipa[:-len(suffix_ipa)]
        roots = set(roots_dict.values())
        if len(roots) == 1:
            return roots.pop()
        for root in roots:
            if self.can_root_derive_paradigm(root, roots_dict, paradigm, conjugated_verb, lg):
                return root # TODO: cases where more than one root can derive the paradigm?
        return False # len(roots) == 1

    def can_root_derive_paradigm(self, root, roots_dict, suffix_dict, surface_dict, lg):
        for features, verb_ipa in surface_dict.items():
            if roots_dict[features] == root:
                continue
            suffix = suffix_dict[features]
            if not lg.apply_phonology(root, suffix) == verb_ipa:
                return False
        return True
