import re, copy

from cutverbs.guesser.stem_maximising_guesser import StemMaximisingGuesser
from cutverbs.guesser.utils import dict_keys_to_regex
from cutverbs.guesser.utils import to_voiceless_dict, to_voiced_dict, palatalized_by_j, palatalized_by_je_only



# TODO:
# 1. Rename functions, clean up
# 2. Add possibility to deal with multiple irregularities in 1 word
# 3. Rewrite e alternation in some better way
# 4. Deal with assimilation (like 'dívka')



### -------- main functions

def guess_stem_and_suffixes(word_paradigm):
    guessing_forms = select_guessing_forms(word_paradigm)
    stem = guess_stem(guessing_forms)
    new_paradigm, stem, stem_alternations = guess_suffixes(word_paradigm, stem)
   
    if stem_alternations.get('e_alternation', False):
        e_index = stem_alternations['e_alternation']
        stem = insert_є(stem, e_index)

    return new_paradigm, stem

def find_existing_paradigm_for(stem, suffix_paradigm, word_paradigm, lg):
    existing_p = compare_to_existing(suffix_paradigm, lg)
    if not existing_p:
        existing_p = which_paradigm_derives(stem, word_paradigm, lg)
    return existing_p


### --------------

def which_paradigm_derives(stem, surface_forms, lg):
    for pname, suff_pdgm in lg.paradigms.items():
        if paradigm_derives(suff_pdgm, stem, surface_forms, lg):
            return pname
    return None

def paradigm_derives(suff_pdgm, stem, surface_forms, lg):
    # quick check: if the length of the paradigms are different, they won't fit.
    # allow a length difference of 1, in case one has a meta key and the other doesn't
    if abs(len(surface_forms) - len(suff_pdgm)) > 1:
        return False
    # for each suffix in suff_pdgm, check that stem+suffix creates the correct surface form in surface_forms
    for features, suffix in suff_pdgm.items():
        if features != 'meta':
            word = lg.apply_phonology(stem + suffix)
            if not surface_forms[features] == word:
                return False
    return True

def compare_to_existing(new_paradigm, language_obj, mode='noun'):
    for paradigm_name in language_obj.paradigms.keys():
        if paradigm_name[0] == mode[0]:
            old_paradigm = language_obj.paradigms[paradigm_name]
            if are_dicts_same(old_paradigm, new_paradigm):
                return paradigm_name
    return False


### --------------

def select_guessing_forms(word_paradigm):
    feature_list = [['finite', ('1', 'sg')], ['finite', ('3', 'pl')], 'infinitive']
    #if word_class == 'verb':
        #declined_word = flatten_paradigm(declined_word)
    return select_from_dict(word_paradigm, feature_list)


def guess_stem(word_form_list):
    """
    Takes list of chosen word forms of a declined noun
    and then returns a possible stem based on comparing these word forms.
    """
    i = find_first_diff_char(word_form_list, ignore_length=True)

    if i is None:
        return min(word_form_list) # min returns first str in alphabetical order

    words_with_length = []
    for word_form in word_form_list:
        if ':' in word_form[:i+1]:
            words_with_length.append(word_form)

    if words_with_length:
        stem = words_with_length[0][:i+1]
    else: 
        stem = word_form_list[0][:i]    
    return stem

def guess_suffixes(declined_word, stem):
    new_paradigm = {}
    stem_alternations = {}
    for feature, word_form in declined_word.items():
        if feature == 'meta':
            meta = new_paradigm['meta'] = copy.deepcopy(word_form) # word_form = meta dict
            doublets = meta.get('doublets')
            if doublets:
                for ffs, dlist in doublets.items():
                    doublets[ffs] = [guess_suffix(w, stem, stem_alternations) for w in dlist]
        elif feature == 'derivation':
            pass
        else:
            if type(word_form) != str:
                new_paradigm[feature] = guess_suffixes(word_form, stem)[0]
            else:
                new_paradigm[feature] = guess_suffix(word_form, stem, stem_alternations)
    return new_paradigm, stem, stem_alternations

def guess_suffix(word, stem, stem_alternations):
    suffix_candidate = remove_prefix(word, stem)
    if suffix_candidate == word:
        suffix_candidate, stem, stem_alternations = guess_suffix_irregular(word, stem, stem_alternations)
    elif suffix_candidate == 'i:x':
        suffix_candidate = 'ji:x'                
    return suffix_candidate

def guess_suffix_irregular(word_form, stem, stem_alternations):
    #deal with multiple irregularities
    suffix_candidate = word_form

    first_diff_char = find_first_diff_char((word_form, stem))

    if first_diff_char == None:
        # deal with type téma (all cases except nom sg have extra t)
        return '', word_form, stem_alternations

    if word_form[first_diff_char] == 'e':
        # deal with e alternation
        stem_alternations['e_alternation'] = first_diff_char
        return '', stem, stem_alternations  # not entirely clear whether to return or let flow into the phono tests

    if stem[first_diff_char] == ':':
        # vowel shortening - pan / altering vowel length 
        # for cases when stem has long vowel but the word form not   
        word_form = word_form[:first_diff_char] + ':' + word_form[first_diff_char:]
        suffix_candidate = remove_prefix(word_form, stem)

    if is_palatalized_by_i(word_form):
        suffix_candidate = 'ji:x'

    elif is_palatalized_by_e(word_form, stem):
        suffix_candidate = 'je'

    elif is_palatalized_by_ᐞe(word_form, stem):
        suffix_candidate = 'ᐞe'

    elif word_form_has_final_devoicing(word_form, stem):
        # deal with final unvoicing
        suffix_candidate = ''       
    
    return suffix_candidate, stem, stem_alternations



#### ---------- Czech phonological tests


def word_form_has_final_devoicing(word_form, stem):
    unvoiced_regex = dict_keys_to_regex(to_voiced_dict)
    voiced_regex = dict_keys_to_regex(to_voiceless_dict)
    word_form_ending = re.search(f'({unvoiced_regex})$', word_form)
    stem_ending =  re.search(f'({voiced_regex})$', stem)
    if word_form_ending and stem_ending and to_voiced_dict[word_form_ending.group(1)] == stem_ending.group(1) :
        return True
    return False

def is_palatalized_by_i(word_form):
    return len(word_form) >= 3 and word_form[-3:] == 'i:x'

def is_palatalized_by_e(word_form, stem):
    if word_form[-1:] != 'e':
        return False
    
    palatalized_list = []
    not_palatalized_list = []

    for not_palatalized, palatalized in palatalized_by_j.items():
        not_palatalized_list.append(not_palatalized[0])
        palatalized_list.append(palatalized)
    for not_palatalized, palatalized in palatalized_by_je_only.items():
        if palatalized[-1] == 'e':
            not_palatalized_list.append(not_palatalized[0])
            palatalized_list.append(palatalized)

    word_form_ending = re.search( '(' + '|'.join(palatalized_list) + ')' + 'e?' + '$', word_form)
    stem_ending =  re.search('(' + '|'.join(not_palatalized_list) + ')$', stem)   

    if word_form_ending and stem_ending:
        word_form_ending = word_form_ending.group(1)
        stem_ending = stem_ending.group(1)
        if palatalized_by_j.get(stem_ending+'j', False) == word_form_ending or palatalized_by_je_only.get(stem_ending+'je', False) == word_form_ending: 
            return True

    return False

def is_palatalized_by_ᐞe(word_form, stem):
    if re.search('tše$', word_form) and re.search('ts$', stem):    
        # in reality, stem should end with єts, but guesser doesn't know at this point about є alternation
        return True
    return False


#def is_palatalized_by_e(word_form):

def insert_є(stem, e_index):
    stem = stem[:e_index] + 'є' + stem[e_index:]
    return stem


#### ------------- helpers

def find_first_diff_char(word_form_list, ignore_length=False):
    """
    Returns index of the first character in which word froms differ. 
    If there are no differences between the shortest one
    and coresponding parts of other strings, return value is None.
    """

    if ignore_length:
        word_forms_shortened = []
        for i, word_form in enumerate(word_form_list):
            word_forms_shortened.append(re.sub(':', '', word_form))

        min_length = min(len(word_form) for word_form in word_forms_shortened) #keyerrorhere for verbs 

        for i in range(min_length):
            current_char = word_forms_shortened[0][i]
            for word_form in word_forms_shortened[1:]:              
                if word_form[i] != current_char:
                    return i

    else:
        min_length = min(len(word_form) for word_form in word_form_list) #keyerrorhere for verbs 
        for i in range(min_length):
            current_char = word_form_list[0][i]
            for word_form in word_form_list[1:]:              
                if word_form[i] != current_char:
                    return i

    return None


def select_from_dict(dictionary, feature_list):
    new_list = []
    for feature in feature_list:
        if type(feature) == list:
            nested_dict = dictionary
            for level in feature[:-1]:
                nested_dict = nested_dict[level]
            new_list.append(nested_dict[feature[-1]])

    else:
        if dictionary[feature] != '':
                new_list.append(dictionary[feature])

    return new_list

def are_dicts_same(reference_dict, other_dict):
    for feature in reference_dict:
        if feature != 'meta' and reference_dict[feature] != other_dict[feature]:
            return False
    return True

def remove_prefix(word, prefix): # removeprefix requires python 3.9 -- in the meantime use this...
    if word.startswith(prefix):
        return word[len(prefix):]
    return word

def remove_suffix(word, suffix): # removesuffix requires python 3.9 -- use it when everybody will have upgraded
    if word.endswith(suffix):
        return word[:-len(suffix)]
    return word


test = {'infinitive': 'fascinovat', 'meta': {'category': 'v'}, ('finite', '1', 'sg'): 'fascinuji', ('finite', '1', 'pl'): 'fascinujeme', ('finite', '2', 'sg'): 'fascinuješ', ('finite', '2', 'pl'): 'fascinujete', ('finite', '3', 'sg'): 'fascinuje', ('finite', '3', 'pl'): 'fascinují'}
test2 = {'infinitive': 'jít', 'meta': {'category': 'v', 'aspect': 'imperfective'}, ('finite', '1', 'sg'): 'jdu', ('finite', '1', 'pl'): 'jdeme', ('finite', '2', 'sg'): 'jdeš', ('finite', '2', 'pl'): 'jdete', ('finite', '3', 'sg'): 'jde', ('finite', '3', 'pl'): 'jdou'}
test3 = {'meta': {'category': 'v'}, 'infinitive': 'abstrahovat', 'participle': 'abstrahoval', 'passive': 'abstrahován', 'finite': {('1', 'sg'): 'abstrahuju', ('2', 'sg'): 'abstrahuješ', ('3', 'sg'): 'abstrahuje', ('1', 'pl'): 'abstrahujeme', ('2', 'pl'): 'abstrahujete', ('3', 'pl'): 'abstrahujou'}}
test4 = {'infinitive': 'balit', 'meta': {'category': 'v', 'aspect': 'imperfective'}, 'finite': {('1', 'sg'): 'balím', ('1', 'pl'): 'balíme', ('2', 'sg'): 'balíš', ('2', 'pl'): 'balíte', ('3', 'sg'): 'balí', ('3', 'pl'): 'balí'}, 'imperative': {('1', 'pl'): 'balme', ('2', 'sg'): 'bal', ('2', 'pl'): 'balte'}, 'participle': {'past': {('masc', 'sg'): 'balil', ('masc', 'anim', 'pl'): 'balili', ('fem', 'sg'): 'balila', ('fem', 'pl'): 'balily', ('neut', 'sg'): 'balilo'}, 'passive': {('masc', 'sg'): 'balen', ('masc', 'anim', 'pl'): 'baleni', ('fem', 'sg'): 'balena', ('fem', 'pl'): 'baleny', ('neut', 'sg'): 'baleno'}}, 'converb': {'present': {('masc', 'sg'): 'bale', ('fem', 'sg'): 'balíc', ('neut', 'sg'): 'balíc', ('pl', 'ipfv'): 'balíce'}, 'past': {}}, 'derivation': {'finite': {('1', 'sg'): ['balit', 'm'], ('1', 'pl'): ['balit', 'íme'], ('2', 'sg'): ['balit', 'íš'], ('2', 'pl'): ['balit', 'íte'], ('3', 'sg'): ['balit', 'í'], ('3', 'pl'): ['balit', 'í']}, 'imperative': {('1', 'pl'): ['-'], ('2', 'sg'): ['-'], ('2', 'pl'): ['-']}, 'participle': {'past': {('masc', 'sg'): ['balit', 'l'], ('masc', 'anim', 'pl'): ['balit', 'l', 'i'], ('fem', 'sg'): ['balit', 'l', 'a'], ('fem', 'pl'): ['balit', 'l', 'y'], ('neut', 'sg'): ['balit', 'l', 'o']}, 'passive': {('masc', 'sg'): ['balit', 'n'], ('masc', 'anim', 'pl'): ['balit', 'n', 'i'], ('fem', 'sg'): ['balit', 'n', 'a'], ('fem', 'pl'): ['balit', 'n', 'y'], ('neut', 'sg'): ['balit', 'n', 'o']}}, 'converb': {'present': {('masc', 'sg'): ['-'], ('fem', 'sg'): ['-'], ('neut', 'sg'): ['-'], ('pl', 'ipfv'): ['-']}, 'past': {}}}}
if __name__ == '__main__':
    #test = guess_stem_and_suffixes(input_dict['dělat'], guessing_features)
    print(guess_stem_and_suffixes(test4))