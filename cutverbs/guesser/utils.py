import re

def dict_keys_to_regex(dictionary):
    """ 
    Create regex that matches any string that is used as a key in the given dictionary.
    E.g.  {'a': 'x', 'b': 'y'} -> 'a|b'
    """
    regex = '|'.join(list(dictionary.keys()))
    return regex

to_voiceless_dict = {'b': 'p', 
                    'd': 't', 
                    'g': 'k', 
                    'v': 'f', 
                    'z': 's', 
                    'dj': 'tj', 
                    'ž': 'š', 
                    'h': 'x',
                    } 

palatalized_by_j ={'gj': 'z',       
                    'kj': 'ts',
                    'xj': 'š',
                    'hj': 'z',
                    'rj': 'ř'
                    } 

palatalized_by_je_only = {   # to add 'tji': 'ti' ?              
                    'mje': 'mnje',
                    'mji': 'mi',
                    }

to_voiced_dict = {voiceless: voiced for voiced,voiceless in to_voiceless_dict.items()}

