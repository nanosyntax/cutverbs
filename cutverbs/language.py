import ast, importlib
from pathlib import Path

from scraper.creator import scraper_named
from diastring import DiaString

class Language():

    def to_ipa(self, word):
        ipa = self.ipa.to_ipa(word)
        return self.phono.apply_internal_phono(ipa)

    def paradigm_to_ipa(self, paradigm):
        return {features: self.to_ipa(ortho) for features, ortho in paradigm.items()}

    def apply_phonology(self, left, right, features = None):
        return self.phono.apply_edge_phono(left, right, features)
    
    def guess_paradigm(self, lemma_or_wparadigm, categ = None):
        return self.guess_stem_and_paradigm(lemma_or_wparadigm, categ)[1]
    
    def guess_stem_and_paradigm(self, lemma_or_wparadigm, categ = None):
        if type(lemma_or_wparadigm) == str:
            wparadigm = self.paradigm_for(lemma_or_wparadigm, categ)
        else:
            wparadigm = lemma_or_wparadigm
        return self.guesser.guess_stem_and_paradigm(wparadigm, self.paradigms, self)

    def verbs(self, conjugation=None, stems_end_with=None):
        all_verbs = Path(f'data/paradigms/{self.code3}/all').iterdir()
        all_verbs = [pdgm for p in all_verbs if (pdgm := self.paradigm_for(p.name, 'v'))]
        categorized = []
        for v in all_verbs:
            stem, pdgm = self.guess_stem_and_paradigm(v)
            if stem:
                categorized.append( {'stem': stem, 'pname': pdgm, 'finite': v} )
            else:
                categorized.append( {'stem': '*****', 'pname': 'uncategorized', 'finite': v} )

        if conjugation:
            all_verbs = [v for v in categorized if v['pname'] == conjugation]
        if stems_end_with:
            all_verbs = [v for v in all_verbs if (v['stem']).endswith(stems_end_with)]
        if conjugation is None and stems_end_with is None:
            by_categ = {}
            for v in categorized:
                categ = v['pname']
                if members := by_categ.get(categ):
                    members.append(v)
                else: by_categ[categ] = [v]
        return all_verbs

    def paradigm_for(self, lemma, categ = None):
        data = self.cached_paradigm_for(lemma, categ)
        if not data:
            return None
        # adjust the paradigm, if needed:
        wparadigm = data['finite'] # TODO: generalise to all tenses
        wparadigm = self.ipa.pre_paradigm_for(wparadigm)
        wparadigm = self.paradigm_to_ipa(wparadigm)   
        wparadigm = self.ipa.post_paradigm_for(wparadigm)
        return wparadigm

    def cached_paradigm_for(self, lemma, categ = None):
        # duplicated from Scraper.cached_paradigm(). TODO: unduplicate.
        file_path = Path(f'data/paradigms/{self.code3}/manual/{lemma}')
        if not file_path.is_file():
            file_path = Path(f'data/paradigms/{self.code3}/all/{lemma}')
            if not file_path.is_file():
                return None
        text = file_path.read_text('utf-8')
        data = ast.literal_eval(text)
        if type(data) == list:
            if categ:   hit = [d for d in data if d['meta'].get('categ') == categ]
            else:       hit = None
            if not hit or len(hit) > 1:
                raise ValueError(f'Multiple meanings for {lemma}, cannot find the right one')
            data = hit[0]
        return data

    # def apply_morphophonology(self, left, right, features):
    #     return self.morpho.apply_morphophonology(left, right, features)
    
    # def features_for_category(self, category):
    #     ffs = self.cache['features'].get(category)
    #     if ffs:
    #         return ffs
    #     paradigm = next(filter(lambda kv: kv[0][:1] == category, self.paradigms.items()))[1] # find pdgm of category
    #     ffs = self.cache['features'][category] = paradigm.features() # remember the features for next time
    #     return ffs

    def scraper(self):
        if not self._scraper:
            self.set_scraper_named(self.preferences['scraper'])
        return self._scraper

    def set_scraper(self, scraper):     self._scraper = scraper
    def set_scraper_named(self, name):  self.set_scraper(scraper_named(name))


    # def seeder(self):
    #     return scraper_named(self.preferences['seeder'])


    # Unused?
    # adapted from tests/utils, for cz
    def get_and_guess_paradigm(self, lemma):    # similar to get_declined_forms in paradig
        ipa_paradigm = self.get_surface_paradigm(lemma)
        return self.guess_paradigm(ipa_paradigm)

    def get_surface_paradigm(self, lemma):
        ortho_paradigm = self.scraper().get_paradigm(lemma)
        finite_ortho_paradigm = ortho_paradigm['finite']
        return self.paradigm_to_ipa(finite_ortho_paradigm)

    def batch_get_paradigms(self, lemmas):
        ortho_paradigms = self.scraper().get_paradigms(lemmas)
        ipa_paradigms = {paradigm_name: self.paradigm_to_ipa(full_paradigm['finite']) for paradigm_name, full_paradigm in ortho_paradigms.items()}
        return ipa_paradigms

    def guess_one_verb_sk(self, one_sk_com_paradigm, lg):
        finite_paradigm = one_sk_com_paradigm['finite']
        ipa_paradigm = {}
        for features, morphemes in finite_paradigm.items():
            dictcom_word = ''.join(morphemes)
            ipa_word = lg.to_ipa(dictcom_word)
            ipa_paradigm[features] = ipa_word
        guessed_paradigm = self.guess_paradigm(ipa_paradigm, lg)
        return guessed_paradigm


    ## Loading and initializing the various components of a language

    def __init__(self, lg_name):
        p = Path('data')  # or: Path(__file__).parent.parent / 'data'
        self.init_lg_codes(lg_name)
        self.preferences = self.load_preferences(self.code, p)
        self.paradigms   = self.load_paradigms(self.code, p)
        # self.stems       = self.load_stems(lg_name, p)
        self.phono       = self.load_phonology(self.code)
        # self.morpho      = self.load_morphology(lg_name)
        self.ipa         = self.load_ipa(self.code)
        self.guesser     = self.load_guesser(self.code)
        self._scraper    = None  # load on-demand, see scraper()
        # self.cache       = {'features': {}}
        # self.wrap_paradigms()

    def init_lg_codes(self, code):
        two, three = both_iso_639(code)
        self.code  = two
        self.code3 = three

    def load_preferences(self, lg_name, data_dir):
        return read_file_dict(data_dir / 'preferences' / f'{lg_name}.py') or {}

    def load_paradigms(self, lg_name, data_dir):
        # return dict_from_files_in(data_dir / 'paradigms' / lg_name)
        module_name = f'suffixes.{lg_name}.verbs'
        module = importlib.import_module(module_name)
        return _convert_to_dia(module.paradigms)
        # return dict_from_files_in(Path('suffixes') / lg_name) #  / 'verbs.py'

    # def load_stems(self, lg_name, data_dir):
    #     return dict_from_files_in(data_dir / 'stems' / lg_name)

    def load_phonology(self, lg_name):
        # ## temp during seminar
        # try:
        #     return importlib.import_module(f'phono.{lg_name}.phono')
        # except:
        #     return importlib.import_module(f'phono.{lg_name}')
        return importlib.import_module(f'phono.{lg_name}.phono')

    # def load_morphology(self, lg_name):
    #     module_name = f'morpho.{lg_name}.morpho'
    #     return importlib.import_module(module_name)

    def load_ipa(self, lg_name):
        module_name = f'ipa.{lg_name}'
        module = importlib.import_module(module_name)
        cls = getattr(module, 'IPA')
        return cls() # module
        # to return a class:
        # module = importlib.import_module(module_name)
        # cls = getattr(module, 'my_class')

    def load_guesser(self, lg_name):
        module_name = f'cutverbs.guesser.{lg_name}.guesser'
        try:
            return importlib.import_module(module_name)
        except:
            module = importlib.import_module(f'cutverbs.guesser')
            cls = getattr(module, 'Guesser')
            return cls()

    # def wrap_paradigms(self):
    #     pprefs = self.preferences.get('paradigm_type')
    #     self.paradigms = {pname: Paradigm.forms(data, pprefs) for pname, data in self.paradigms.items()}





def dict_from_files_in(dir_path): # each file in dir is a dictionary, create a unified dictionary grouping the content of each file  
    all = {}
    for p in dir_path.iterdir():
        if p != dir_path / '__pycache__':
            cur = read_file_dict(p)
            all.update(cur)
    return all

def read_file_dict(path):
    try:
        with open(path, 'r', encoding='utf-8' ) as f:
            s = f.read()
            return ast.literal_eval(s)
    except:
        return None


# https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
lg_code_2_to_3 = {
    'cz': 'cze',
    'sk': 'svk',
    'ua': 'ukr',
}

lg_code_3_to_2 = {}
for two, three in lg_code_2_to_3.items():
    lg_code_3_to_2[three] = two

def both_iso_639(code):
    if other := lg_code_2_to_3.get(code):
        return( (code, other) )
    elif other := lg_code_3_to_2.get(code):
        return( (other, code) )
    # lets let it return None and crash


def _convert_to_dia(paradigm):
    for k, v in paradigm.items():
        if k == 'meta':
            continue
        elif type(v) == str:
            paradigm[k] = DiaString(v)
        elif type(v) == dict:
            paradigm[k] = _convert_to_dia(v)
    return paradigm
