from phono.cz.phono import apply_phonology

def guess_paradigm(verb, paradigms):
    raise ValueError(' **** old code, please now use lg.guess_paradigm')
    candidates = []
    for paradigm_name, paradigm in paradigms.items():
        if stem := belongs_to_paradigm(verb, paradigm):
            candidates.append( (stem, paradigm_name) )
    if len(candidates) == 0:
        return None
    if len(candidates) == 1:
        return candidates[0][1] # paradigm_name
    # multiple possible paradigms: choose the one with the shortest stem
    stems = [stem for stem, _ in candidates]
    shortest_stem = min(stems, key = lambda s: len(s))
    # double-check for the unlikely case that multiple paradigms use that shortest stem
    winners = [paradigm_name for stem, paradigm_name in candidates if stem == shortest_stem]
    if len(winners) > 1:
        raise ValueError('Unknown case, please examine and fix')
    return winners[0]


# -- private

def belongs_to_paradigm(conjugated_verb, paradigm):
    return belongs_to_paradigm_next(conjugated_verb, paradigm)


def belongs_to_paradigm_prev(conjugated_verb, paradigm):
    roots = set()
    for features, verb_ipa in conjugated_verb.items():
        suffix_ipa = paradigm[features]
        if not verb_ipa.endswith(suffix_ipa):
            return(False)
        root = verb_ipa[:-len(suffix_ipa)]
        roots.add(root)
    return len(roots) == 1

def belongs_to_paradigm_next(conjugated_verb, paradigm):
    roots_dict = {}
    for features, verb_ipa in conjugated_verb.items():
        suffix_ipa = paradigm[features]
        if not verb_ipa.endswith(suffix_ipa):
            return(False)
        roots_dict[features] = verb_ipa[:-len(suffix_ipa)]
    roots = set(roots_dict.values())
    if len(roots) == 1:
        return roots.pop()
    for root in roots:
        if can_root_derive_paradigm(root, roots_dict, paradigm, conjugated_verb):
            return root # TODO: cases where more than one root can derive the paradigm?
    return False # len(roots) == 1

def can_root_derive_paradigm(root, roots_dict, suffix_dict, surface_dict):
    for features, verb_ipa in surface_dict.items():
        if roots_dict[features] == root:
            continue
        suffix = suffix_dict[features]
        if not apply_phonology(root, suffix) == verb_ipa:
            return False
    return True
