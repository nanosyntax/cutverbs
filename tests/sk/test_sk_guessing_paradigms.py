from tests.data.sk.verbs   import a_sffx_length, a_root_length, a_root_length_pal, a_va_sffx, i_sffx_length, i_root_length, e_uj_sffx, e_n_sffx_length, e_n_root_length, e_no_length, e_no_length_root_final_n, e_no_length_root_final_vowel, e_no_length_pal, e_root_length, e_root_length_pal, e_3pl_preserved, e_3pl_preserved_pal, irregular, two_paradigms, problematic

from cutverbs.language import Language

sk = Language('sk')

def test_a_sffx_length():
    ipa_paradigms = sk.batch_get_paradigms(a_sffx_length.keys())
    for verb, expected_paradigm in a_sffx_length.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_a_root_length_pal():
    ipa_paradigms = sk.batch_get_paradigms(a_root_length_pal.keys())
    for verb, expected_paradigm in a_root_length_pal.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_a_root_length():
    ipa_paradigms = sk.batch_get_paradigms(a_root_length.keys())
    for verb, expected_paradigm in a_root_length.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_a_va_sffx():
    ipa_paradigms = sk.batch_get_paradigms(a_va_sffx.keys())
    for verb, expected_paradigm in a_va_sffx.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_i_sffx_length():
    ipa_paradigms = sk.batch_get_paradigms(i_sffx_length.keys())
    for verb, expected_paradigm in i_sffx_length.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_i_root_length():
    ipa_paradigms = sk.batch_get_paradigms(i_root_length.keys())
    for verb, expected_paradigm in i_root_length.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_e_uj_sffx():
    ipa_paradigms = sk.batch_get_paradigms(e_uj_sffx.keys())
    for verb, expected_paradigm in e_uj_sffx.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_e_n_sffx_length():
    ipa_paradigms = sk.batch_get_paradigms(e_n_sffx_length.keys())
    for verb, expected_paradigm in e_n_sffx_length.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_e_n_root_length():
    ipa_paradigms = sk.batch_get_paradigms(e_n_root_length.keys())
    for verb, expected_paradigm in e_n_root_length.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_e_no_length():
    ipa_paradigms = sk.batch_get_paradigms(e_no_length.keys())
    for verb, expected_paradigm in e_no_length.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_e_no_length_root_final_n():
    ipa_paradigms = sk.batch_get_paradigms(e_no_length_root_final_n.keys())
    for verb, expected_paradigm in e_no_length_root_final_n.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_e_no_length_root_final_vowel():
    ipa_paradigms = sk.batch_get_paradigms(e_no_length_root_final_vowel.keys())
    for verb, expected_paradigm in e_no_length_root_final_vowel.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_e_no_length_pal():
    ipa_paradigms = sk.batch_get_paradigms(e_no_length_pal.keys())
    for verb, expected_paradigm in e_no_length_pal.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_e_root_length():
    ipa_paradigms = sk.batch_get_paradigms(e_root_length.keys())
    for verb, expected_paradigm in e_root_length.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb    

def test_e_root_length_pal():
    ipa_paradigms = sk.batch_get_paradigms(e_root_length_pal.keys())
    for verb, expected_paradigm in e_root_length_pal.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb    

def test_e_3pl_preserved():
    ipa_paradigms = sk.batch_get_paradigms(e_3pl_preserved.keys())
    for verb, expected_paradigm in e_3pl_preserved.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb    

def test_e_3pl_preserved_pal():
    ipa_paradigms = sk.batch_get_paradigms(e_3pl_preserved_pal.keys())
    for verb, expected_paradigm in e_3pl_preserved_pal.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb 

def test_irregular():
    ipa_paradigms = sk.batch_get_paradigms(irregular.keys())
    for verb, expected_paradigm in irregular.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_two_paradigms():
    ipa_paradigms = sk.batch_get_paradigms(two_paradigms.keys())
    for verb, expected_paradigm in two_paradigms.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb

def test_problematic():
    ipa_paradigms = sk.batch_get_paradigms(problematic.keys())
    for verb, expected_paradigm in problematic.items():
        guessed_paradigm = sk.guess_paradigm(ipa_paradigms[verb])
        assert guessed_paradigm == expected_paradigm, verb