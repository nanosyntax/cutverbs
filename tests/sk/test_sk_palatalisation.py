from phono.sk.phono import apply_edge_phono
from diastring import DiaString

def do_edge_phono(left, right):
    right = DiaString(right)
    return apply_edge_phono(left, right)

def test_palatalisation():
    assert do_edge_phono('d', '^') == 'dj' # uškodji:m
    assert do_edge_phono('š', '^a') == 'šja' # vešja:m
    assert do_edge_phono('š', '^i') == 'ši' # tuši:m
    assert do_edge_phono('r', '^e') == 'rje' # vrijem
    assert do_edge_phono('ts', '^a') == 'tsja' # grtsjam
    assert do_edge_phono('s', '^e') == 'sje' # njesjem, pasjem
    assert do_edge_phono('dz', '^a') == 'dzja' # zavadzjam
    assert do_edge_phono('z', '^e') == 'zje' # vezjiem
    assert do_edge_phono('v', '^a') == 'vja' # stavja:m
    assert do_edge_phono('l', '^e') == 'lje' # meljem
    assert do_edge_phono('ž', '^e') == 'žje' # strežjem