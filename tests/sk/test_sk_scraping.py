from scraper.sketch_paradigms.sktenten import mk_imperatives
from scraper.dictcom_sk import DictComSk
dictcom = DictComSk()

from cutverbs.language import Language
sk = Language('sk')

def test_dictcom_cuts_off_pronouns():
    assert dictcom.cached_paradigm('spať')['participle'][('neuter', 'sg')] == 'spalo'  # was just 'lo', because of diff number of items in: 'ja som spa l' vs 'ono spa lo'
    assert dictcom.cached_paradigm('hajať')['finite'][('3', 'sg')] == 'hajá' # dictcom gives `on hajá` vs `ja hajá m`: make sure diff number of items are chopped off correctly

def test_verb_with_two_paradigms():
    assert sk.paradigm_for('hltať')[('3', 'sg')] == 'hlta:' # 'hltse/hlta:' – the verbs has two paradigms, in 3sg combination of both of them
    assert sk.paradigm_for('hlceť')[('3', 'sg')] == 'hltse' # invented infinitive for the other variant

def test_imperative_form_instead_of_present_tense():
    assert sk.paradigm_for('vravieť')[('1', 'pl')] == 'vravi:me' # 'vravme'– 1pl is the imperative forms instead of present tense
    assert sk.paradigm_for('vyhodnotiť')[('2', 'pl')] == 'vihodnotji:tje' # vihodnotjtje
    assert sk.paradigm_for('naučiť')[('2', 'pl')] == 'nautši:tje' # nautštje

def test_length_on_thematic_vowel():
    assert sk.paradigm_for('predeliť')[('1', 'sg')] == 'predjeli:m' # 'predjelim'– there is a missing length on TV in 1sg

def test_dictcom_disambiguates_multiple_options():
    assert dictcom.cached_paradigm('drkotať')['gerund'] == ['drkocúc', 'drkotajúc'] # there are two forms of gerund
    assert dictcom.cached_paradigm('cumľať')['gerund'] == ['cumľajúc', 'cumľúc'] # there are two forms of gerund
# not sure whether the test is written correctly

def test_spat():
    paradigm = sk.cached_paradigm_for('spať')
    assert paradigm['participle'][('neuter', 'sg')] == 'spalo'  # was just 'lo', because of diff # of items in: 'ja som spa l' vs 'ono spa lo'
    assert paradigm['passive'][('masc', 'sg')] == 'spaný'       # n/t alternation
    assert paradigm['meta']['passive_nt_alternation'] == True

def test_generate_imperatives():
    assert mk_imperatives('drží') == ['drž', 'držme', 'držte', 'drz', 'drzme', 'drzte']
    assert mk_imperatives('da')[1] == 'dajme'
    assert mk_imperatives('jebe')[1] == 'jebme'
    assert mk_imperatives('dakuje')[1] == 'dakujme'
    assert len(mk_imperatives('oddá')) == 6
    assert mk_imperatives('prebudí')[1] == 'prebuďme' 
    assert mk_imperatives('drží')[1] == 'držme'
