e = {
    'brát':'e',
    'česat':'e',
    'číst':'e',
    'chápat':'e',
    'lézt':'e',
    'lhát':'e',
    'jmout':'e',
    'mýt':'e',                          #ve finitních tvarech i: > i v kořeni
    'smát':'e',
    'uspět':'e',
    'chvět':'e',
    'objet': 'e',
    'odnést': 'e',
    'opsat': 'e',
    'otázat': 'e',
    'otřást': 'e',
    'ozvat': 'e',
    'pást': 'e',
    'péct': 'e',
    'pobýt': 'e',
    'podvést': 'e',
    'opít': 'e', # Ondrej: rikam 'opiju', ne 'opiji'
    'ožít': 'e', # Ondrej: 'ožiju'
}

e_n = {
    'blbnout':'e_n',
    'bodnout':'e_n',
    'cvaknout':'e_n',
    'minout':'e_n',
    'lesknout':'e_n',
    'nastat':'e_n',
    'lajknout':'e_n',
    'začít':'e_n',
    'počít': 'e_n',
    'planout': 'e_n',
}

e_j_epenthesis = {
    'bít':'e',
    'žít':'e',
    'zabít':'e',
    'blahopřát':'e', 
    'přispět':'e',
    'chvět':'e',
}

e_uju = {
    'cálovat':'e_uj', 
    'asistovat':'e_uj', 
    'apelovat':'e_uj', 
    'adresovat':'e_uj', 
    'darovat':'e_uj',
    'plout':'e_uj',
    'zout':'e_uj',
    'lelkovat':'e_uj',  
    'lyžovat':'e_uj',   
    'lajkovat':'e_uj',         
    'malovat':'e_uj',
    'iniciovat':'e_uj',
    'doporučovat':'e_uj',
    'vokalizovat': 'e_uj', # problem with 1sg (-uji) and 3pl (-uji:)    
}

