to_add_from_ondrej = {
    'odvážet': 'eji', # ve 3pl chce 'odváží', ale já mám 'odvážejí'
    'otupět': 'eji',
    'lpět':'eji',
    'nakrájet':'eji',
    'natáčet':'eji',   
    'narážet':'eji',
    'chybět':'eji',
    'vytvářet':'eji',
    'klanět':'eji',
}


# L Z S 
eji_no_pal = {
    'bulet':'eji',
    'válet':'eji',
    'kutálet':'eji',
    'sdílet':'eji',
    'sázet':'eji',
    'vycházet':'eji',
    'souviset':'eji',
    'muset':'eji',
}

# J only in 3pl
eji_j_epen = {
    'civět':'eji',
    'vyprávět':'eji',
    'chybět':'eji',
    'roztápět':'eji'}

# J in all forms
eji_ě = {
    'krájet':'eji',
    'spouštět':'eji',
    'svádět':'eji',
    'naklánět':'eji',
    'dít':'eji',
    'uvádět':'eji',
    'ujíždět':'eji'}

# MĚ
eji_m_palatalisation = {
    'čumět':'eji',
    'šumět':'eji',
    'umět':'eji',
    'smět':'eji',
    'rozumět':'eji'}

# Š Č Ž Ř
eji_palatalised_root = {
    'odnášet':'eji',
    'zkoušet':'eji',
    'kráčet':'eji',
    'namáčet':'eji',
    'vrážet':'eji',
    'rozhlížet':'eji',
    'zmoudřet':'eji',
    'večeřet':'eji'}

