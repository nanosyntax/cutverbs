# Suppletive
suppletive = {
    'jíst':'i',
    'sníst':'i',
    'vědět':'i',
    'odpovědět':'i'
}

ondrej_verbs_no_working = {
    # prirucka issues
    "pět": "i_suffix_epen_informal", # je homonymní s číslovnkou - JDE TO ŘEŠIT NAPŘ. TAK, ŽE FUNKCI ŘEKNU, ŽE CHCI SLOVESO?
    "obřezat": "a_suffix", # prirucka gives  ['obřezám', 'obřeži', 'obřežu'] - A JAK TO ŘEŠIT?
    "plavat": "e_suffix", # prirucka gives  ['plavej', 'plav', 'poplav'] - JAK ŘEŠIT?
    "posrat": "e_suffix", # neni v prirucce
    "podělkovat": "i_suffix_epen_informal", # neni v prirucce

    # suppletion
    "odpovědět": "i_suffix", # problemem je "odpovjedi:" – je potreba ten kmen rozsirit v tyhle osobe, zavist neco jako vyjimku?
    "pomoct": "e_suffix", # "pomohu", ale "pomůžeš" – kdyby bylo "pomůžu" v 1. osobě (resp. v 6.), tak je po problému – řešit jako výjimku
}

lukas_not_working_verbs = {
    'křesat':'e_suffix',                      #a_suffix funguje, e_suffix ne
    'prosit':'i_suffix',                      #ambigní (kód he neuhádne jako prosit, prý na něj sedí víc paradigmat)
    'kopat':'e_suffix',                       #jak se vyrovnat s dubletami? Mělo by být a_suffix i e_suffix.
    'odplavávat':'a_suffix',                  #přířučka nezná
    'lít':'e_suffix_epen_informal',  
}
