a_sffx_length = {
    'bafať': 'a_sffx_length',
    'behať': 'a_sffx_length',
    'čakať': 'a_sffx_length',    # recorded 
    'červenať': 'a_sffx_length',    # recorded
    'čuchať': 'a_sffx_length',
    'cikať': 'a_sffx_length',
    'dať': 'a_sffx_length',
    'dbať': 'a_sffx_length',    # recorded
    'džgať': 'a_sffx_length',
    'džugať': 'a_sffx_length',
    'glgať': 'a_sffx_length',
    'grgať': 'a_sffx_length',
    'hajať': 'a_sffx_length',
    'hľadať': 'a_sffx_length',    # recorded
    'hnevať': 'a_sffx_length',    # recorded
    'hojdať': 'a_sffx_length',
    'hrať': 'a_sffx_length',    # recorded
    'hryzkať': 'a_sffx_length',
    'chovať': 'a_sffx_length',
    'chytať': 'a_sffx_length',
    'jajkať': 'a_sffx_length',
    'jednať': 'a_sffx_length',
    'klesať': 'a_sffx_length',    # recorded
    'konať': 'a_sffx_length',
    'lengať': 'a_sffx_length',
    'mať': 'a_sffx_length',    # recorded
    'mňaukať': 'a_sffx_length',
    'ňuchať': 'a_sffx_length',
    'objednať': 'a_sffx_length',
    'odolať': 'a_sffx_length',    
    'ochutnať': 'a_sffx_length',
    'padať': 'a_sffx_length',    # recorded
    'pamätať': 'a_sffx_length',
    'papať': 'a_sffx_length',    # recorded
    'plytvať': 'a_sffx_length',
    'postarať': 'a_sffx_length',
    'požehnať': 'a_sffx_length',
    'pozerať': 'a_sffx_length',    # recorded
    'poznať': 'a_sffx_length',    # recorded
    'prečkať': 'a_sffx_length',
    'predať': 'a_sffx_length',
    'predpovedať': 'a_sffx_length',
    'prekladať': 'a_sffx_length',
    'pretekať': 'a_sffx_length',
    'pridať': 'a_sffx_length',
    'rozoberať': 'a_sffx_length',
    'sekať': 'a_sffx_length',
    'starať': 'a_sffx_length',
    'šepkať': 'a_sffx_length',
    'štekať': 'a_sffx_length',
    'štverať': 'a_sffx_length',
    'tkať': 'a_sffx_length',
    'tikať': 'a_sffx_length',
    'trhať': 'a_sffx_length',    # recorded
    'trvať': 'a_sffx_length',    # recorded
    'tykať': 'a_sffx_length',
    'ťahať': 'a_sffx_length',    # recorded
    'ťukať': 'a_sffx_length',
    'volať': 'a_sffx_length',    # recorded 
    'vrhať': 'a_sffx_length',    # recorded
    'vychutnať': 'a_sffx_length',
    'vyzerať': 'a_sffx_length',
    'zahrať': 'a_sffx_length',
    'zanedbať': 'a_sffx_length',
    'zdať': 'a_sffx_length',
    'zelenať': 'a_sffx_length',    # recorded
    'znamenať': 'a_sffx_length',
    'znať': 'a_sffx_length',
    'zobkať': 'a_sffx_length',
    'žehnať': 'a_sffx_length',
    'želať': 'a_sffx_length',
}

a_root_length = {
    'bádať': 'a_root_length',
    'baláchať': 'a_root_length',
    'burácať': 'a_root_length', 
    'búrať': 'a_root_length',    # recorded
    'bývať': 'a_root_length',    # recorded
    'cmúľať': 'a_root_length',
    'cucať': 'a_root_length',
    'číhať': 'a_root_length',
    'čítať': 'a_root_length',    # recorded
    'dobývať': 'a_root_length',
    'dopĺňať': 'a_root_length',
    'dostávať': 'a_root_length',
    'dostavať': 'a_root_length',
    'dúfať': 'a_root_length',    # recorded
    'dýchať': 'a_root_length',    # recorded
    'fúkať': 'a_root_length',
    'gágať': 'a_root_length',
    'gúľať': 'a_root_length',
    'grcať': 'a_root_length',
    'hádať': 'a_root_length',
    'híkať': 'a_root_length',
    'ískať': 'a_root_length',
    'klaňať': 'a_root_length',
    'kráčať': 'a_root_length',
    'krívať': 'a_root_length',
    'kŕkať': 'a_root_length',
    'krvácať': 'a_root_length',
    'lákať': 'a_root_length',
    'lietať': 'a_root_length',
    'máčať': 'a_root_length',
    'miešať': 'a_root_length',  
    'nahrávať': 'a_root_length',
    'napĺňať': 'a_root_length', 
    'nechávať': 'a_root_length', 
    'ňúrať': 'a_root_length',
    'obávať': 'a_root_length',
    'obracať': 'a_root_length',
    'odmietať': 'a_root_length',
    'odolávať': 'a_root_length',
    'odpočívať': 'a_root_length',
    'odpúšťať': 'a_root_length', 
    'oháňať': 'a_root_length',
    'ohovárať': 'a_root_length',
    'okúňať': 'a_root_length',
    'ovládať': 'a_root_length',
    'plávať': 'a_root_length',    # recorded
    'otriasať': 'a_root_length',
    'počítať': 'a_root_length',
    'počúvať': 'a_root_length',
    'pomáhať': 'a_root_length',    # recorded
    'prebiehať': 'a_root_length',    # recorded
    'predávať': 'a_root_length',
    'predvídať': 'a_root_length',    # recorded
    'preháňať': 'a_root_length',
    'premieňať': 'a_root_length',
    'prežívať': 'a_root_length',
    'používať': 'a_root_length',
    'ráňať': 'a_root_length', 
    'rozvíjať': 'a_root_length',
    'rušať': 'a_root_length', 
    'rúhať': 'a_root_length',
    'skláňať': 'a_root_length',
    'spievať': 'a_root_length',    # recorded
    'spĺňať': 'a_root_length',
    'spýtať': 'a_root_length', 
    'striedať': 'a_root_length',
    'strieľať': 'a_root_length',
    'strúhať': 'a_root_length',
    'stúpať': 'a_root_length',    # recorded
    'škriekať': 'a_root_length',    # recorded
    'štikútať': 'a_root_length',
    'triafať': 'a_root_length',
    'trúfať': 'a_root_length',    # recorded
    'uháňať': 'a_root_length',
    'uvádzať': 'a_root_length',    # recorded
    'večerať': 'a_root_length',
    'vnímať': 'a_root_length',    # recorded
    'vŕzgať': 'a_root_length',    # recorded
    'vyrábať': 'a_root_length',    # recorded
    'vyratúvať': 'a_root_length',
    'uvádzať': 'a_root_length',    # recorded
    'uzatvárať': 'a_root_length',
    'užívať': 'a_root_length',
    'zacláňať': 'a_root_length',
    'zabúdať': 'a_root_length',    # recorded
    'zahŕňať': 'a_root_length', 
    'zamieňať': 'a_root_length',
    'zbierať': 'a_root_length',
    'získať': 'a_root_length',
    'zívať': 'a_root_length',
    'zháňať': 'a_root_length',
    'žiadať': 'a_root_length',
}

a_root_length_pal = {
    'merať': 'a_root_length',    # recorded
    'míňať': 'a_root_length',    # recorded
    'požičať': 'a_root_length',
    'púšťať': 'a_root_length',    # recorded
    'skúšať': 'a_root_length',    # recorded
    'stavať': 'a_root_length',    # recorded
    'utrácať': 'a_root_length',    # recorded
    'vešať': 'a_root_length',    # recorded
    'voňať': 'a_root_length',    # recorded
    'vracať': 'a_root_length',    # recorded
    'zavadzať': 'a_root_length',    # recorded
    'znášať': 'a_root_length',
}

a_va_sffx = {
    # 'dávať': 'a_va',
    'získavať': 'a_va'
}

i_sffx_length = {
    'báť': 'i_sffx_length',    # recorded
    'baviť': 'i_sffx_length',    # recorded
    'bežať': 'i_sffx_length',    # recorded
    'blyšťať': 'i_sffx_length',
    'bolieť': 'i_sffx_length',
    'budiť': 'i_sffx_length',    # recorded
    'cvendžať': 'i_sffx_length',
    'čeliť': 'i_sffx_length',
    'čistiť': 'i_sffx_length',
    'čumieť': 'i_sffx_length',
    'čupieť': 'i_sffx_length',
    'čušať': 'i_sffx_length',
    'dariť': 'i_sffx_length',
    'dediť': 'i_sffx_length',
    'deliť': 'i_sffx_length',
    'dojčiť': 'i_sffx_length',
    'dojiť': 'i_sffx_length',    # recorded
    'držať': 'i_sffx_length',
    'drviť': 'i_sffx_length',
    'dymiť': 'i_sffx_length',    # recorded
    'dunieť': 'i_sffx_length',
    'erdžať': 'i_sffx_length',
    'fajčiť': 'i_sffx_length',
    'fotiť': 'i_sffx_length',    # recorded
    'funieť': 'i_sffx_length',
    'hanbiť': 'i_sffx_length',
    'hľadieť': 'i_sffx_length',
    'hnojiť': 'i_sffx_length',    # recorded
    'hojiť': 'i_sffx_length',
    'horieť': 'i_sffx_length',
    'hovoriť': 'i_sffx_length',    # recorded
    'hroziť': 'i_sffx_length',
    'hrmieť': 'i_sffx_length',    # recorded
    'chodiť': 'i_sffx_length', 
    'chopiť': 'i_sffx_length',
    'chybieť': 'i_sffx_length',
    'iskriť': 'i_sffx_length',
    'javiť': 'i_sffx_length',
    'jatriť': 'i_sffx_length',
    'jazdiť': 'i_sffx_length', 
    'končiť': 'i_sffx_length',
    'kosiť': 'i_sffx_length',
    'kresliť': 'i_sffx_length',
    'kričať': 'i_sffx_length',
    'krstiť': 'i_sffx_length',
    'kupčiť': 'i_sffx_length',
    'kypieť': 'i_sffx_length',
    'leňošiť': 'i_sffx_length',
    'lepiť': 'i_sffx_length',    # recorded
    'letieť': 'i_sffx_length',
    'ležať': 'i_sffx_length', 
    'loviť': 'i_sffx_length',
    'meniť': 'i_sffx_length',
    'mlčať': 'i_sffx_length',
    'močiť': 'i_sffx_length',
    'mrzieť': 'i_sffx_length',
    'mučiť': 'i_sffx_length',
    'musieť': 'i_sffx_length',    # recorded
    'myslieť': 'i_sffx_length',    # recorded
    'naučiť': 'i_sffx_length',
    'násobiť': 'i_sffx_length',
    'nosiť': 'i_sffx_length',    # recorded
    'obsadiť': 'i_sffx_length',
    'opustiť': 'i_sffx_length',
    'ošetriť': 'i_sffx_length',
    'oslobodiť': 'i_sffx_length',
    'osloviť': 'i_sffx_length',
    'patriť': 'i_sffx_length',    # recorded
    'pišťať': 'i_sffx_length',
    'platiť': 'i_sffx_length',
    'prebudiť': 'i_sffx_length',
    'prekvapiť': 'i_sffx_length',
    'primusieť': 'i_sffx_length',
    'pokrstiť': 'i_sffx_length',
    'postaviť': 'i_sffx_length',
    'podeliť': 'i_sffx_length',
    'podrobiť': 'i_sffx_length',
    'poprosiť': 'i_sffx_length',
    'popučiť': 'i_sffx_length',
    'predeliť': 'i_sffx_length',
    'pršať': 'i_sffx_length',
    'radiť': 'i_sffx_length',
    'raniť': 'i_sffx_length',
    'robiť': 'i_sffx_length',    # recorded
    'rodiť': 'i_sffx_length',
    'rozdeliť': 'i_sffx_length',
    'sedieť': 'i_sffx_length',    # recorded
    'sipieť': 'i_sffx_length',
    'skočiť': 'i_sffx_length',
    'slušať': 'i_sffx_length',
    'smažiť': 'i_sffx_length',
    'smädiť': 'i_sffx_length',
    'smrdieť': 'i_sffx_length',
    'snažiť': 'i_sffx_length',
    'snežiť': 'i_sffx_length',
    'spať': 'i_sffx_length',    # recorded
    'spraviť': 'i_sffx_length',
    'stáť': 'i_sffx_length',    # recorded
    'strašiť': 'i_sffx_length',
    'streliť': 'i_sffx_length',
    'stvoriť': 'i_sffx_length',
    'súhlasiť': 'i_sffx_length',
    'súťažiť': 'i_sffx_length',
    'súvisieť': 'i_sffx_length',
    'svedčiť': 'i_sffx_length',
    'šetriť': 'i_sffx_length',
    'škodiť': 'i_sffx_length',
    'šumieť': 'i_sffx_length',    # recorded
    'šťať': 'i_sffx_length',    # recorded
    'štepiť': 'i_sffx_length',
    'tešiť': 'i_sffx_length',
    'tlačiť': 'i_sffx_length',
    'trafiť': 'i_sffx_length',    # recorded
    'trpieť': 'i_sffx_length',
    'tušiť': 'i_sffx_length',    # recorded
    'tvoriť': 'i_sffx_length',
    'tvrdiť': 'i_sffx_length',
    'ťažiť': 'i_sffx_length',
    'učiť': 'i_sffx_length',    # recorded
    'ukončiť': 'i_sffx_length',
    'umožniť': 'i_sffx_length',
    'uškodiť': 'i_sffx_length',
    'útočiť': 'i_sffx_length',
    'variť': 'i_sffx_length',    # recorded 
    'väzieť': 'i_sffx_length',
    'veriť': 'i_sffx_length',    # recorded
    'vidieť': 'i_sffx_length',    # recorded  
    'visieť': 'i_sffx_length',
    'vodiť': 'i_sffx_length',
    'voliť': 'i_sffx_length',    # recorded
    'voziť': 'i_sffx_length',    # recorded
    'vravieť': 'i_sffx_length',
    'vrtieť': 'i_sffx_length',
    'vydediť': 'i_sffx_length',
    'vydržať': 'i_sffx_length',
    'vyhodnotiť': 'i_sffx_length',
    'vzbudiť': 'i_sffx_length', 
    'zabezpečiť': 'i_sffx_length',
    'zahojiť': 'i_sffx_length',
    'zaútočiť': 'i_sffx_length',
    'zaspať': 'i_sffx_length',    
    'závidieť': 'i_sffx_length',
    'zhodnotiť': 'i_sffx_length',
    'zhorieť': 'i_sffx_length',
    'zmeniť': 'i_sffx_length',
    'zradiť': 'i_sffx_length',
    'zrkadliť': 'i_sffx_length',
}

i_root_length = {
    'bieliť': 'i_root_length',
    'blázniť': 'i_root_length',
    'blúdiť': 'i_root_length',
    'brániť': 'i_root_length',    # recorded
    'cítiť': 'i_root_length',    # recorded
    'hlásiť': 'i_root_length',    # recorded
    'chváliť': 'i_root_length',    # recorded
    'líčiť': 'i_root_length',    # recorded
    'liečiť': 'i_root_length',    # recorded
    'ľúbiť': 'i_root_length',    # recorded
    'lúpiť': 'i_root_length',    # recorded
    'kŕmiť': 'i_root_length',    # recorded
    'krútiť': 'i_root_length',
    'kúpiť': 'i_root_length',    # recorded
    'líšiť': 'i_root_length',
    'nakrútiť': 'i_root_length',
    'odstrániť': 'i_root_length',
    'okabátiť': 'i_root_length', 
    'otráviť': 'i_root_length',
    'páčiť': 'i_root_length',
    'páliť': 'i_root_length',
    'prepáčiť': 'i_root_length',
    'riadiť': 'i_root_length',
    'riešiť': 'i_root_length',
    'sľúbiť': 'i_root_length',
    'slúžiť': 'i_root_length',    # recorded
    'strážiť': 'i_root_length',
    'súdiť': 'i_root_length',
    'svietiť': 'i_root_length',
    'šíriť': 'i_root_length',
    'trápiť': 'i_root_length',
    'tráviť': 'i_root_length',    # recorded
    'triediť': 'i_root_length',
    'trúbiť': 'i_root_length',
    'túliť': 'i_root_length',
    'túžiť': 'i_root_length',    # recorded
    'upriamiť': 'i_root_length',
    'vábiť': 'i_root_length',
    'vrátiť': 'i_root_length',    # recorded
    'záhradníčiť': 'i_root_length',
    'zapáliť': 'i_root_length',
    'zvážiť': 'i_root_length',
    'zvýšiť': 'i_root_length',
}

e_3pl_preserved = {
    'belieť': 'e_3pl_preserved',
    'civieť': 'e_3pl_preserved',    # recorded, NS used civja as 3PL
    'červivieť': 'e_3pl_preserved',
    'čpieť': 'e_3pl_preserved',    # recorded
    'divieť': 'e_3pl_preserved',
    'fialovieť': 'e_3pl_preserved',
    'hlivieť': 'e_3pl_preserved',
    'hovieť': 'e_3pl_preserved',
    'jarabieť': 'e_3pl_preserved',
    'kučeravieť': 'e_3pl_preserved',
    'lenivieť': 'e_3pl_preserved',
    'ľpieť': 'e_3pl_preserved',
    'meravieť': 'e_3pl_preserved',    # recorded
    'modrieť': 'e_3pl_preserved',    # recorded
    'múdrieť': 'e_3pl_preserved',    # recorded
    'omdlieť': 'e_3pl_preserved',    # recorded
    'ochorieť': 'e_3pl_preserved',
    'otupieť': 'e_3pl_preserved',    # recorded
    'ovdovieť': 'e_3pl_preserved',
    'plesnivieť': 'e_3pl_preserved',    # recorded
    'rozumieť': 'e_3pl_preserved',    # recorded
    'sivieť': 'e_3pl_preserved',
    'skvieť': 'e_3pl_preserved',
    'šalieť': 'e_3pl_preserved',    # recorded
    'šedivieť': 'e_3pl_preserved',    # recorded
    'tkvieť': 'e_3pl_preserved',
    'tlieť': 'e_3pl_preserved',
    'tmieť': 'e_3pl_preserved',
    'zmúdrieť': 'e_3pl_preserved',    # recorded
    'zružovieť': 'e_3pl_preserved',    # recorded
    'želieť': 'e_3pl_preserved',
}

e_3pl_preserved_pal = {
    'besnieť': 'e_3pl_preserved',    # recorded
    'bdieť': 'e_3pl_preserved',    # recorded
    'brnieť': 'e_3pl_preserved',    # recorded
    'bujnieť': 'e_3pl_preserved',    # recorded
    'cnieť': 'e_3pl_preserved',
    'černieť': 'e_3pl_preserved',
    'červenieť': 'e_3pl_preserved',    # recorded 
    'čnieť': 'e_3pl_preserved',    # recorded
    'drevnatieť': 'e_3pl_preserved',
    'kamenieť': 'e_3pl_preserved',
    'krpatieť': 'e_3pl_preserved',
    'lačnieť': 'e_3pl_preserved',
    'ondieť': 'e_3pl_preserved',
    'osinieť': 'e_3pl_preserved',    # recorded
    'pevnieť': 'e_3pl_preserved',
    'plesnieť': 'e_3pl_preserved',
    'pnieť': 'e_3pl_preserved',
    'temnieť': 'e_3pl_preserved',
    'splstnatieť': 'e_3pl_preserved',
    'spozornieť': 'e_3pl_preserved',
    'znieť': 'e_3pl_preserved',    # recorded
    'zelenieť': 'e_3pl_preserved',
    'zvážnieť': 'e_3pl_preserved',    # recorded
    'žltieť': 'e_3pl_preserved',
}

e_uj_sffx = {
    'absorbovať': 'e_uj',
    'aktivizovať': 'e_uj',
    'akumulovať': 'e_uj',  
    'amerikanizovať': 'e_uj',
    'amortizovať': 'e_uj',
    'amputovať': 'e_uj',
    'anketovať': 'e_uj',
    'anticipovať': 'e_uj', 
    'artikulovať': 'e_uj',
    'asimilovať': 'e_uj',
    'atomizovať': 'e_uj',
    'automatizovať': 'e_uj',
    'autorizovať': 'e_uj',
    'balzamovať': 'e_uj',
    'bankrotovať': 'e_uj',
    'betónovať': 'e_uj',
    'bojovať': 'e_uj',
    'boľševizovať': 'e_uj',
    'boxovať': 'e_uj',
    'budovať': 'e_uj',
    'cestovať': 'e_uj',    # recorded
    'cínovať': 'e_uj',
    'ďakovať': 'e_uj',
    'datovať': 'e_uj',
    'debutovať': 'e_uj',
    'dedikovať': 'e_uj',
    'defilovať': 'e_uj', 
    'demokratizovať': 'e_uj',
    'deportovať': 'e_uj', 
    'destabilizovať': 'e_uj',
    'diskreditovať': 'e_uj',
    'domestikovať': 'e_uj',
    'dominovať': 'e_uj',
    'donucovať': 'e_uj',
    'doťahovať': 'e_uj',
    'duplikovať': 'e_uj',
    'ejakulovať': 'e_uj',
    'elektrifikovať': 'e_uj',
    'emigrovať': 'e_uj',
    'eskortovať': 'e_uj',
    'exhumovať': 'e_uj',
    'faulovať': 'e_uj',
    'faxovať': 'e_uj', 
    'finišovať': 'e_uj',
    'fotografovať': 'e_uj',
    'galvanizovať': 'e_uj',
    'gazdovať': 'e_uj',
    'grilovať': 'e_uj',
    'hladovať': 'e_uj',
    'hlasovať': 'e_uj',
    'chrómovať': 'e_uj',
    'idealizovať': 'e_uj',
    'kovať': 'e_uj',
    'kupovať': 'e_uj',    # recorded
    'magnetizovať': 'e_uj',
    'masírovať': 'e_uj', 
    'masturbovať': 'e_uj',
    'mašírovať': 'e_uj',
    'milovať': 'e_uj',
    'mumifikovať': 'e_uj',  
    'napredovať': 'e_uj',
    'obraňovať': 'e_uj',
    'ohlasovať': 'e_uj',
    'pochodovať': 'e_uj',
    'pokračovať': 'e_uj',
    'polemizovať': 'e_uj',
    'potrebovať': 'e_uj',
    'poukazovať': 'e_uj',
    'poskytovať': 'e_uj',
    'pracovať': 'e_uj',    # recorded
    'raňajkovať': 'e_uj',
    'ráčkovať': 'e_uj',  
    'röntgenovať': 'e_uj',
    'tancovať': 'e_uj',    # recorded 
    'telefonovať': 'e_uj',    # recorded
    'terorizovať': 'e_uj',
    'skrutkovať': 'e_uj',
    'spolupracovať': 'e_uj',
    'stotožňovať': 'e_uj',
    'šoférovať': 'e_uj',
    'študovať': 'e_uj',    # recorded
    'ubezpečovať': 'e_uj',
    'ukameňovať': 'e_uj',
    'venovať': 'e_uj', 
    'vypracovať': 'e_uj',
    'zdržovať': 'e_uj',
    'zröntgenovať': 'e_uj',
}

e_n_sffx_length = {
    'blednúť': 'e_n_sffx_length',    # recorded
    'bodnúť': 'e_n_sffx_length',
    'dostať': 'e_n_sffx_length',
    'dotknúť': 'e_n_sffx_length',
    'džugnúť': 'e_n_sffx_length',
    'hluchnúť': 'e_n_sffx_length',    # recorded
    'hlúpnuť': 'e_n_root_length',
    'hnúť': 'e_n_sffx_length',
    'chudnúť': 'e_n_sffx_length',
    'kradnúť': 'e_n_sffx_length',    # recorded
    'mäknúť': 'e_n_sffx_length',    # recorded
    'mrznúť': 'e_n_sffx_length',    # recorded
    'odtrhnúť': 'e_n_sffx_length',
    'primknúť': 'e_n_sffx_length',
    'rozhodnúť': 'e_n_sffx_length',
    'schnúť': 'e_n_sffx_length',    # recorded 
    'slepnúť': 'e_n_sffx_length',    # recorded 
    'stretnúť': 'e_n_sffx_length',
    'škrtnúť': 'e_n_sffx_length',
    'tuhnúť': 'e_n_sffx_length',
    'ukradnúť': 'e_n_sffx_length',
    'vädnúť': 'e_n_sffx_length',
    'vypnúť': 'e_n_sffx_length',
    'zabudnúť': 'e_n_sffx_length',
    'zahynúť': 'e_n_sffx_length',
    'začať': 'e_n_sffx_length',
    'zapnúť': 'e_n_sffx_length',
    'zmrznúť': 'e_n_sffx_length',    # recorded
    'zobnúť': 'e_n_sffx_length',
    'žasnúť': 'e_n_sffx_length',
}

e_n_root_length = {
    'kýchnuť': 'e_n_root_length',   
    'lízať': 'e_root_length',
    'tiahnuť': 'e_n_root_length',
    'viazať': 'e_root_length',
    'zvládnuť': 'e_n_root_length',
}

e_no_length = {
    'biť': 'e_no_length',    # recorded
    'česať': 'e_no_length',    # recorded
    'diať': 'e_no_length',
    'dospieť': 'e_no_length',
    'džavotať': 'e_no_length',
    'hrabať': 'e_no_length',
    'hriať': 'e_no_length',    # recorded
    'chvieť': 'e_no_length',
    'kašlať': 'e_no_length',    # recorded
    'kašľať': 'e_no_length',
    'klamať': 'e_no_length',    # recorded
    'kliať': 'e_no_length',
    'kopať': 'e_no_length',    # recorded
    'kryť': 'e_no_length',
    'chcieť': 'e_no_length',
    'objať': 'e_no_length',
    'odieť': 'e_no_length',
    'opiť': 'e_no_length',
    'ozvať': 'e_no_length',
    'piť': 'e_no_length',
    'plieť': 'e_no_length',
    'pratať': 'e_no_length',
    'priať': 'e_no_length',    # recorded
    'rehotať': 'e_no_length',
    'revať': 'e_no_length', 
    'rezať': 'e_no_length',    # recorded
    'rozmazať': 'e_no_length',
    'ryť': 'e_no_length',
    'sať': 'e_no_length',    # recorded 
    'siať': 'e_no_length',    # recorded
    'smiať': 'e_no_length',
    'spieť': 'e_no_length',    # recorded
    'šibať': 'e_no_length',
    'šiť': 'e_no_length',
    'škrabať': 'e_no_length',
    'štvať': 'e_no_length',    # recorded
    'šušlať': 'e_no_length',
    'tepať': 'e_no_length',
    'upratať': 'e_no_length',
    'viať': 'e_no_length',    # recorded
    'vymazať': 'e_no_length',
    'vysiať': 'e_no_length',
    'vyspieť': 'e_no_length',
    'vziať': 'e_no_length',    # recorded
    'zabiť': 'e_no_length',    
    'zvať': 'e_no_length',
    'žiť': 'e_no_length',    # recorded
}

e_no_length_root_final_n = {
    'hnať': 'e_no_length',    # recorded, n is a part of the root
    'ťať': 'e_no_length', # n is a part of the root
    'zažať': 'e_no_length',
    'žať': 'e_no_length',    # recorded 
    'rozmazneť': 'e_no_length', #two paradigms, correctl infinitive rozmaznať
}

e_no_length_root_final_vowel = {
    'pľuť': 'e_no_length',   # problem with the 'uj' sffx
    'počuť': 'e_no_length',  # problem with the 'uj' sffx
    'žuť': 'e_no_length',   # problem with the 'uj' sffx
    'žuvať': 'e_no_length', # problem with the 'uj' sffx
}

e_no_length_pal = {
    'brať': 'e_no_length',    # recorded
    'drať': 'e_no_length',
    'drieť': 'e_no_length',
    'hrýzť': 'e_no_length',
    'huhňať': 'e_no_length',
    'ísť': 'e_no_length',    # recorded
    'klásť': 'e_no_length',
    'liezť': 'e_no_length',
    'miesť': 'e_no_length',
    'mlieť': 'e_no_length',
    'mrieť': 'e_no_length',
    'niesť': 'e_no_length',    # recorded
    'orať': 'e_no_length',
    'pásť': 'e_no_length',
    'piecť': 'e_no_length',
    'pliesť': 'e_no_length',
    'prať': 'e_no_length',
    'prieť': 'e_no_length',
    'priniesť': 'e_no_length', 
    'rásť': 'e_no_length',
    'srať': 'e_no_length',
    'stĺcť': 'e_no_length',
    'striezť': 'e_no_length', #pal with Ž! 
    'škrieť': 'e_no_length',
    'tiecť': 'e_no_length',
    'tĺcť': 'e_no_length',
    'triasť': 'e_no_length',
    'trieť': 'e_no_length',
    'udrieť': 'e_no_length',
    'umrieť': 'e_no_length',
    'uniesť': 'e_no_length',
    'upiecť': 'e_no_length',
    'utiecť': 'e_no_length',
    'uzavrieť': 'e_no_length',
    'viesť': 'e_no_length',
    'viezť': 'e_no_length',
    'vrieť': 'e_no_length',
    'vyrásť': 'e_no_length',
    'zarásť': 'e_no_length',
    'zobať': 'e_no_length',
    'zomrieť': 'e_no_length',
    'zrieť': 'e_no_length',
    'žrať': 'e_no_length',
}

e_root_length = {
    'dokázať': 'e_root_length',
    'driapať': 'e_root_length',
    'driemať': 'e_root_length',
    'hádzať': 'e_root_length',    # recorded
    'hýbať': 'e_root_length',    # recorded
    'chápať': 'e_root_length',    # recorded
    'chrápať': 'e_root_length',
    'kázať': 'e_root_length',    # recorded
    'kĺzať': 'e_root_length',
    'lámať': 'e_root_length',
    'môcť': 'e_root_length',
    'písať': 'e_root_length',    # recorded 
    'preukázať': 'e_root_length',
    'sádzať': 'e_root_length', 
    'šliapať': 'e_root_length',    # recorded
    'ukázať': 'e_root_length',
    'pomôcť': 'e_root_length',
    'vládať': 'e_root_length',
    'zvládať': 'e_root_length',
}

e_root_length_pal = {
    'nájsť': 'e_root_length',
    'prísť': 'e_root_length',    # recorded
    'zísť': 'e_root_length',    # recorded 
}

irregular = {
    'byť': 'v_byť',    # recorded
    'jesť': '',    # recorded
    'povedať': '',
    'vedieť': '',    # recorded
    'smieť': '',    # recorded
}

two_paradigms = {
    'hltať': 'a_sffx_length', # two paradigms
    'hlceť': 'e_no_length', # two paradigms, correct infinitive hltať
    'rafať': 'a_sffx_length', # two paradigms
    'rafeť': 'e_no_length', # two paradigms, correct infinitive rafať
    'šeptať': 'a_sffx_length', # two paradigms
    'šepceť': 'e_no_length', # two paradigms, correct infinitive šeptať
    'luhať': 'a_sffx_length', # two paradigms
    'lužeť': 'e_no_length', # two paradigms, correct infinitive luhať
    'koktať': 'a_sffx_length', # two paradigms
    'kokceť': 'e_no_length', # two paradigms, correct infinitive koktať
    'páchať': 'a_root_length', # two paradigms
    'pášeť': 'e_root_length', # two paradigms, correct infinitive páchať
    'kecať': 'a_sffx_length', # two paradigms
    'keciať': 'a_root_length', # two paradigms, correct infinitive kecať
    'kývať': 'a_root_length', # two paradigms
    'kýveť': 'e_root_length', # two paradigms, correct infinitive kývať 
    'spáchať': 'a_root_length', # recorded, two paradigms
    'spášeť': 'e_root_length', # recorded, two paradigms, correct infinitive spáchať
    'ligotať': 'a_sffx_length', # two paradigms
    'ligoceť': 'e_no_length', # two paradigms, correct infinitive ligotať
    'rozmaznať': 'a_sffx_length', #two paradigms
}
    
problematic = {
    'drkotať': 'e_no_length', # problem with parsing, two forms of imperative
    'cumľať': 'a_sffx_length', # problem with parsing, two forms of transgressive   
}