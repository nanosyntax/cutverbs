ipa_pidpesate = {
    ('1', 'sg'): 'pidpeš→u',
    ('2', 'sg'): 'pidpeš←ɛš',
    ('3', 'sg'): 'pidpeš←ɛ',
    ('1', 'pl'): 'pidpeš←ɛmo',
    ('2', 'pl'): 'pidpeš←ɛtɛ',
    ('3', 'pl'): 'pidpeš←utʲ'
}

ipa_dušete = {
    ('1', 'sg'): 'duš→u',
    ('2', 'sg'): 'duš←eš',
    ('3', 'sg'): 'duš←etʲ',
    ('1', 'pl'): 'duš←emo',
    ('2', 'pl'): 'duš←etɛ',
    ('3', 'pl'): 'duš←atʲ'
}

ipa_varete = {
    ('1', 'sg'): 'varj→u',
    ('2', 'sg'): 'var←eš',
    ('3', 'sg'): 'var←etʲ',
    ('1', 'pl'): 'var←emo',
    ('2', 'pl'): 'var←etɛ',
    ('3', 'pl'): 'varj←atʲ' #3pl sffx is -jatj or PAL+atj?
}

ipa_ljubete = {
    ('1', 'sg'): 'ljublj→u',
    ('2', 'sg'): 'ljub←eš',
    ('3', 'sg'): 'ljub←etʲ',
    ('1', 'pl'): 'ljub←emo',
    ('2', 'pl'): 'ljub←etɛ',
    ('3', 'pl'): 'ljublj←atʲ'
}

ipa_plakate = {
    ('1', 'sg'): 'plač←u',
    ('2', 'sg'): 'plač←ɛš',
    ('3', 'sg'): 'plač←ɛ',
    ('1', 'pl'): 'plač←ɛmo',
    ('2', 'pl'): 'plač←ɛtɛ',
    ('3', 'pl'): 'plač←utʲ'
}

ipa_bačete = {
    ('1', 'sg'): 'bač←u',
    ('2', 'sg'): 'bač←eš',
    ('3', 'sg'): 'bač←etʲ',
    ('1', 'pl'): 'bač←emo',
    ('2', 'pl'): 'bač←etɛ',
    ('3', 'pl'): 'bač←atʲ'
}

ipa_pɛkte = {
    ('1', 'sg'): 'pɛč→u',
    ('2', 'sg'): 'pɛč→ɛš',
    ('3', 'sg'): 'pɛč→ɛ',
    ('1', 'pl'): 'pɛč→ɛmo', #pɛč ɛm'o
    ('2', 'pl'): 'pɛč→ɛtɛ', #pɛč ɛt'ɛ
    ('3', 'pl'): 'pɛč→utʲ'
}

ipa_učete = {
    ('1', 'sg'): 'uč→u',
    ('2', 'sg'): 'uč→eš',
    ('3', 'sg'): 'uč→etʲ',
    ('1', 'pl'): 'uč→emo', #uč em'o
    ('2', 'pl'): 'uč→etɛ', #uč et'ɛ
    ('3', 'pl'): 'uč→atʲ'
}




# pidpesate = {
#     ('1', 'sg'): 'підпишу́',
#     ('2', 'sg'): 'підпи́шеш',
#     ('3', 'sg'): 'підпи́ше',
#     ('1', 'pl'): 'підпи́шемо',
#     ('2', 'pl'): 'підпи́шете',
#     ('3', 'pl'): 'підпи́шуть'
# }

# dušete = {
#     ('1', 'sg'): 'душу́',
#     ('2', 'sg'): 'ду́шиш',
#     ('3', 'sg'): 'ду́шить',
#     ('1', 'pl'): 'ду́шимо',
#     ('2', 'pl'): 'ду́шите',
#     ('3', 'pl'): 'ду́шать'
# }


# ipa_pidpesate_1 = {
#     ('1', 'sg'): 'pidpešú',
#     ('2', 'sg'): 'pidpéšɛš',
#     ('3', 'sg'): 'pidpéšɛ',
#     ('1', 'pl'): 'pidpéšɛmo',
#     ('2', 'pl'): 'pidpéšɛtɛ',
#     ('3', 'pl'): 'pidpéšutʲ'
# }


