
#changing the order of paradigms in suffixes.verbs (from 3 elements-> 1 element) my algoritm is able to individuate complex stems
#Qs: what in case of such verbs as viju, where -i is part of the root.  
#issue with root segmentation: da- or d-? ma- or m-?

ɛ_sffx_stress = {
    'продавати': 'ɛ_sffx_stress', # considered root da-
    'здавати': 'ɛ_sffx_stress', # considered root da-
    'зібрати': 'ɛ_sffx_stress',
}

ɛ_root_stress = {
    
}

reflexives = {
    'позмагатися': 'TV_áj',
}

reflexives_1pl = {                      #1pl-sffx is -m not -mo
    'поскаржитися': 'e_root_stress', 
    'вибратися': 'ɛ_root_stress', 
    'друкуватися': 'TV_új', 
    'відкритися': 'ɛ_root_stress', 
    'застуджуватися': 'TV_uj',
    'вторгнутися': 'TV_n',
    'підвищуватися': 'TV_uj',
    'відокремитися': 'e_root_stress',
    'втомлюватися': 'TV_uj',
    'розвинутися': 'TV_n',
    'заступитися': 'e_mixed_stress',
    'скаржитися': 'e_root_stress',
    'позбавитися': 'e_root_stress',
    'підключитися': 'e_mixed_stress',
    'спізнюватися': 'TV_uj', 
}

reflexives_problematic = {
    # 'проводитись': 'e_root_stress', #in goroh present only 3sg and 3pl forms. How to deal with it?
    # 'обходитися': 'e_root_stress', #see 1.7
    # 'перевдягтися': 'ɛ_mixed_stress', #to download forms from goroh
    'обійтися':  'ɛ_mixed_stress',
    'спромогтися': 'ɛ_mixed_stress',
}

reflexives_stressed_jɛ = {              #see 1.4. in notes
    'ставатися': 'ɛ_sffx_stress',
    'битися': 'ɛ_sffx_stress',
    'зоставатися': 'ɛ_sffx_stress', 
    'діставатися': 'ɛ_sffx_stress',
    
    'здаватися': 'ɛ_sffx_stress',
  
    'зізнаватися': 'ɛ_sffx_stress',
    'дізнаватися': 'ɛ_sffx_stress',

    'сміятися':  'ɛ_sffx_stress',
    'засміятися': 'ɛ_sffx_stress',
    'розсміятися': 'ɛ_sffx_stress',
}


tv_aj = {
    'підтримати': 'TV_aj', # considered root m-; this verb with not sure segmentation (root trem- or tremaj-)
}

tv_uj = {
    'встановлювати': 'TV_uj',
    'купувати': 'TV_új',
    'підготувати': 'TV_új',
}

irregular = {                       #added own paradigm
    'дати': 'v_dam_sffx_stress',     
    'надати': 'v_dam_sffx_stress', 
    'видати': 'v_dam',             

    'їсти': 'v_jim',                 
    "з'їсти": 'v_jim',
    'наїсти': 'v_jim',

    # 'сопіти': 'ɛ_sffx_stress',#root behaviour class II + 1sg and 3pl sffx class II but in mid forms sffxes class I 
    # 'бути': 'v_je', #to download
}


e_mixed_stress = {
    'створити': 'e_mixed_stress',
    'купити': 'e_mixed_stress',
    'застудити': 'e_mixed_stress',
    'розголосити': 'e_mixed_stress',
    'відродити': 'e_mixed_stress',
   
   
    
}

e_root_stress = {
    'відправити': 'e_root_stress', 
    'вилетіти': 'e_root_stress',  
    'позаздрити': 'e_root_stress',
    'заводити': 'e_root_stress',
    'марнотратити': 'e_root_stress',
    'затвердити': 'e_root_stress',
    'засекретити': 'e_root_stress',
    'доводити': 'e_root_stress',
    'їздити': 'e_root_stress',
    'виїздити': 'e_root_stress',
    



}

e_sffx_stress = {
    'відлетіти': 'e_sffx_stress',
    'приїздити': 'e_sffx_stress',
    
}


ɛ_mixed_stress = {
    'підписати': 'ɛ_mixed_stress'
}  

double_stress_pattern = {       #added two lex entries in data.manual
                                #if str pattern is mixed, in infinit form stress is on last vowel
    'поясни́ти': 'e_sffx_stress',
    'пояснити́': 'e_mixed_stress',
    
    'кра́сти': 'ɛ_root_stress',
    'красти́': 'ɛ_mixed_stress',
    'вкра́сти': 'ɛ_root_stress',
    'вкрасти́': 'ɛ_mixed_stress',
    'укра́сти': 'ɛ_root_stress',
    'украсти́': 'ɛ_mixed_stress',
    
    'відобрази́ти': 'e_sffx_stress',
    'відобразити́': 'e_mixed_stress',

    'збагати́ти': 'e_sffx_stress',
    'збагатити́': 'e_mixed_stress',
   
    'ну́дити': 'e_root_stress',
    'нудити́': 'e_mixed_stress',

    'охоло́дити': 'e_sffx_stress',
    'охолодити́': 'e_mixed_stress',

    'досліди́ти': 'e_sffx_stress',
    'дослідити́': 'e_mixed_stress'



}

double_forms = {                #added two lex entries in data.manual  
    'дихати': 'TV_aj',
    'дишати': 'ɛ_root_stress', #invented infin

    'надихати': 'TV_aj',
    'надишати': 'ɛ_root_stress',  #invented infin

    'метати': 'TV_áj',
    'мечти': 'ɛ_mixed_stress',


    'назвати': 'ɛ_sffx_stress',
    'назовати': 'ɛ_sffx_stress',




     


}

no_stress_monosyllabic_forms = { #here are also reflexives; added stress manually in data.manual
    'гнути': 'ɛ_sffx_stress',
    'ждати': 'ɛ_sffx_stress',
    'ткати': 'ɛ_sffx_stress',
    'тяти': 'ɛ_sffx_stress',
    'йти': 'ɛ_sffx_stress',
    'вмерти': 'ɛ_sffx_stress',
    'вчити': 'e_sffx_stress',
    'вчитися': 'e_sffx_stress',
    'жати': 'ɛ_sffx_stress',
    'йняти': 'ɛ_sffx_stress',
    "м'яти": 'ɛ_sffx_stress',
    'здріти': 'e_sffx_stress',
    'мерти': 'ɛ_sffx_stress',
    'мстити': 'e_sffx_stress',
    'мститися': 'e_sffx_stress',
    'перти': 'ɛ_sffx_stress',
    'спати': 'e_sffx_stress',
    'ссати': 'ɛ_sffx_stress',
    'терти': 'ɛ_sffx_stress',
    'тнути': 'ɛ_sffx_stress',
    'чхнути': 'ɛ_sffx_stress',
    'лити': 'ɛ_sffx_stress', 
    'вбити': 'ɛ_sffx_stress', #also apostrophe
    'пити': 'ɛ_sffx_stress', #also apostrophe
    'снитися': 'e_sffx_stress',
    'звати': 'ɛ_sffx_stress',

}

incorrect_paradigm_match = {
    'дути': 'ɛ_sffx_stress',    #assignes TV_új,maybe add a condition about the past tense form? or infinitive?
    'кувати': 'ɛ_sffx_stress',  #todo: add to sffx uJ'ɛ



}

problematic = {                
    # 'бити': 'ɛ_sffx_stress',     #apostrophe
    'хотіти': 'ɛ_root_stress', #second forms for 2sg and 2pl not cut. why?


}

migrated_to_class1 = {          #palatalizer in 1sg and 3pl, but class I sffxes
    'бороти': 'ɛ_mixed_stress_for_migrated_class2_roots',
    'боротися': 'ɛ_mixed_stress_for_migrated_class2_roots',
    'вибороти': 'ɛ_root_stress_for_migrated_class2_roots',
    'перебороти': 'ɛ_mixed_stress_for_migrated_class2_roots',
    'побороти': 'ɛ_mixed_stress_for_migrated_class2_roots',
    'колоти': 'ɛ_mixed_stress_for_migrated_class2_roots',
    'розколоти': 'ɛ_mixed_stress_for_migrated_class2_roots',
    'молоти': 'ɛ_mixed_stress_for_migrated_class2_roots',
    'полоти': 'ɛ_mixed_stress_for_migrated_class2_roots',
    'пороти': 'ɛ_mixed_stress_for_migrated_class2_roots',

    
    'слати': 'ɛ_sffx_stress_for_migrated_class2_roots',
    'надіслати': 'ɛ_sffx_stress_for_migrated_class2_roots',
    'послати': 'ɛ_mixed_stress_for_migrated_class2_roots', #see 1.8

    'сипати': 'ɛ_root_stress_for_migrated_class2_roots',

}

no_stress = {
    'інформатизувати': 'TV_új',
}