aruku_mannual = {
    ('aruku'): ['aru', 'ku'], 
    ('arukimas'): ['aru', 'ki', 'mas'], 
    ('arukanai'): ['aru', 'ka', 'nai'], 
    ('arukimasen'): ['aru', 'ki', 'mase', 'n'], 
    ('aruita'): ['aru', 'i', 'ta'], 
    ('arukimasita'): ['aru', 'ki', 'masi', 'ta'], 
    ('arukumai'): ['aru', 'ku', 'mai'], 
    ('arukimasmai'): ['aru', 'ki', 'mas', 'mai'], 
    ('aruite'): ['aru', 'i', 'te'], 
    ('arukimasite'): ['aru', 'ki', 'masi', 'te'], 
    ('arukanakute'): ['aru', 'ka', 'naku', 'te'], 
    ('aruki'): ['aru', 'ki'], 
    ('aruke'): ['aru', 'ke'], 
    ('arukinasai'): ['aru', 'ki', 'nasa', 'i'], 
    ('arukuna'): ['aru', 'ku', 'na'], 
    ('arukeba'): ['aru', 'ke', 'ba'], 
    ('arukanakereba'): ['aru', 'ka', 'na', 'kere', 'ba'], 
    ('aruitara'): ['aru', 'i', 'tara'], 
    ('arukimasitara'): ['aru', 'ki', 'masi', 'ta', 'ra'],
    ('aruitari'): ['aru', 'i', 'tari'], 
    ('arukimasitari'): ['aru', 'ki', 'masi', 'tari'], 
    ('arukeru'): ['aru', 'ke', 'ru'], 
    ('arukareru'): ['aru', 'ka', 'reru'], 
    ('arukaseru'): ['aru', 'ka', 'se', 'ru'], 
    ('arukaserareru'): ['aru', 'ka', 'se', 'rare', 'ru']
}

kuru_mannual = {
('kuru'): ['ku', 'ru'], 
('kimas'): ['ki', 'mas'], 
('konai'): ['ko', 'nai'], 
('kimasen'): ['ki', 'mase', 'n'], 
('kita'): ['ki', 'ta'], 
('kimasita'): ['ki', 'masi', 'ta'], 
('kurumai'): ['kuru', 'mai'], 
('kimasmai'): ['ki', 'mas', 'mai'], 
('kite'): ['ki', 'te'], 
('kimasite'): ['ki', 'masi', 'te'], 
('konakute'): ['ko', 'naku', 'te'], 
('ki'): ['ki'], 
('kore'): ['kore'], 
('kinasai'): ['ki', 'nasa', 'i'], 
('kuruna'): ['kuru', 'na'], 
('kureba'): ['kure', 'ba'], 
('konakereba'): ['ko', 'na', 'kere', 'ba'], 
('kitara'): ['ki', 'tara'], 
('kimasitara'): ['ki', 'masi', 'ta', 'ra'],
('kitari'): ['ki', 'tari'], 
('kimasitari'): ['ki', 'masi', 'tari'], 
('kureba'): ['kure', 'ba'], 
('konakereba'): ['ko', 'na', 'kere', 'ba'], 
('korareru'): ['ko', 'rare', 'ru'], 
('korareru'): ['ko', 'rare', 'ru'], 
('kosaseru'): ['ko', 'sase', 'ru'], 
('kosaserareru'): ['ko', 'sase', 'rare', 'ru'], 
('iraʃʃaru'): ['iraʃʃa', 'ru'], 
('mairu'): ['mai', 'ru']
}

souzi_mannual = {
('so:dgisuru'): ['so:dgi', 'suru'], 
('so:dgisimas'): ['so:dgi', 'si', 'mas'], 
('so:dgisinai'): ['so:dgi', 'si', 'nai'], 
('so:dgisimasen'): ['so:dgi', 'si', 'mase', 'n'], 
('so:dgisita'): ['so:dgi', 'si', 'ta'], 
('so:dgisimasita'): ['so:dgi', 'si', 'masi', 'ta'], 
('so:dgisijo:'): ['so:dgi', 'si', 'jo:'], 
('so:dgisurumai'): ['so:dgi', 'suru', 'mai'], 
('so:dgisimasmai'): ['so:dgi', 'si', 'mas', 'mai'], 
('so:dgisite'): ['so:dgi', 'si', 'te'], 
('so:dgisimasite'): ['so:dgi', 'si', 'masi', 'te'], 
('so:dgisinakute'): ['so:dgi', 'si', 'naku', 'te'], 
('so:dgisi'): ['so:dgi', 'si'], 
('so:dgisinasai'): ['so:dgi', 'si', 'nasa', 'i'], 
('so:dgisuruna'): ['so:dgi', 'suru', 'na'], 
('so:dgisureba'): ['so:dgi', 'sure', 'ba'], 
('so:dgisinakereba'): ['so:dgi', 'si', 'nake', 're', 'ba'], 
('so:dgisitara'): ['so:dgi', 'si', 'tara'], 
('so:dgisimasitara'): ['so:dgi', 'si', 'masi', 'ta', 'ra'], 
('so:dgisitari'): ['so:dgi', 'si', 'tari'], 
('so:dgisimasitari'): ['so:dgi', 'si', 'masi', 'tari'], 
('so:dgidekiru'): ['so:dgi', 'deki', 'ru'], 
('so:dgisareru'): ['so:dgi', 'sare', 'ru'], 
('so:dgisaseru'): ['so:dgi', 'sa', 'seru'], 
('so:dgisaserareru'): ['so:dgi', 'sase', 'rare', 'ru'], 
('so:dginasaru'): ['so:dgi', 'nasa', 'ru'], 
}

taberu_mannual = {
('taberu'): ['ta','beru'], 
('tabemas'): ['ta','be','mas'],
('tabenai'): ['ta','be','nai'], 
('tabemasen'): ['ta','be','mase','n'], 
('tabeta'): ['ta','be','ta'], 
('tabemasita'): ['ta','be','masi','ta'], 
('tabejo:'): ['ta','be','jo:'], 
('tabemai'): ['ta','be','mai'], 
('tabemasmai'): ['ta','be','mas','mai'], 
('tabete'): ['ta','be','te'], 
('tabemasite'): ['ta','be','masi','te'], 
('tabenakute'): ['ta','be','naku','te'], 
('tabe'): ['ta','be'], 
('tabenasai'): ['ta','be','nasa','i'], 
('taberuna'): ['ta','beru','na'], 
('tabereba'): ['ta','bere','ba'], 
('tabenakereba'): ['ta','be','na', 'kere','ba'], 
('tabetara'): ['ta','be','tara'], 
('tabemasitara'): ['ta','be','masi','ta', 'ra'], 
('tabetari'): ['ta','be','tari'], 
('tabemasitari'): ['ta','be','masi','tari'], 
('taberareru'): ['ta','be','ra','reru'], 
('taberareru'): ['ta','be','ra','reru'], 
('tabesaseru'): ['ta','be','sase','ru'], 
('tabesaserareru'): ['ta','be','sase','ra','reru'], 
('mesiagaru'): ['mesiaga', 'ru'], 
('itadaku'): ['itada', 'ku']
}

verb_error={
    ('arukimaʃjo'): ['aru', 'ki', 'mas', 'jo:'], #the ending s of 'mas' and the first sound of 'jo:(or jou)', together, change the sound into ʃj  
    ('so:dgisimaʃjo'): ['so:dgi', 'si', 'mas', 'jo:'], #same as the first one
    ('tabemaʃjo'): ['ta','be','mas','jo:'], #same
    ('so:dgisoi'): ['so:dgi', 'siro'], #scraper giving wrong form, it should be 'so:dgisiro' 
    ('so:dgi:tasu'): ['so:dgi', 'ita', 'su'],#double vowel(ii) makes long vowel(i:), but in this case, each i belongs to different morphemes 
    ('aruko:'): ['aru', 'ko',  'u'], #double vowel(ou) makes long vowel(o:) but each vowel belongs to different morphems, 'ko' and 'u'

    ('arukanakatta'): ['aru', 'ka', 'naka', 'tta'], #tta is variant of ta. the sound changes when it follows after naka.
    ('arukanakattara'): ['aru', 'ka', 'naka', 'tta', 'ra'], #same
    ('arukanakattari'): ['aru', 'ka', 'naka', 'ttari'], #ttari is variant of tari. same reason.
    ('so:dgisinakatta'): ['so:dgi', 'si', 'naka', 'tta'], #same
    ('so:dgisinakattara'): ['so:dgi', 'si', 'naka', 'tta', 'ra'], #same
    ('so:dgisinakattari'): ['so:dgi', 'si', 'naka', 'ttari'], #same
    ('tabenakatta'): ['ta','be','naka','tta'], #same
    ('tabenakattara'): ['ta','be','naka','tta', 'ra'], #same
    ('tabenakattari'): ['ta','be','naka','ttari'], #same
}

