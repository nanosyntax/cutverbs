from tests.utils_for_test import cz

from scraper.sketch_paradigms.cztenten import SketchParadigmsCz

sketchcz = SketchParadigmsCz()

def test_u_i():
    paradigm = cz.paradigm_for('doporučovat')
    assert paradigm[('1', 'sg')] == 'doporutšuju'

def test_u_i_sketch(): 
    paradigm = sketchcz.cached_paradigm('vokalizovat')
    assert paradigm['finite'][('1', 'sg')] == 'vokalizuju' # testing whether sketchengine correctly parses 1sg as -uju instead of -uji