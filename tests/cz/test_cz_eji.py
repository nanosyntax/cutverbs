from tests.utils_for_test import cz, verb_stem_given_paradigm_named, guess_one_verb_cz
from tests.data.cz.eji_verbs   import eji_no_pal, eji_j_epen, eji_ě, eji_m_palatalisation, eji_palatalised_root

def test_root_derives():
    roots_dict = {
        ('1', 'sg'): 'tsiv',
        ('2', 'sg'): 'tsiv',
        ('3', 'sg'): 'tsiv',
        ('1', 'pl'): 'tsiv',
        ('2', 'pl'): 'tsiv',
        ('3', 'pl'): 'tsivj'}
    conjugated_verb = cz.paradigm_for('civět')
    paradigm = cz.paradigms['eji']
    resu = cz.guesser.can_root_derive_paradigm('tsiv', roots_dict, paradigm, conjugated_verb, cz)
    assert resu

def test_match_eji():
    stem = verb_stem_given_paradigm_named('civět', 'eji', cz)
    assert stem == 'tsiv'

def test_cz_eji_no_pal():
    for verb, expected_paradigm in eji_no_pal.items():
        guessed_paradigm = guess_one_verb_cz(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_cz_eji_j_epen():
    for verb, expected_paradigm in eji_j_epen.items():
        guessed_paradigm = guess_one_verb_cz(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_cz_eji_ě():
    for verb, expected_paradigm in eji_ě.items():
        guessed_paradigm = guess_one_verb_cz(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_cz_eji_m_palatalisation():
    for verb, expected_paradigm in eji_m_palatalisation.items():
        guessed_paradigm = guess_one_verb_cz(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_cz_eji_palatalised_root():
    for verb, expected_paradigm in eji_palatalised_root.items():
        guessed_paradigm = guess_one_verb_cz(verb)
        assert guessed_paradigm == expected_paradigm, verb
