from tests.utils_for_test import guess_one_verb_cz
from tests.data.cz.ai_verbs   import a, i

def test_a_verbs():
    for verb, expected_paradigm in a.items():
        guessed_paradigm = guess_one_verb_cz(verb)
        assert guessed_paradigm == expected_paradigm

def test_i_verbs():
    for verb, expected_paradigm in i.items():
        guessed_paradigm = guess_one_verb_cz(verb)
        assert guessed_paradigm == expected_paradigm, verb
