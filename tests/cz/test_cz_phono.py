from phono.cz.phono import apply_phonology
from diastring      import DiaString

def phono(left, right):
    return apply_phonology(DiaString(left), DiaString(right))

def test_edge_palatalisation():
    assert phono('tsiv',    'ᐞeji:') == 'tsivjeji:'
    assert phono('xib',     'ᐞeji:') == 'xibjeji:'
    assert phono('rozta:p', 'ᐞeji:') == 'rozta:pjeji:'

    assert phono('tšum',    'ᐞeji:') == 'tšumnjeji:'
    