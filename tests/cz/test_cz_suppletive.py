from tests.utils_for_test import guess_one_verb_cz
from tests.data.cz.suppletive import suppletive, lukas_not_working_verbs, ondrej_verbs_no_working

def test_cz_suppletive():
    for verb, expected_paradigm in suppletive.items():
        guessed_paradigm = guess_one_verb_cz(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_lukas_not_working_verbs():
    for verb, expected_paradigm in lukas_not_working_verbs.items():
        guessed_paradigm = guess_one_verb_cz(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_ondrej_verbs_no_working():
    for verb, expected_paradigm in ondrej_verbs_no_working.items():
        guessed_paradigm = guess_one_verb_cz(verb)
        assert guessed_paradigm == expected_paradigm, verb