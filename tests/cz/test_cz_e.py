from tests.utils_for_test import cz, guess_one_verb_cz, verb_stem_given_paradigm_named

from tests.data.cz.e_verbs import e, e_n, e_j_epenthesis, e_uju

def test_match_e_uju():
    stem = verb_stem_given_paradigm_named('doporučovat', 'e_uj', cz)
    assert stem == 'doporutš'

def test_cz_e():
    for verb, expected_paradigm in e.items():
        guessed_paradigm = guess_one_verb_cz(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_cz_e_uju():
    for verb, expected_paradigm in e_uju.items():
        guessed_paradigm = guess_one_verb_cz(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_cz_e_n():
    for verb, expected_paradigm in e_n.items():
        guessed_paradigm = guess_one_verb_cz(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_cz_e_j():
    for verb, expected_paradigm in e_j_epenthesis.items():
        guessed_paradigm = guess_one_verb_cz(verb)
        assert guessed_paradigm == expected_paradigm, verb
