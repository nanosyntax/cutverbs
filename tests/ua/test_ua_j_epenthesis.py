from phono.ua.phono import j_epenthesis

def do_j_epenthesis(left, right):
    left, right = j_epenthesis(left, right)
    return left + right

def test_j_epenthesis():
    assert do_j_epenthesis('a', 'u') == 'aju' #koxaju
    assert do_j_epenthesis('a', 'ɛ') == 'ajɛ' #koxajɛ
    assert do_j_epenthesis('u', 'u') == 'uju' #prohramuju
    assert do_j_epenthesis('u', 'ɛ') == 'ujɛ' #prohramujɛ
    assert do_j_epenthesis('i', 'u') == 'iju' #xvoriju
    assert do_j_epenthesis('i', 'ɛ') == 'ijɛ' #xvorijɛ

    assert do_j_epenthesis('e', 'u') == 'eju' #meju
    assert do_j_epenthesis('e', 'ɛ') == 'ejɛ' #mejɛ

    assert do_j_epenthesis('o', 'u') == 'oju' #poju
    assert do_j_epenthesis('o', 'i') == 'oji' #pojiš
    assert do_j_epenthesis('o', 'a') == 'oja' #pojatj
    assert do_j_epenthesis('ɛ', 'u') == 'ɛju' #klɛju
    assert do_j_epenthesis('ɛ', 'i') == 'ɛji' #klɛjiš
    assert do_j_epenthesis('ɛ', 'a') == 'ɛja' #klɛjatj

    assert do_j_epenthesis('o', '→u') == 'oj→u' #stoj'u
    assert do_j_epenthesis('o', '→i') == 'oj→i' #stoj'iš
    assert do_j_epenthesis('o', '→a') == 'oj→a' #stoj'atj

    assert do_j_epenthesis('u', '→u') == 'uj→u' #kuj'u
    assert do_j_epenthesis('u', '→ɛ') == 'uj→ɛ' #kuj'ɛ

    assert do_j_epenthesis('o', '←i') == 'oj←i' #d'ojiš
    assert do_j_epenthesis('o', '←a') == 'oj←a' #d'ojatj
