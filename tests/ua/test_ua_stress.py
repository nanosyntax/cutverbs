from cutverbs.language import Language
from tests.data.ua.conjugated_verbs import ipa_pidpesate, ipa_dušete, ipa_plakate, ipa_pɛkte, ipa_bačete, ipa_učete, ipa_varete, ipa_ljubete
ua = Language('ua')

def test_ɛ_mixed_stress():
    assert ua.paradigm_for('підписати') == ipa_pidpesate

def test_e_mixed_stress():
    assert ua.paradigm_for('душити') == ipa_dušete

def test_e_mixed_stress_and_palatalizer():
    assert ua.paradigm_for('варити') == ipa_varete

def test_e_mixed_stress_and_palatalizer_and_l_insertion():
    assert ua.paradigm_for('любити') == ipa_ljubete

# def test_ɛ_root_stress():
#     assert ua.paradigm_for('плакати') == ipa_plakate

# def test_e_root_stress():
#     assert ua.paradigm_for('бачити') == ipa_bačete

def test_ɛ_sffx_stress():
    assert ua.paradigm_for('пекти') == ipa_pɛkte

def test_e_sffx_stress():
    assert ua.paradigm_for('учити') == ipa_učete
    