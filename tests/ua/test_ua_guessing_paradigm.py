from scraper.goroh import Goroh
from tests.data.ua.verbs_to_conjugation_classes import ɛ_root_stress, ɛ_sffx_stress, tv_aj, tv_uj, reflexives, reflexives_1pl, reflexives_problematic, reflexives_stressed_jɛ, irregular, e_mixed_stress, e_root_stress, ɛ_mixed_stress, double_stress_pattern, double_forms, no_stress_monosyllabic_forms, incorrect_paradigm_match, problematic, migrated_to_class1, no_stress
from cutverbs.language import Language
ua = Language('ua')

## high level tests:  match a conjugated verb with aall paradigm types that we know,
#                     and check that we get the right winner conjugation-type

def guess_one_verb_ua(verb):
    return ua.guess_paradigm(verb) # uses the new stress encoding

def test_ɛ_sffx_stress():
    for verb, expected_paradigm in ɛ_sffx_stress.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_ɛ_root_stress():
    for verb, expected_paradigm in ɛ_root_stress.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_reflexives():
    for verb, expected_paradigm in reflexives.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_reflexives_1pl():
    for verb, expected_paradigm in reflexives_1pl.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_reflexives_problematic():
    for verb, expected_paradigm in reflexives_problematic.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_reflexives_stressed_jɛ():
    for verb, expected_paradigm in reflexives_stressed_jɛ.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_tv_aj():
    for verb, expected_paradigm in tv_aj.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_tv_uj():
    for verb, expected_paradigm in tv_uj.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_irregular():
    for verb, expected_paradigm in irregular.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_e_mixed_stress():
    for verb, expected_paradigm in e_mixed_stress.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_e_root_stress():
    for verb, expected_paradigm in e_root_stress.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_ɛ_mixed_stress():
    for verb, expected_paradigm in ɛ_mixed_stress.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_double_stress_pattern():
    for verb, expected_paradigm in double_stress_pattern.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_double_forms():
    for verb, expected_paradigm in double_forms.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_no_stress_monosyllabic_forms():
    for verb, expected_paradigm in no_stress_monosyllabic_forms.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_incorrect_paradigm_match():
    for verb, expected_paradigm in incorrect_paradigm_match.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_problematic():
    for verb, expected_paradigm in problematic.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_migrated_to_class1():
    for verb, expected_paradigm in migrated_to_class1.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb

def test_no_stress():
    for verb, expected_paradigm in no_stress.items():
        guessed_paradigm = guess_one_verb_ua(verb)
        assert guessed_paradigm == expected_paradigm, verb