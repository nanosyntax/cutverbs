from tests.data.ua.verbs_to_conjugation_classes import ɛ_root_stress, ɛ_sffx_stress#, tv_uj, reflexives, irregular, e_mixed_stress, e_root_stress, ɛ_mixed_stress
from cutverbs.language import Language
ua = Language('ua')

## lower level tests: match a conjugated verb with only one paradigm type, 
##                    and check that the outcome of the match is what we expect

def test_ɛ_sffx_stress_matches():
    verb, expected_paradigm_name = list(ɛ_sffx_stress.items())[0]
    conjugated_verb = ua.paradigm_for(verb)
    expected_paradigm = ua.paradigms[expected_paradigm_name]
    stem = ua.guesser.belongs_to_paradigm(conjugated_verb, expected_paradigm, ua)
    assert stem is not None

