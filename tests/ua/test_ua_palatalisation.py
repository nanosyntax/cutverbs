from phono.ua.phono import palatalize_strongly, palatalize_weakly
from diastring import DiaString

def do_weak_palatalize(left, right):
    right = DiaString(right)
    left, right = palatalize_weakly (left, right)
    return left + right

def do_strong_palatalize(left, right):
    right = DiaString(right)
    left, right = palatalize_strongly(left, right)
    return left + right


def test_weak_palatalisation():
    assert do_weak_palatalize('r', '^') == 'rj'   #pozázdrjatʲ
    assert do_weak_palatalize('n', '^') == 'nj'   #boronjatʲ
    assert do_weak_palatalize('l', '^') == 'lj'   #stavljatʲ

    assert do_weak_palatalize('p', '^') == 'plj'  #sepljatʲ
    assert do_weak_palatalize('b', '^') == 'blj'  #robljatʲ
    assert do_weak_palatalize('m', '^') == 'mlj'  #ekonomljatʲ
    assert do_weak_palatalize('v', '^') == 'vlj'  #trávljatʲ

    assert do_weak_palatalize('d', '^') == 'dj'   #xódjatʲ
    assert do_weak_palatalize('t', '^') == 'tj'   #vélɛtjatʲ
    assert do_weak_palatalize('z', '^') == 'zj'   #morozjatʲ
    assert do_weak_palatalize('s', '^') == 'sj'   #hasjatʲ
    assert do_weak_palatalize('st', '^') == 'stj' #mstjatʲ
    assert do_weak_palatalize('zd', '^') == 'zdj' #jizdjatʲ
    
def test_strong_palatalisation():
    assert do_strong_palatalize('r', 'ᐞ') == 'rj'   #pozázdrju 
    assert do_strong_palatalize('n', 'ᐞ') == 'nj'   #boronju
    assert do_strong_palatalize('l', 'ᐞ') == 'lj'   #stavlju

    assert do_strong_palatalize('p', 'ᐞ') == 'plj'  #seplju
    assert do_strong_palatalize('b', 'ᐞ') == 'blj'  #roblju
    assert do_strong_palatalize('m', 'ᐞ') == 'mlj'  #ekonomlju
    assert do_strong_palatalize('v', 'ᐞ') == 'vlj'  #trávlju

    assert do_strong_palatalize('d', 'ᐞ') == 'dž'   #xodžu
    assert do_strong_palatalize('t', 'ᐞ') == 'č'    #vélɛču
    assert do_strong_palatalize('z', 'ᐞ') == 'ž'    #morožu
    assert do_strong_palatalize('s', 'ᐞ') == 'š'    #hašu
    assert do_strong_palatalize('st', 'ᐞ') == 'šč'   #mšču
    assert do_strong_palatalize('zd', 'ᐞ') == 'ždž'  #jiždžu