from scraper.goroh import Goroh, disambiguate
goroh = Goroh()

def test_goroh_reflexives_stripping():
    paradigm = goroh.cached_paradigm('цікавитися')
    assert paradigm['finite'][('3', 'sg')] == 'ціка́вить'

    paradigm = goroh.cached_paradigm('вдаватися')
    assert paradigm['finite'][('3', 'sg')] == 'вдає́'

def test_goroh_imperative_disambiguation():
    ff, tense = ('2', 'sg'), ['imperative']
    ambiguous = ['продава́й', ' продава́й-но']
    assert disambiguate(ambiguous, ff, tense) == 'продава́й'
    ambiguous = [' продава́й-но', 'продава́й']
    assert disambiguate(ambiguous, ff, tense) == 'продава́й'