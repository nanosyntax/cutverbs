
from utils import flatten_paradigm
from tests.utils_for_test import check_parses_for_paradigm

from scraper.verbix_jp import get_paradigm
from ipa.jp import paradigm_to_ipa

from tests.data.jp.norika import aruku_mannual, kuru_mannual, souzi_mannual, taberu_mannual, verb_error
from suffixes.jp.verbs import suffixes
jp_suffixes = list(suffixes.values())

def get_jp_verb(verb):
    kanji_paradigm = get_paradigm(verb)
    flat_kanji_paradigm = flatten_paradigm(kanji_paradigm)
    flat_ipa_paradigm = paradigm_to_ipa(flat_kanji_paradigm)
    return flat_ipa_paradigm


def test_aruku():
    ipa_paradigm  = get_jp_verb('aruku')
    check_parses_for_paradigm(ipa_paradigm, aruku_mannual, jp_suffixes)

def test_taberu():
    ipa_paradigm  = get_jp_verb('taberu')
    check_parses_for_paradigm(ipa_paradigm, taberu_mannual, jp_suffixes)

def test_kuru():
    ipa_paradigm  = get_jp_verb('kuru')
    check_parses_for_paradigm(ipa_paradigm, kuru_mannual, jp_suffixes)

def test_souzi():
    ipa_paradigm  = get_jp_verb('souzisuru')
    check_parses_for_paradigm(ipa_paradigm, souzi_mannual, jp_suffixes)

