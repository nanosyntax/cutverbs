import importlib

from scraper.prirucka import Prirucka
from ipa.cz import paradigm_to_ipa
from parse import all_nonempty_parses # parse_all

from cutverbs.language import Language
cz = Language('cz')
prirucka = Prirucka()

def verb_stem_given_paradigm_named(verb, expected_paradigm_name, lg):
    conjugated_verb = lg.paradigm_for(verb)
    expected_paradigm = lg.paradigms[expected_paradigm_name]
    stem = lg.guesser.belongs_to_paradigm(conjugated_verb, expected_paradigm, lg)
    return stem

def guess_one_verb_cz(verb):
    conjugated_verb = cz.paradigm_for(verb)
    return cz.guess_paradigm(conjugated_verb, cz.paradigms)

def check_parses_for_paradigm(ipa_paradigm, expected_parses, suffixes):
    for conjugated_form in ipa_paradigm.values():
        all_parses = all_nonempty_parses(conjugated_form, suffixes)
        expected_parse = expected_parses.get(conjugated_form)
        if not expected_parse:
            print('  ****  missing expected parse for: ' + conjugated_form)
            continue
            # raise ValueError('missing expected parse for ' + conjugated_form)
        # now check if any of the parses is correct
        correct_parse = None
        for parse in all_parses:
            if parse == expected_parse:
                correct_parse = parse
                break
        # make errors more helpful: show all parses if none matched
        if correct_parse is None:
            if type(all_parses) == list and len(all_parses) == 1:
                correct_parse = all_parses[0]
            else:
                correct_parse = all_parses
        assert correct_parse == expected_parse
