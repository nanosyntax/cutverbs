from diastring import DiaString
import copy

def test_diastring_cp():
    ds = DiaString('ᐞabc')
    clone = copy.deepcopy(ds)
    assert clone.diacritics == ['ᐞ']
    assert clone == 'abc'
