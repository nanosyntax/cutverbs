from utils import flatten_paradigm
from tests.utils_for_test import check_parses_for_paradigm

from scraper.verbix_tk import get_paradigm
from ipa.tk import paradigm_to_ipa

from tests.data.tk.sabri import bulmak_parsed

from suffixes.tk.verbs import suffixes
tk_suffixes = list(suffixes.values())


def get_ipa_paradigm(verb):
    surface_paradigm = get_paradigm(verb)
    flat_surface_paradigm = flatten_paradigm(surface_paradigm)
    flat_ipa_paradigm = paradigm_to_ipa(flat_surface_paradigm)
    return flat_ipa_paradigm


def test_bulmak():
    ipa_paradigm  = get_ipa_paradigm('bulmak')
    check_parses_for_paradigm(ipa_paradigm, bulmak_parsed, tk_suffixes)
