class DiaString(str):

    # allows diacritics only at the beginning of the string
    def __new__(cls, s, diacritics=None): # diacritics will be used only when doing deepcopy
        i = 0
        if diacritics is None:
            diacritics = []
            while i < len(s) and s[i] in '^ᐞ²': # →←
                diacritics.append(s[i])
                i += 1
        ds = super().__new__(cls, s[i:])
        ds.diacritics = diacritics
        return ds

    # version which allows diacritics anywhere, but loses the position of the diacritic!
    def __new__anywhere(cls, s, diacritics=None): # diacritics will be used only when doing deepcopy
        i = 0
        if diacritics is None:
            diacritics = []
            no_diacritics = ''
            for c in s:
                if c in '^ᐞ²→←':    
                    diacritics.append(c)
                else:               no_diacritics += c
        ds = super().__new__(cls, no_diacritics)
        ds.diacritics = diacritics
        return ds

    def __getnewargs__(self):
        # Return the arguments that *must* be passed to __new__ (so that deepcopy knows to pass them)
        # https://stackoverflow.com/questions/46283738/attributeerror-when-using-python-deepcopy
        return (str(self), self.diacritics,)


# monkey-patch string with diacritics() method:
#   https://gist.github.com/bricef/1b0389ee89bd5b55113c7f3f3d6394ae