
def parse(stem, suffixes):
    for suffix in suffixes:
        result = parse_with_suffix(stem, suffix, suffixes)
        if result:
            return result

def parse_all(stem, suffixes): # experimental, needs testing
    parses = _all_parses_of_stem(stem, [], suffixes)
    return parses if parses else []

# remove empty first morpheme in Japanese
def all_nonempty_parses(stem, suffixes):
    parses = parse_all(stem, suffixes)
    without_empty_roots = []
    for parse in parses:
        clean = parse[1:] if parse[0] == '' else parse
        without_empty_roots.append(clean)
    return without_empty_roots

# --- private

def parse_with_suffix(stem, suffix, suffixes):
    if not stem.endswith(suffix):
        return None
    s_len = len(suffix)
    new_stem = stem[:-s_len]
    parsed_new_stem = parse(new_stem, suffixes)
    if parsed_new_stem:
        parsed_new_stem.append(suffix)
        return parsed_new_stem
    else:
        return [new_stem, suffix]

## --- private parse_all implementation

def _all_parses_of_stem(stem, parsed, suffixes):
    stem_parses = []
    for suffix in suffixes:
        results = _all_parses_with_suffix(stem, suffix, parsed, suffixes)
        if results:
            stem_parses = stem_parses + results
    return stem_parses or None

def _all_parses_with_suffix(stem, suffix, parsed, suffixes):
    if not stem.endswith(suffix):
        return None
    s_len = len(suffix)
    new_stem = stem[:-s_len]
    parsed_suffixes = [suffix] + parsed
    parses_of_new_stem = _all_parses_of_stem(new_stem, parsed_suffixes, suffixes)
    if parses_of_new_stem:
        return parses_of_new_stem
    else:
        return [ [new_stem] + parsed_suffixes ]
