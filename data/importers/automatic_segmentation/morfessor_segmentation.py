import morfessor
from pathlib import Path

io = morfessor.MorfessorIO()

train_data = list(io.read_corpus_file('C:\\Users\\horak\\Documents\\cutverbs\\data\\importers\\automatic_segmentation\\training_set_ipa'))

model =  morfessor.BaselineModel()
model.load_data(train_data)
model.train_batch()

io.write_segmentation_file('segmentations_ipa', model.get_segmentations())