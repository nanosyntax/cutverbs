from pathlib import Path
from morphynet_full import data
from pprint import pprint

"""
data_dir = Path('cutverbs', 'data', 'paradigms', 'morphynet')
for file_path in data_dir.glob('*'):
    print(1)
    with(open(file_path, 'r+', encoding='utf-8')) as f:
        print(f.readlines())
        print(2)
"""
found_suffix_sets = {}
errors = []
stats = {}

def suffix_dicts_are_same(dict1, dict2):
    features = [('1', 'pl'), ('2', 'pl'), ('3', 'pl'), ('1', 'sg'), ('2', 'sg'), ('3', 'sg')]
    for feature in features:
        if dict1[feature] != dict2[feature]:
            return False
    return True

for infinitive, paradigm in data.items():
    match = False
    parsed_paradigm = {k:val[-1] for k, val in paradigm['derivation']['finite'].items()}

    try:
        for paradigm_name, suffix_dict in found_suffix_sets.items():
            if infinitive == 'být' or suffix_dicts_are_same(suffix_dict, parsed_paradigm):
                match = True
                stats[paradigm_name] += 1
        if not match:
            found_suffix_sets[infinitive] = parsed_paradigm
            stats[infinitive] = 1

    except KeyError:
        errors.append(infinitive)
        continue

num_of_full = 0
print(found_suffix_sets)
for inf, suffix_set in found_suffix_sets.items():
    #if '-' not in suffix_set.values():
        print(f'{inf}: {stats[inf]}')
        pprint(suffix_set)

        num_of_full += 1
print(len(found_suffix_sets.values()))
print(num_of_full) #19
print(stats)
"""
for verb in found_suffix_sets:
    for suf in verb.values():
        if len(suf) < 2:
            print(verb)
"""
    
        