from cutverbs.language import Language
from suffixes.cz.verbs import paradigms
from pprint import pprint
from pathlib import Path
import pickle
import re
from copy import deepcopy

cz = Language('cze')
print('loaded')
for key, suf in cz.paradigms.items():
    print(key, suf)


def parse_morphynet_line(line):
    results = {}
    split_line = line.split('\t')
    if len(split_line) == 4:
        #return split_line
        return WordForm(split_line)
    else:
        raise ValueError(f'Number of args in line: {len(split_line)}, arguments {split_line}')

class WordForm:

    unimorph_to_cutverbs = {"IPFV": "imperfective", "PFV": "perfective", "PRS": 'present', "PST": 'past'}

    def __init__(self, split_line):
        self.lemma = split_line[0]
        self.word_from = split_line[1]
        self.tag = split_line[2]
        self.segmentations = split_line[3]
        self.features = self.parse_tag()
        print(self.features)

    def parse_tag(self):
        # problem - different number of tags
        return re.split(";|\|", self.tag)
   
    
    def extract_features_and_derivation(self):
        for i, feature in enumerate(self.features):
            match feature:
                case "V":
                    pass
                case 'IPFV' | 'PFV':
                    if not parsed_verbs[self.lemma]['meta'].get('aspect'):
                        parsed_verbs[self.lemma]['meta']['aspect'] = self.unimorph_to_cutverbs[feature]
                case 'IMP':
                    key = (self.features[i + 2], self.features[i + 3].lower())
                    parsed_verbs[self.lemma]['imperative'][key] = self.word_from
                    parsed_verbs[self.lemma]['derivation']['imperative'][key] = [step.rstrip('\n') for step in self.segmentations.split('|')]
                    break
                case 'IND':
                    key = (self.features[i + 2], self.features[i + 3].lower())
                    parsed_verbs[self.lemma]['finite'][key] = self.word_from
                    parsed_verbs[self.lemma]['derivation']['finite'][key] = [step.rstrip('\n') for step in self.segmentations.split('|')]
                    break
                case 'V.CVB':
                    tense = self.unimorph_to_cutverbs[self.features[i+1]]
                    if i + 3 == len(self.features):
                        key = (self.features[i + 2].lower(),)
                        parsed_verbs[self.lemma]['converb'][tense][key] = self.word_from
                        parsed_verbs[self.lemma]['derivation']['converb'][tense][key] = [step.rstrip('\n') for step in self.segmentations.split('|')]
                    elif self.features[i + 2].find('+') == -1:
                        key = (self.features[i + 2].lower(), self.features[i + 3].lower())
                        parsed_verbs[self.lemma]['converb'][tense][key] = self.word_from
                        parsed_verbs[self.lemma]['derivation']['converb'][tense][key] = [step.rstrip('\n') for step in self.segmentations.split('|')]
                    else:
                        for f in self.features[i + 2].split('+'):
                            key = (f.lower(), self.features[i + 3].lower())
                            parsed_verbs[self.lemma]['converb'][tense][key] = self.word_from
                            parsed_verbs[self.lemma]['derivation']['converb'][tense][key] = [step.rstrip('\n') for step in self.segmentations.split('|')]
                    break
                case 'V.PTCP':
                    if 'PST' in self.features:
                        if self.features[self.features.index('PST') - 1] == 'MASC':
                            key = ('masc', self.features[self.features.index('PST') + 1].lower())
                            parsed_verbs[self.lemma]['participle']['past'][key] = self.word_from
                            parsed_verbs[self.lemma]['derivation']['participle']['past'][key] = [step.rstrip('\n') for step in self.segmentations.split('|')]
                        else:
                            key = tuple([f.lower() for f in self.features[self.features.index('PST') + 1:]])
                            parsed_verbs[self.lemma]['participle']['past'][key] = self.word_from
                            parsed_verbs[self.lemma]['derivation']['participle']['past'][key] = [step.rstrip('\n') for step in self.segmentations.split('|')]
                    elif 'PASS' in self.features:
                            if self.features[self.features.index('PASS') - 1] == 'MASC':
                                key = ('masc', self.features[self.features.index('PASS') + 1].lower())
                                parsed_verbs[self.lemma]['participle']['passive'][key] = self.word_from
                                parsed_verbs[self.lemma]['derivation']['participle']['passive'][key] = [step.rstrip('\n') for step in self.segmentations.split('|')]
                            else:
                                key = tuple([f.lower() for f in self.features[self.features.index('PASS') + 1:]])
                                parsed_verbs[self.lemma]['participle']['passive'][key] = self.word_from
                                parsed_verbs[self.lemma]['derivation']['participle']['passive'][key] = [step.rstrip('\n') for step in self.segmentations.split('|')]
                    break
                case 'SG':
                    self.features.append('SG')
                case _:
                    pass


            


parsed_verbs = {}
with(open('ces.segmentations', 'r', encoding='utf-8')) as f:
    chunk_of_lines = f.readlines()
    last_verb = 0
    for i in range(0, len(chunk_of_lines) ): # verbs end at 73084
        parsed_line = parse_morphynet_line(chunk_of_lines[i])
        if not parsed_line.tag.startswith("V"):
            print(f'Not a verb')
            break
        #elif Path(f'data/paradigms/cze/morphynet/{parsed_line.lemma}').is_file():
            #print(f'Lemma {parsed_line.lemma} already covered')
        elif parsed_verbs.get(parsed_line.lemma):
            print(f'convertng features for {parsed_line.word_from}')
            parsed_line.extract_features_and_derivation()
            last_verb = i
        else:
            parsed_verbs[parsed_line.lemma] = {}
            parsed_verbs[parsed_line.lemma]['infinitive'] = parsed_line.lemma
            parsed_verbs[parsed_line.lemma]['meta'] = {'category': 'v'}
            parsed_verbs[parsed_line.lemma]['finite'] = {}
            parsed_verbs[parsed_line.lemma]['imperative'] = {}
            parsed_verbs[parsed_line.lemma]['participle'] = {}
            parsed_verbs[parsed_line.lemma]['participle']['past'] = {}
            parsed_verbs[parsed_line.lemma]['participle']['passive'] = {}
            parsed_verbs[parsed_line.lemma]['converb']= {}
            parsed_verbs[parsed_line.lemma]['converb']['present'] = {}
            parsed_verbs[parsed_line.lemma]['converb']['past'] = {}

            parsed_verbs[parsed_line.lemma]['derivation'] = {}
            parsed_verbs[parsed_line.lemma]['derivation']['finite'] = {}
            parsed_verbs[parsed_line.lemma]['derivation']['imperative'] = {}
            parsed_verbs[parsed_line.lemma]['derivation']['participle'] = {}
            parsed_verbs[parsed_line.lemma]['derivation']['participle']['past'] = {}
            parsed_verbs[parsed_line.lemma]['derivation']['participle']['passive'] = {}
            parsed_verbs[parsed_line.lemma]['derivation']['converb']= {}
            parsed_verbs[parsed_line.lemma]['derivation']['converb']['present'] = {}
            parsed_verbs[parsed_line.lemma]['derivation']['converb']['past'] = {}
            parsed_line.extract_features_and_derivation()

print('parsed verbs:')
print(parsed_verbs)

for verb in parsed_verbs:
    p = Path('data', 'paradigms', 'cze', 'morphynet', f'{verb}')
    with(open(p, 'w', encoding='utf-8')) as f:
        f.write(str(parsed_verbs[verb]))

with(open('morphynet_full', 'w', encoding='utf-8')) as f:
    f.write(str(parsed_verbs))

        
#with(open(f'/cutverbs/data/cze/morphynet/test.txt'))
#for infinitive, paradigm in parsed_verbs.items():
    #if infinitive != 'být':
        #print(infinitive)
        #print(cz.guess_stem_and_paradigm(paradigm['finite']))
        #print(last_verb) #73082


