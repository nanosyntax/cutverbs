from morphynet_full import data
from cutverbs.language import Language

cz = Language('cze')

known = []
unknown = []
ovat_verbs = []

for verb, paradigm in data.items():
    errors = []
    try:
        guess = cz.guess_stem_and_paradigm(paradigm['finite'])
    except ValueError:
        errors.append(verb)
        continue

    print(verb)
    print(guess)

    if guess[0]:
        known.append(verb)
    elif verb.endswith('ovat'):
        ovat_verbs.append(verb)
    else:
        unknown.append(verb)

print(f'known: {len(known)}')
print(f'unknown: {len(unknown)}')
print(f'ovat verbs: {len(ovat_verbs)}')
print(f'errors: {len(errors)}')
print(known)
