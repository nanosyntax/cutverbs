correspondences = {
    'ie': 'je',
    'ia': 'ja',
    'iu': 'ju',
    'á': 'a:',
    'é': 'e:',
    'í': 'i:',
    'ó': 'o:',
    'ú': 'u:',
    'ý': 'y:',
    'ŕ': 'r:',
    'ĺ': 'l:',
    'ô': 'uo',
    'ä': 'æ',

    "ch":"x",
 
    'c': 'ts',
    'č': 'tš',

    'di': 'dji',
    'ti': 'tji',
    'ni': 'nji',
    'de': 'dje',
    'te': 'tje',
    'ne': 'nje',
    'ď': 'dj',
    'ť': 'tj',
    'ň': 'nj',
    'ľ': 'lj',

    "y": "i",
}

def to_ipa(word):
    for grapheme, phoneme in correspondences.items():
        word = word.replace(grapheme, phoneme)
    return word




from ipa import AbstractIPA
class IPA(AbstractIPA):

    def to_ipa(self, word):
        return to_ipa(word)
