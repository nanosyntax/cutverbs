
class AbstractIPA():

    def to_ipa(self, word):
        raise ValueError('please implement me')
    
    def pre_paradigm_for(self, word_paradigm):
        return word_paradigm

    def post_paradigm_for(self, word_paradigm):
        return word_paradigm
