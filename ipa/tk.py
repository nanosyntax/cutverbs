transliteration_dict = {

    'c' : 'dž',
    'ç' : 'tš',
    'ı' : 'ɯ',
    'r' : 'ɾ',
    'j' : 'ž',
    'y' : 'j',
    'ğ' : ':',
    'ö' : 'œ',
    'ş' : 'š',
    'ü' : 'y'
}


import re
def substitute(text, mapping_dict):
    rgx = '|'.join(mapping_dict.keys())
    replacer = lambda match: mapping_dict[match.group(0)]
    return re.sub(rgx, replacer, text)

def to_ipa(word):
    ipa_word = substitute(word, transliteration_dict)
    return ipa_word

def paradigm_to_ipa(paradigm):
    return {features: to_ipa(ortho) for features, ortho in paradigm.items()}


from ipa import AbstractIPA
class IPA(AbstractIPA):

    def to_ipa(self, word):
        return to_ipa(word)

