correspondences = {
    'с' : 's',
    'б' : 'b',
    'в' : 'v',
    'г' : 'h',
    'ґ' : 'g',
    'д' : 'd',
    'е' : 'ɛ',
    'ж' : 'ž',
    'з' : 'z',
    'и' : 'e',
    'к' : 'k',
    'л' : 'l',
    'м' : 'm',
    'н' : 'n',
    'п' : 'p',
    'р' : 'r',
    'с' : 's',
    'т' : 't',
    'у' : 'u',
    'ф' : 'f',
    'ч' : 'č',
    'ш' : 'š',
    'а' : 'a',
    'і' : 'i',
    'о' : 'o',
    'х' : 'x',
    'ю' : 'ju',
    'я' : 'ja',
    'є' : 'jɛ',
    'ї' : 'ji',
    'ц' : 'ts',
    'щ' : 'šč',
    'й' : 'j',
    'ь' : 'ʲ'}

def to_ipa(word):
    for key, value in correspondences.items():
        word = word.replace(key, value)
    return(word)

def paradigm_to_ipa(paradigm):
    return {features: to_ipa(ortho) for features, ortho in paradigm.items()}



from ipa import AbstractIPA
class IPA(AbstractIPA):

    def to_ipa(self, word):
        return to_ipa(word)

    def pre_paradigm_for(self, word_paradigm):
        word_paradigm = self.adjust_reflexives_1pl(word_paradigm)
        word_paradigm = self.adjust_reflexives_3sg(word_paradigm)
        return word_paradigm
    
    def adjust_reflexives_1pl(self, word_paradigm):
        if word_paradigm[('1', 'pl')].endswith('м'):
            word_paradigm[('1', 'pl')] = word_paradigm[('1', 'pl')] + 'о'   
        return word_paradigm
    
    def adjust_reflexives_3sg(self, word_paradigm):
        if word_paradigm[('3', 'sg')].endswith('є́ть'):
            word_paradigm[('3', 'sg')] = word_paradigm[('3', 'sg')][:-2]
        return word_paradigm
         
       
      



#original:
    # def pre_paradigm_for(self, word_paradigm):
    #     if word_paradigm[('1', 'pl')].endswith('м'):
    #         word_paradigm[('1', 'pl')] = word_paradigm[('1', 'pl')] + 'о'   
    #     return word_paradigm







    # def pre_paradigm_for(self, word_paradigm):
    #     sg1 = word_paradigm[('1', 'sg')] # 'зварю́'
    #     sg2 = word_paradigm[('2', 'sg')] # 'зва́риш'
    #     if sg1 in ['', '—']:
    #         print('TODO: delete empty paradigm')
    #     elif sg1[-2:] in ['ю́', 'у́'] and sg2[-2:] in ['иш', 'еш', 'їш']:  # counter-intutive: accent and char below it are two different characters in Unicode
    #         unstressed_root = sg1[:-2]
    #         stressed_root   = sg2[:-2]
    #         len_root        = len(stressed_root)
    #         segmented = {}
    #         for ff, form in word_paradigm.items():
    #             if ff == ('1', 'sg'):
    #                 segmented[ff] = f'{unstressed_root}→u'
    #             else:
    #                 assert form[:len_root] == stressed_root # beware of allomorphy, etc
    #                 segmented[ff] = f'{unstressed_root}←{form[len_root:]}'
    #         return segmented
    #     elif sg1[-1] == 'ю' or sg1[-1] == 'у' or sg1[-2:] == 'ам':
    #         pass
    #     elif sg1[-2:] in ['ю́', 'у́'] and sg2[-3:] in ['и́ш', 'е́ш', 'є́ш', 'ї́ш']:
    #         root      = sg2[:-3]
    #         len_root  = len(root)
    #         segmented = {}
    #         for ff, form in word_paradigm.items():
    #             # sometimes the 1sg root != others, for phono reasons -- hopefully they're always the same length?
    #             # eg відлечу́, 'відлети́ш'
    #             if   ff == ('1', 'sg'):
    #                 segmented[ff] = f'{form[:-2]}→{form[-2:-1]}' # 'барю́'
    #             elif ff == ('1', 'pl') or ff == ('2', 'pl'):
    #                 # from 2sg up, the root is always the same (unless allomorphy)
    #                 assert form[:len_root] == root # catch allomorphy, etc
    #                 segmented[ff] = f'{root}→{form[len_root:-1]}' # 'баримо́' 'барите́'
    #             else:
    #                 assert form[:len_root] == root # catch allomorphy, etc
    #                 segmented[ff] = f'{root}→{form[len_root:len_root+1]}{form[len_root+2:]}' # 'бари́ш' etc
    #         return segmented
    #     elif sg1[-3:] in ['а́м'] and sg2[-2:] in ['и́']: # 'вдатися' -> вда́м вдаси́
    #         print(f'TODO: {sg1}, {sg2}, of the type вдатися -> вда́м вдаси́')
    #         pass # check what to do here
    #     elif sg1[-2:] in ['їм'] and sg2[-2:] in ['и́']: # 'вдатися' -> вда́м вдаси́
    #         print(f"TODO: {sg1}, {sg2}, of the type з'їсти -> з'їм з'їси́")
    #         pass # check what to do here
    #     elif sg1[-3:] in ['ї́м', 'і́м'] and sg2[-2:] in ['и́']: # 'вдатися' -> вда́м вдаси́
    #         print(f"TODO: {sg1}, {sg2}, of the type наїсти -> наї́м 'наїси́ ")
    #         pass # check what to do here
    #     else:
    #         raise ValueError('unhandled case')
    #     return word_paradigm

    def post_paradigm_for(self, word_paradigm):
        sg1 = word_paradigm[('1', 'sg')]
        sg2 = word_paradigm[('2', 'sg')]
        if sg1 in ['', '—']:
            print('TODO: delete empty paradigm')
        elif sg1[-1:] in ['u']: # leave unchanged
            pass
        elif sg1[-2:] in ['ú'] and sg2[-2:] in ['eš', 'ɛš', 'iš']:  # counter-intutive: accent and char below it are two different characters in Unicode
            segmented = {}
            for ff, form in word_paradigm.items():
                if ff == ('1', 'sg'):
                    segmented[ff] = f'{form[:-2]}→u'
                elif ff == ('2', 'sg'):
                    root = strip_accents(form[:-2])
                    segmented[ff] = f'{root}←{form[-2:]}'
                elif ff == ('3', 'sg') and form[-1:] == 'ɛ':
                    root = strip_accents(form[:-1])
                    segmented[ff] = f'{root}←ɛ'
                else:
                    root = strip_accents(form[:-3])
                    segmented[ff] = f'{root}←{form[-3:]}'
            return segmented
        elif sg1[-2:] in ['ú'] and sg2[-3:] in ['ɛ́š', 'éš', 'íš']: # , 'е́ш', 'є́ш', 'ї́ш' 
            root      = sg2[:-3]
            len_root  = len(root)
            segmented = {}
            for ff, form in word_paradigm.items():
                if   ff == ('1', 'sg'):
                    segmented[ff] = f'{form[:-2]}→u'
                elif ff == ('2', 'sg'):
                    root = form[:-3]
                    segmented[ff] = f'{root}→{strip_accents(form[-3:])}'
                elif ff == ('3', 'sg') and form[-2:] == 'ɛ́':
                    root = form[:-2]
                    segmented[ff] = f'{root}→ɛ'
                else:
                    root = form[:-4]
                    segmented[ff] = f'{root}→{strip_accents(form[-4:])}'
            return segmented
        elif sg1[-3:] in ['ám', 'ím'] and sg2[-2:] in ['é']:
            print(f'TODO: {sg1}, {sg2}, of the type вдатися -> vdám vdasé')
            pass # check what to do here
        elif sg1[-2:] in ['am'] and sg2[-1:] in ['e']:
            print(f'TODO: {sg1}, {sg2}, of the type: védam védase')
            pass # check what to do here
        elif sg1[-2:] in ['am', 'im'] and sg2[-2:] in ['é']:
            print(f'TODO: {sg1}, {sg2}, of the type: dam dasé')
            pass # check what to do here
        else:
            raise ValueError('unhandled case')
        return word_paradigm

# accent_sign = hex(ord('ú'[1]))
def strip_accents(s):
    temp = [c for c in s if c != '\u0301']
    return ''.join([c for c in s if c != '\u0301'])
    # a = ''
    # for c in s:
    #     if c != '\u0301':
    #         a += c
    # return a

# TODO: look into cases such as відпові́м, відповіси́, відпові́сть ??
# TODO: verbs such as вдатися -> вда́м вдаси́