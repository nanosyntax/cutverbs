correspondences = {
    'á': 'a:',
    'é': 'e:',
    'í': 'i:',
    'ó': 'o:',
    'ů': 'u:',
    'ú': 'u:',
    'ý': 'y:',

    "ch":"x",
    "nn":"n",

    'c': 'ts',
    'č': 'tš',

    'di': 'dji',
    'ti': 'tji',
    'ni': 'nji',
    'ď': 'dj',
    'ť': 'tj',
    'ň': 'nj',
    'mě': 'mnje',
    'ě':  'je',

    "y": "i",
}

def to_ipa(word):
    for grapheme, phoneme in correspondences.items():
        word = word.replace(grapheme, phoneme)
    return word

def paradigm_to_ipa(paradigm):
    return {features: to_ipa(ortho) for features, ortho in paradigm.items()}





from ipa import AbstractIPA
class IPA(AbstractIPA):

    def to_ipa(self, word):
        return to_ipa(word)

    # def pre_paradigm_for(self, word_paradigm):
    #     if word_paradigm[('1', 'sg')].endswith('uji') and word_paradigm[('3', 'pl')].endswith('ují'):
    #         word_paradigm[('1', 'sg')] = word_paradigm[('1', 'sg')][:-3] + 'uju'   
    #         word_paradigm[('3', 'pl')] = word_paradigm[('3', 'pl')][:-3] + 'ujou'  
    #     return word_paradigm

        # if word_paradigm.endswith('ovat'):
        #     word_paradigm[('1', 'sg')] = word_paradigm[('1', 'sg')][:-3] + 'uju'   
        #     word_paradigm[('3', 'pl')] = word_paradigm[('3', 'pl')][:-3] + 'ujou'  
        # return word_paradigm