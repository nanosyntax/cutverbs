import requests
from scraper import Scraper
from time import sleep

class SketchBase(Scraper):
    # max 100/mn, 900/h and 2000/day

    def get_tag(self, tag, lemma = None):
        if lemma:
            cql = f'q[tag="{tag}" & lemma="{lemma}"]'
        else:
            cql = f'q[tag="{tag}"]'
        params = self.build_params(cql)
        return self.get(params)

    def get_cql(self, cql, max = None):
        params = self.build_params(cql, max)
        return self.get(params)

    def get(self, params):
        response = requests.get(self.url, params=params, headers=self.headers)
        if response.status_code == 429:
            raise ValueError(
                'Too many requests to SketchEngine -- you must stop for an hour or the day')
        elif response.status_code != 200:
            raise ValueError('Something went wrong with a Sketchengine request...')
        lines = response.json()['Lines']
        words = set()  # use a set to avoid duplicates
        for line in lines:
            word = line['Kwic'][0]['str']
            word = word.lower().strip()  # add .lower() to avoid duplicates with uppercase
            words.add(word)
        sleep(12) # respect quotas and their suggestion for spreading out requests
        return list(words)

    def build_params(self, q, max = None):
        params = self.params.copy()
        params['q'] = q
        if max:
            params['pagesize'] = str(max)
        return params

    def __init__(self, corpus, lg = None, tagset = None):
        self.tagset = tagset
        name = corpus[10:] if corpus.startswith('preloaded/') else corpus
        name = 'sketch_' + name
        super().__init__(name, lg)
        self.headers = {'Authorization': f'Bearer {api_key()}'}
        self.params = {
            'corpname': corpus,
            'format': 'json',
            'viewmode': 'kwic'
        }
        self.url = f'https://api.sketchengine.eu/bonito/run.cgi/view?corpname={corpus}'


# --- private helper functions

def api_key():
    return read_priv_file('sketchengine_key')


def read_priv_file(fname):
    with open(f'private/{fname}', 'r', encoding='utf-8') as f:
        content = f.read().rstrip()
    return content

