import time, random
from urllib.parse import quote_plus
from pathlib      import Path
from bs4          import BeautifulSoup
from playwright.sync_api import sync_playwright


def get_paradigm(word):
    data     = get_html(word)
    return parse_paradigm(data, word)


# ---- PRIVATE ------------------------------------

html_dir = Path('data/external/verbix/tk/html')
html_dir.mkdir(parents=True, exist_ok=True)

def get_html(word):
    file_path = html_dir / word
    if file_path.is_file():
        return file_path.read_text('utf-8')

    time.sleep(random.randint(5, 16))
    escaped_word = quote_plus(word)
    url = f'https://www.verbix.com/webverbix/go.php?&D1=31&T1={escaped_word}' # 'https://www.verbix.com/webverbix/go.php?&D1=31&T1=gitmek'
    html = fetch_remote_html(url)

    cache_original(html, word)
    return html

def cache_original(data, word):
    file_path = html_dir / word  # use nom.sg / 1.sg.pres instead of word?
    with open(file_path, 'w', encoding='utf-8') as f:
        f.write(data)

def fetch_remote_html(url):
    with sync_playwright() as playwright: # hyper slow, creates a browser for each page that we download...
        browser = playwright.chromium.launch(headless=True)
        page = browser.new_page()
        page.goto(url)
        # page.screenshot(path="playw_screenshot.png", full_page=True)
        html = page.content()
        return(html)


def parse_paradigm(html, word):
    bhtml = BeautifulSoup(html) #, 'lxml'
    table = bhtml.find("div", id="verbixConjugations")
    table = table.find('section', class_='verbtable')
    table_elements = table.find_all(recursive=False)
    # TODO: reuse the 'similar verbs' later as seeder
    table_elements = table_elements[2:-1]  # get rid of 1st two (quick-nav) and last (similar verbs)
    # every 3 elements describe one mood
    moods = [parse_mood(table_elements[i:i+3]) for i in range(0, len(table_elements), 3)]
    return dict(moods)

def parse_mood(triplet):
    [a, _h2, tense_divs] = triplet
    mood_name = a['name']
    # mood_descr = h2.text
    mood_tenses = [parse_tense(e) for e in tense_divs.find_all(recursive=False)]
    return mood_name, dict(mood_tenses)

def parse_tense(tense_div):
    children = tense_div.find_all(recursive=False)
    tense_name = children[0].text ## double-check 1st child is an h3?
    tense = {}

    assert children[1].text == 'Affirmative'
    [positive_n_assert, positive_n_question] = children[2].find_all(recursive=False)
    tense[('positive', 'assertion')] = parse_persons(positive_n_assert, 'Statement')
    tense[('positive', 'question')]  = parse_persons(positive_n_question,'Interrogative')

    assert children[3].text == 'Negative'
    [neg_n_assert, neg_n_question] = children[4].find_all(recursive=False)
    tense[('negative', 'assertion')] = parse_persons(neg_n_assert, 'Statement')
    tense[('negative', 'question')]  = parse_persons(neg_n_question,'Interrogative')

    return (tense_name, tense)

def parse_persons(node, title):
    # [assertion_n, question_n] = node.find_all(recursive=False)
    [title_n, content_n] = node.find_all(recursive=False)
    assert title_n.text == title
    rows = content_n.find_all('tr')
    verb_forms = [r.find_all(recursive=False)[1].text for r in rows]
    if len(rows) == 6:
        features = [('1', 'sg'), ('2', 'sg'), ('3', 'sg'), ('1', 'pl'), ('2', 'pl'), ('3', 'pl')]
    elif len(rows) == 4: # imperative assertion
        features = [('2', 'sg'), ('3', 'sg'), ('2', 'pl'), ('3', 'pl')]
    elif len(rows) == 2: # negative imperative
        features = [('3', 'sg'), ('3', 'pl')]
    else:
        raise ValueError(f'Uknown type of verb with {len(rows)} forms')
    return dict(zip(features, verb_forms))



if __name__ == '__main__':
    p = get_paradigm('gitmek')
    print(p)
    p = get_paradigm('olmak')
    print(p)
