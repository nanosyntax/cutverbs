import time, random
from urllib.parse import quote_plus
from pathlib      import Path
from bs4          import BeautifulSoup
from playwright.sync_api import sync_playwright

from cutverbs.utils import walk
from scraper import Scraper

class DictComSk(Scraper):

    def get_paradigms(self, lemmas, categ = None):
        return self.get_all_paradigms(lemmas)

    def parse_paradigm(self, data, word, categ = None):
        return parse_paradigm(data, word)

    def __init__(self):
        super().__init__('dictcom_sk', 'svk')


    def get_all_paradigms(self, words):
        all_paradigms = {}
        to_be_downloaded = []
        for word in words:
            if pdgm := self.cached_paradigm(word):
                all_paradigms[word]= pdgm
            elif raw := self.cached_raw_data(word):
                pdgm = self.parse_paradigm(raw, word)
                if pdgm:
                    self.write_paradigm(pdgm, word)
                    all_paradigms[word] = pdgm
                else:
                    raise ValueError('cannot understand the paradigm for ' + word)
            else:
                to_be_downloaded.append(word)
        # raise ValueError('temporarily do not use -- reorganizing')
        if to_be_downloaded:
            with sync_playwright() as playwright: # hyper slow, creates a browser for each page that we download...
                browser = playwright.chromium.launch(headless=True)
                page = browser.new_page()

                for word in to_be_downloaded:
                    html = self.get_html_using_page(word, page)
                    if not html:
                        continue
                    paradigm = parse_paradigm(html, word)
                    if paradigm:
                        self.write_paradigm(paradigm, word)
                        all_paradigms[word] = paradigm
        return all_paradigms


    def get_html_using_page(self, word, page):
        file_path = self.html_dir / word ## TODO: can delete this. Done in get_all_paradigms.  
        if file_path.is_file():
            raise ValueError('this should never happen anymore. Please debug.')
            return file_path.read_text('utf-8')

        time.sleep(random.randint(5, 16))
        escaped_word = quote_plus(word)
        url = f'https://dict.com/slovak-english/{escaped_word}'
        html = _page_get_html(url, page, word)
        if html:
            print(f'  ----  downloaded html page for {word}')
            self.write_originals(html, word)
        return html



    def get_paradigm(self, word):
        raise ValueError('Please use get_all_paradigms() and a larger batch of verbs, not with single verbs')

    ## is this used at all still?
    def download_data(self, word, categ):
        time.sleep(random.randint(5, 16))
        escaped_word = quote_plus(word)
        url = f'https://dict.com/slovak-english/{escaped_word}'
        return self.fetch_remote_html(url, word)

    def fetch_remote_html(self, url, word):
        with sync_playwright() as playwright: # hyper slow, creates a browser for each page that we download...
            browser = playwright.chromium.launch(headless=True)
            page = browser.new_page()
            return _page_get_html(url, page, word)




# ---- PRIVATE ------------------------------------

html_dir = Path('data/external/dictcom/sk/html')
html_dir.mkdir(parents=True, exist_ok=True)

# def get_html(word):
#     file_path = html_dir / word
#     if file_path.is_file():
#         return file_path.read_text('utf-8')

#     html = download_data(word)
#     cache_original(html, word)
#     return html


# def cache_original(data, word):
#     data = '' if data is None else data # for cases in which there is no such word on the website and we want to remember that fact
#     file_path = html_dir / word  # use nom.sg / 1.sg.pres instead of word?
#     with open(file_path, 'w', encoding='utf-8') as f:
#         f.write(data)


def _page_get_html(url, page, word):
        page.goto(url)
        # page.screenshot(path="var/play/consent_before.png", full_page=True)
        # with page.expect_navigation():
        if page.query_selector('button:has-text(\"Consent\")'):
            page.locator("button:has-text(\"Consent\")").click()
            time.sleep(1)
            # page.screenshot(path="var/play/consent_after.png", full_page=True)
        if page.query_selector('#ppoadvmodal'):
            # page.screenshot(path="var/play/register_today.png", full_page=True)
            page.locator('i.modal-close').click()
            time.sleep(1)
        if not page.query_selector('span.lex_ful_mtab'):
            entry = page.query_selector('#entry-wrapper')
            if entry.query_selector('span.no_entry_found'):
                print(f'  ****  There is no verb for {word} at: {url}')
                return None
            elif ortho := entry.query_selector('span.lex_ful_entr'): # ipa = lex_ful_pron
                ortho = ortho.text_content()  # or: ortho.inner_text()
                print(f'  ****  There is no paradigm for: {ortho}')
                return None
            else:
                print('  ****  Unknown download error -- please inspect the situation and update the scraper code to deal with it')
                raise ValueError('  ****  Unknown download error -- please inspect the situation and update the scraper code to deal with it')
        page.locator("span.lex_ful_mtab").click()
        time.sleep(1)
        # page.screenshot(path="var/play/mtab-click-after.png", full_page=True)

        html = page.content()
        return(html)


def parse_paradigm(html, word):
    bhtml = BeautifulSoup(html, "html.parser") #, 'lxml'
    data  = bhtml.find("div", id="mycol-center")
    table = data.find("table", class_="entry")
    table_kids = table.find_all(recursive=False)
    assert len(table_kids) == 1 # tbody
    main_rows = table_kids[0].findChildren(recursive=False)
    paradigm = parse_main_rows(main_rows, word)
    if not paradigm:
        return None
    extract_additional_words(bhtml, paradigm)
    disambiguate_paradigm(paradigm)
    return paradigm

def disambiguate_paradigm(paradigm):
    for ff, forms, cur_tense, f_above in walk(paradigm):
        if type(forms) == list:
            if f_above == ['passive'] and len(forms) == 2:
                forms.sort()
                if ff == ('masc', 'sg'): # enough to sanity check just once for all passive forms
                    endings = [f[-2:] for f in forms]
                    assert endings == ['ný', 'tý']
                    paradigm['meta']['passive_nt_alternation'] = True
                cur_tense[ff] = forms[0]
            else:
                raise ValueError('unhandled ambiguity, please implement')

def parse_main_rows(rows, word):
    conjug_wrapper = rows[1].find('div', class_="morfwrapper")
    segmented_paradigms = parse_conjugations(conjug_wrapper)
    paradigms = join_morphemes(segmented_paradigms)
    paradigms['meta'] = parse_about_row(rows[0], word)
    paradigms['word'] = word # for convenience, cache the original unparsed word
    paradigms['meta']['segmented'] = segmented_paradigms # is this useful?
    return paradigms

def parse_about_row(row, word):
    spans = row.findChildren('span')
    if len(spans) != 4:
        # eg 'dodržiavať' = dodržiavať [dodrʒ̑avac], dodržovať [dodrʒovac] impf = 7
        # Not clear for now whether it is worth to code an exception for these or just ignore them
        # For now, it seems ok for these verbs to simply not have an IPA or 'aspect' info
        print(f'  ****  Cannot parse the about row for {word}: {row.text}')
        return {}
    [infinitive, ipa, aspect, _] = spans
    infinitive = infinitive.text
    if infinitive != word:
        assert is_version_of_infinitive(infinitive, word)
    return {
        'ipa': ipa.text.strip()[1:-1],  # take away square brackets around IPA 
        'aspect': aspect.text.strip()}

def is_version_of_infinitive(downloaded_word, infinitive):
    if deaccent(downloaded_word) == deaccent(infinitive):
        return True
    if downloaded_word.endswith('1') and downloaded_word[:-1] == infinitive:
        return True
    if downloaded_word.endswith(' si') and downloaded_word[:-3] == infinitive:
        return True
    return False

def deaccent(word):
    deaccented = word.replace('ý', 'y')
    deaccented = deaccented.replace('ú', 'u')
    deaccented = deaccented.replace('ľ', 'l')
    return deaccented

def parse_conjugations(conjug_wrapper):
    conjug_tables_titles = conjug_wrapper.find('ul', class_="nav-tabs")
    conjug_tables_titles = conjug_tables_titles.findChildren(recursive=False) # check li ?
    conjug_tables_titles = [t.text for t in conjug_tables_titles]

    conjug_tables  = conjug_wrapper.findChildren("table", class_="morf-table")
    # assert len(conjug_tables) == len(conjug_tables_titles)
    conjug_tables = zip(conjug_tables_titles, conjug_tables)

    pdict = {}
    for name, table in conjug_tables:
        match name:
            case 'Conjugation':
                parse_main_conjug_table(table, pdict)
            case 'Past tense  - Active voice':
                parse_participle_conjug_table(table, 'participle', pdict)
            case 'Conditional':
                pass # just a participle with by+aux in front of it
                # parse_participle_conjug_table(table, 'conditional', pdict)
            case 'Transgressive':
                parse_transgressive_table(table, pdict)
            case 'Passive voice':
                parse_passive_table(table, pdict)
            case 'Future tense': # perfective finite
                rows = iter(table.findChildren('tr'))
                finite_perfective = parse_finite_conjug_table(rows)
                pdict['finite_perfective'] = finite_perfective
            case _:
                raise ValueError(f'Unknown Slovak conjugation table: {name}')
    return pdict

def parse_main_conjug_table(table_div, pdict):
    rows = table_div.findChildren('tr')
    rows = iter(rows)

    [infin_title, infin_val] = next(rows).findChildren(recursive=False) # td
    assert infin_title.text.strip() == 'Infinitive'
    pdict['infinitive'] = parse_td_morphemes(infin_val)
    pdict['finite']     = parse_finite_conjug_table(rows, 'Present tense')

def parse_finite_conjug_table(rows, tense_name = None):
    finite = {}
    if tense_name:
        [title, singular, plural]    = next(rows).findChildren(recursive=False)
        assert title.text.strip() == tense_name
    else:
        next(rows)  # eat the title row
    for n in range(1, 4):
        [_features, singular, plural] = next(rows).findChildren(recursive=False)
        finite[(str(n), 'sg')] = parse_td_no_pronoun(singular)
        finite[(str(n), 'pl')] = parse_td_no_pronoun(plural)
    return finite

def parse_participle_conjug_table(table_div, tense_name, pdict):
    rows = table_div.findChildren('tr')
    rows = rows[1:]  # remove the 'singular'/'plural' row
    assert len(rows) == 8 # 
    [_, masc, plur] = rows[0].findChildren(recursive=False)
    m = parse_td_morphemes(masc, -2)
    p = parse_td_morphemes(plur, -2)
    [_, fem, _]    = rows[1].findChildren(recursive=False)
    f = parse_td_morphemes(fem, -2)
    [_, neuter, _] = rows[-1].findChildren(recursive=False)
    n = parse_td_morphemes(neuter, -2)

    pdict[tense_name] = {
        ('masc',   'sg'): m,
        ('fem',    'sg'): f,
        ('neuter', 'sg'): n,
        ('plur'):         p,
    }

def parse_passive_table(table_div, pdict):
    rows = table_div.findChildren('tr')
    assert len(rows) == 10
    passive = {}
    [_, masc, plur] = rows[1].findChildren(recursive=False)
    passive[('masc', 'sg')] = parse_td_morphemes(masc, 2)
    passive[('anim', 'pl')] = parse_td_morphemes(plur, 2)
    [_, fem, plur] = rows[1].findChildren(recursive=False)
    passive[('fem',  'sg')] = parse_td_morphemes(fem, 2)
    passive[('inan', 'pl')] = parse_td_morphemes(plur, 2)
    [_, neuter, _] = rows[-1].findChildren(recursive=False)
    passive[('neuter', 'pl')] = parse_td_morphemes(neuter, 2)
    pdict['passive'] = passive

def parse_transgressive_table(table_div, pdict):
    rows = table_div.findChildren('tr')
    assert len(rows) == 4
    [_, gerund] = rows[1].findChildren(recursive=False)
    pdict['gerund'] = parse_td_morphemes(gerund)

def parse_td_no_pronoun(td):
    return parse_td_morphemes(td, 1)

def parse_td_morphemes(td, rkeep=0):
    morphemes = td.contents[0].split()
    if len(td.contents) > 1:
        morphemes.append(td.contents[1].text)
    # pre_text = morphemes[:lstrip]
    # morphemes = morphemes[lstrip:]
    pre_text = morphemes[:rkeep]
    morphemes = morphemes[rkeep:]
    if len(td.contents) > 2:
        assert '/' in td.text
        assert len(td.contents) == 4
        variant2 = td.contents[2].text.lstrip('/').split()#[lstrip:]
        variant2.append(td.contents[3].text)
        if variant2[:rkeep] == pre_text:
            variant2 = variant2[rkeep:]
        morphemes = (morphemes, variant2)
    return morphemes

def join_morphemes(paradigm):
    joined = {}
    for k, v in paradigm.items():
        if k == 'meta':
            continue
        if type(v) == dict:
            joined[k] = join_morphemes(v)
        elif type(v) == tuple: # multiple exponents of same feature set
            variants = [''.join(variant) for variant in v]
            joined[k] = variants
        else:
            joined[k] = ''.join(v)
    return joined


def extract_additional_words(soup, pdict):
    col_left  = soup.find('div', id='col-left')
    # card = col_left.find('div', class_='mcard mcardlmenu')
    cards = col_left.findChildren('div', class_='mcard mcardlmenu')
    for card in cards:
        card_title = card.find('h2', class_='mcardlmenuh').text
        match card_title:
            case 'Prefixes':
                parse_prefixed_list(card, pdict)
            case 'Derived words':
                parse_derived_words(card, pdict)
            case _:
                pass


def parse_prefixed_list(card, pdict):
    if prefixed := parse_additional_words_card(card, pdict):
        pdict['meta']['cognates_prefixed'] = prefixed

def parse_derived_words(card, pdict):
    if derived := parse_additional_words_card(card, pdict):
        pdict['meta']['cognates_derived'] = derived

def parse_additional_words_card(card, pdict):
    spans = card.findChildren('span', class_='ispan')
    words = [span.text for span in spans]
    if words[0] == pdict['word']:
        words = words[1:]
    return words

if __name__ == '__main__':
    dictcom = DictComSk()
    p = dictcom.get_all_paradigms(['hajať', 'zavadzať', 'vrinduť', 'podeliť', 'vedieť', 'fandiť'])
    print(p)
