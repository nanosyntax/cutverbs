import requests, re, time
from urllib.parse import quote_plus
from pathlib      import Path
from bs4          import BeautifulSoup

from scraper import Scraper

class Prirucka(Scraper):

    def __init__(self, sleep_time = 7):
        super().__init__('prirucka', 'cze')
        self.sleep_time = sleep_time

    # def download_data(self, word, categ):
    #     return download_data(word, self.sleep_time)

    def parse_paradigm(self, html, word, categ):
        bhtml = BeautifulSoup(html) #, 'lxml'    
        table = bhtml.find("table", class_="para")
        if not table:
            self.delete_html_file(word)
            if bhtml.find('title', string = '502 Proxy Error'):
                print(f'    -- You did too many searches -- the server is overloaded')
                exit()  # don't continue running the code
            elif len(bhtml.select('#content span.odsazeno')) > 0:
                raise ValueError(f'    -- This verb {word} is ambiguous between multiple meanings, please choose another one')
            elif bhtml.find(id="protest"): # check if id=protest is still the best option when such a word doesn't exist in Priurcka
                print(f'    -- Prirucka did not find declension table -- skipping: {word}')
                self.note_word_not_available(word)
                return False # this is not html with paradigm data
        rows = table.findChildren(['tr'])
        del rows[0]  # remove the title row
        data = [parse_table_row(row) for row in rows]
        return create_v_dict(data, word)


# def get_paradigm(word):
#     data     = get_html(word)
#     return parse_paradigm(data, word)


# ---- PRIVATE ------------------------------------

# html_dir = Path('data/external/prirucka/html')
# html_dir.mkdir(parents=True, exist_ok=True)

# def get_html(word):
#     file_path = html_dir / word
#     if file_path.is_file():
#         return file_path.read_text('utf-8')

#     html = download_data(word)
#     cache_original(html, word)
#     return html

# def download_data(word, sleep_time = 7):
#     time.sleep(sleep_time)
#     escaped_word = quote_plus(word)
#     headers = {'User-Agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'}
#     response = requests.get(f'https://prirucka.ujc.cas.cz/?slovo={escaped_word}', headers=headers)
#     if response.status_code != 200:
#         print('  *********  bad response from Prirucka', response.status_code, response.reason)
#         print('       ', response.text)
#         raise ValueError(response)
#     return response.text

# def cache_original(data, word):
#     file_path = html_dir / word  # use nom.sg / 1.sg.pres instead of word?
#     with open(file_path, 'w', encoding='utf-8') as f:
#         f.write(data)

# def delete_html_file(word):
#     path = html_dir / word
#     path.unlink()


def create_v_dict(data, infinitive):
    [imp2sg, imp2pl] = data[3]
    paradigm = {
        'meta': {'category': 'v'},
        'infinitive': infinitive,
        'participle': data[4][0],
        'passive': data[5][0],
        'imperative': {
            ('2', 'sg'): imp2sg,
            ('2', 'pl'): imp2pl},
        'gerund': {
            ('masc', 'sg'): data[6][0], 
            ('fem', 'sg'): data[7][0],
            ('pl',): data[6][1]},
        # 'nominalisation': data[8][0]
    }
    finite = {}
    [sgs, pls] = list(zip(*data[0:3]))
    for sg, person in zip(sgs, range(1, 4)):
        finite[(str(person), 'sg')] = sg
    for pl, person in zip(pls, range(1, 4)):
        finite[(str(person), 'pl')] = pl
    paradigm['finite'] = finite
    return paradigm

# ------ low level prirucka parsing

def parse_table_row(row):
    row_as_td  = row.findChildren('td')
    row_as_txt = [parse_table_cell(td) for td in row_as_td]
    row_content = row_as_txt[1:]  # strip the left header
    row_content = [temp_hack_choose_variant(form) for form in row_content] ## temp hack: if multiple forms for a given feature set (eg: ['sázejí', 'sází']), use only the 1st
    # row_content = [(form if type(form) == str else form[0]) for form in row_content] ## temp hack: if multiple forms for a given feature set (eg: ['sázejí', 'sází']), use only the 1st
    return tuple(row_content)

def parse_table_cell(cell):
    content = cell.text.strip()
    content = re.sub(r'[0-9]', '', content)
    content = re.sub(r'\ss(e|i)|\s\(se\)', '', content)        #clean up the paradig from se/(se)/si
    if content.find(',') >= 0:
        content = content.split(',')
        content = [form.strip() for form in content]
    return content


def temp_hack_choose_variant(val):
    if type(val) == str:
            return val
    # assert len(val) == 2
    if val[0][-3:] == 'ají' and val[1][-2:] == 'ou': # 'couvají' / 'couvou'
        return val[0]
    if val[0][-1] == 'i' and val[1][-1] == 'u':
        return val[1]
    if val[0][-1] == 'í' and val[1][-2:] == 'ou':
        return val[1]
    return val[0] # elsewhere case -- ie list above only cases where return 2nd