import requests, ast, time
from pathlib import Path
from urllib.parse import quote_plus

from scraper.tags    import translate_tag_to

class Scraper:

    ## ------------- main API --

    def get_lemmas(self, query):
        # implement this in subclasses
        return []

    def get_noun_lemmas(self, max):
        tag = translate_tag_to( ('nom', 'sg') , self.tag_type)  # some lg have no notion of nominative, delegate feature-choice to each lg?
        return self.get_lemmas(tag, max)

    def get_verb_lemmas(self, max):
        tag = translate_tag_to( ('infinitive') , self.tag_type)
        return self.get_lemmas(tag, max)


    def get_paradigm(self, word, lg=None, categ=None):
        return self.cached_paradigm(word, categ) or self.compute_paradigm(word, categ, lg)

    def reparse_raw_data(self, word=None, lg=None):
        if word is not None:
            data = self.get_raw_data(word)
            return self.extract_paradigm(data, word)
        for src in self.html_dir.iterdir():
            if src.is_file():
                word = src.name
                print(f'reparsing {word}')
                data = self.get_raw_data(word)
                if not data:    self.delete_html_file(word)
                else:           self.extract_paradigm(data, word)


    ## ------------- subclass responsability --

    def url(self, word):
        pass

    def parse_paradigm(self, data, word, categ = None):
        pass

    def rearrange_doublets(self, doublets, features):
        return doublets

    def download_data(self, word, categ = None):
        # Generic default implementation, 2023-05.
        headers = {'User-Agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'}
        url = self.url(quote_plus(word))
        response = requests.get(url, headers=headers)
        if response.status_code != 200:
            if response.status_code == 404:
                self.note_word_not_available(word)
                print(f'  ****  {word} is not available in {self.name}')
                return None
            print(f'  *********  bad response from {self.name}', response.status_code, response.reason)
            print('       ', response.text)
            raise ValueError(response)
        time.sleep(self.sleep_time)
        return response.text


    ## ------------- high level helpers --

    def compute_paradigm(self, word, categ=None, lg=None):
        data = self.get_raw_data(word, categ)
        if not data:
            return False
        paradigm = self.extract_paradigm(data, word, categ, lg)
        return paradigm

    def get_raw_data(self, word, categ=None, url=None):
        if url is None: # try to return the cached data on disk, but not if we were told explicitely to go to a given url
            if raw := self.cached_raw_data(word):
                return raw
        if self.is_word_unavailable(word):
            print(f'  ****  word {word} is unavailable at {self.name}')
            return None
        data = self.download_data(word, categ)
        if data:
            self.write_originals(data, word)
        return data

    def extract_paradigm(self, data, word, categ=None, lg=None):
        paradigm = self.parse_paradigm(data, word, categ)
        match paradigm:
            case False:
                return False
            case ('redirect', url):
                data = self.get_raw_data(word, categ=None, url=url)
                return self.extract_paradigm(data, word, categ, lg)
            case _:
                paradigm = self.adjust_paradigm(paradigm, categ, lg)
                if not paradigm:  return False
                self.write_paradigm(paradigm, word, lg)
                return paradigm

    def adjust_paradigm(self, paradigm, categ=None, lg=None):
        # hook to mark optional items which are absent or reorder features in dictionaries
        prefs = self.preferences()
        if prefs and prefs.get('optional_slots'):
            cat = categ or paradigm['meta']['category']
            opt = prefs['optional_slots'].get(cat) or []
            for f in opt:
                if not paradigm.get(f):
                    paradigm[f] = True # paradigm lacks an optional form -> make it always match any suffix for that feature
        return paradigm

    ## ------------- doublet helpers --

    # standardise policy for format of doublets, shared across all scrapers
    def store_form_and_doublets(self, form, features, paradigm):
        if type(form) == list:
            self.rearrange_doublets(form, features)
            self.store_doublets(form[1:], features, paradigm)
            form = form[0]
        paradigm[features] = form

    def store_doublets(self, doublets, features, paradigm):
        if not paradigm.get('meta'):
            paradigm['meta'] = {}
        if not paradigm['meta'].get('doublets'): 
            paradigm['meta']['doublets'] = {}
        paradigm['meta']['doublets'][features] = doublets

    def separate_doublets(self, paradigm):
        """Takes an already formed paradigm, and extract doublet values into meta.doublets"""
        meta = paradigm.get('meta') or {}
        doublets = meta.get('doublets') or {}
        self.separate_doublets_into(paradigm, tuple(), doublets)
        if len(doublets) > 0:
            meta['doublets'] = doublets
            paradigm['meta'] = meta
        return paradigm

    def prefer_over_doublet(self, preferred_ending, dispreferred, doublets):
        pref = None
        dis = None
        if preferred_ending == '':
            if doublets[0].endswith(dispreferred):
                doublets.insert(0, doublets.pop(1))
                return True # changed order
            else:
                return False
        if dispreferred == '':
            if doublets[0].endswith(preferred_ending): return False # didnt change anything
            if doublets[1].endswith(preferred_ending):                          pref = 1
            elif len(doublets) > 2 and doublets[2].endswith(preferred_ending):  pref = 2
            if pref is not None:
                doublets.insert(0, doublets.pop(pref))
                return True # changed order
            return False
        for i, d in enumerate(doublets):
            if d.endswith(preferred_ending):
                pref = i
            elif d.endswith(dispreferred):
                dis = i
        if pref is not None and dis is not None and pref > 0:
            doublets.insert(0, doublets.pop(pref))
            return True
        return False # did not change order

    def separate_doublets_into(self, subparadigm, mom_features, doublets):
        for k, v in subparadigm.items():
            if k == 'meta':
                continue
            elif type(v) == list:
                if len(mom_features) > 0: # inside the doublet, keep a flat list of features. here k + mom_features.
                    features = k if type(k) == tuple else (k,)
                    features = mom_features + features
                else:
                    features = k
                self.rearrange_doublets(v, features)
                subparadigm[k] = v[0]
                doublets[features] = v[1:]
            elif type(v) == dict:
                features = k if type(k) == tuple else (k,)
                features = mom_features + features
                subparadigm[k] = self.separate_doublets_into(v, features, doublets)
        return subparadigm

    ## ------------- low level helpers --

    def cached_paradigm(self, word, categ = None):
        def find_cached():
            if self.dir_manual and (file_path := self.dir_manual / word).is_file():
                return file_path
            elif (file_path := self.parsed_dir / word).is_file():
                return file_path
            elif self.dir_all and (file_path := self.dir_all / word).is_file():
                return file_path
            else:
                return None

        if not (file_path := find_cached()):
            return None
        text = file_path.read_text('utf-8')
        data = ast.literal_eval(text)
        if type(data) == list:
            if categ:   hit = [d for d in data if d['meta'].get('categ') == categ]
            else:       hit = None
            if not hit or len(hit) > 1:
                raise ValueError(f'Multiple meanings for {word}, cannot find the right one')
            data = hit[0]
        return data

    def cached_raw_data(self, word):
        file_path = self.html_dir / word
        if file_path.is_file():
            return file_path.read_text('utf-8')
        return None

    def preferences(self, lg=None):
        if hasattr(self, '_prefs'): return self._prefs
        prefs = {}
        if lg:
            prefs = lg.preferences
        elif hasattr(self, '_lg'):
            with open(f'data/preferences/{self._lg}.py', 'r', encoding='utf-8' ) as f:
                s = f.read()
            prefs = ast.literal_eval(s)
        self._prefs = prefs  # keep only paradigm_type and optional_slots?
        return prefs

    def write_originals(self, data, word):
        file_path = self.html_dir / word  # use nom.sg / 1.sg.pres instead of word?
        with open(file_path, 'w', encoding='utf-8') as f:
            f.write(data)

    def write_paradigm(self, para, word, lg = None):
        s_para = str(para)
        with open(self.parsed_dir / word, 'w', encoding='utf-8') as f:
            f.write(s_para)
        with open(self.dir_all / word,    'w', encoding='utf-8') as f:
            f.write(s_para)        # experimental, 2023-06: pool all parsed words into a single dir
        if lg and self.word_store: # experimental, 2023-04: upload paradigm to our server
            json_word = {'word': {'lg': lg.code, 'ortho': word, 'conjugation': para}}
            requests.post(self.word_store, params=json_word, headers={'content-type' : 'application/json'})

    def delete_html_file(self, word):
        path = self.html_dir / word
        path.unlink()

    def is_word_unavailable(self, word):
        return word in self.unavailable()
   
    def note_word_not_available(self, word):
        path = self.html_dir.parent / '.unavailable'
        with open(path, 'a+', encoding='utf-8') as f:
            prev = f.read().find(word)
            if prev == -1:
                f.write(word + '\n')
                self.unavailable().append(word)
    
    def unavailable(self):
        if self.__unavailable is None:
            path = self.html_dir.parent / '.unavailable'
            if not path.is_file():
                return []
            with open(path, 'r', encoding='utf-8') as f:
                self.__unavailable = f.read().strip().split('\n') # TODO: empty lines will create empty lemmas - problem?
        return self.__unavailable

    def __init__(self, name, lg = None) -> None:
        self.name = name.replace('/', '_')
        if not lg: # old style, during transition
            self.html_dir   = Path(f'data/external/{name}/html')
            self.parsed_dir = Path(f'data/external/{name}/parsed')
        else:
            if type(lg) != str: lg = lg.code3
            if len(lg) != 3:
                raise ValueError(f' ** The lg code in a scraper should be 3-letter standard code for the language, not {lg}')
            self.html_dir   = Path(f'data/paradigms/{lg}/{name}/html')
            self.parsed_dir = Path(f'data/paradigms/{lg}/{name}/parsed')
            self.dir_all    = Path(f'data/paradigms/{lg}/all')
            self.dir_all.mkdir(parents=True, exist_ok=True)
            self.dir_manual    = Path(f'data/paradigms/{lg}/manual')
            self.dir_manual.mkdir(parents=True, exist_ok=True)
        self.html_dir.mkdir(parents=True, exist_ok=True)
        self.parsed_dir.mkdir(parents=True, exist_ok=True)
        # experimental, 2023-04
        self.__unavailable = None
        self.sleep_time = 10
        self.word_store = "http://michal.starke.ch/words"
