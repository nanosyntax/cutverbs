tags = {
    ('nom', 'sg'): {
        'Majka.cz': 'k1g.nSc1',
        'SNK': 'SS.s1',             # slovak national korpus
        'CNK': 'NN.S1.*'            # czech national korpus
    },
    ('acc', 'sg'): {
        'Majka.cz': 'k1g.nSc4',
        'SNK': 'SS.s4',
        'CNK': 'NN.S4.*'
    },
    ('gen', 'sg'): {
        'Majka.cz': 'k1g.nSc2',
        'SNK': 'SS.s2',
        'CNK': 'NN.S2.*'
    },
    ('dat', 'sg'): {
        'Majka.cz': 'k1g.nSc3',
        'SNK': 'SS.s3',
        'CNK': 'NN.S3.*'
    },
    ('voc', 'sg'): {
        'Majka.cz': 'k1g.nSc5',
        'SNK': 'SS.s5',
        'CNK': 'NN.S5'
    },
    ('loc', 'sg'): {
        'Majka.cz': 'k1g.nSc6',
        'SNK': 'SS.s6',
        'CNK': 'NN.S6'
    },
    ('instr', 'sg'): {
        'Majka.cz': 'k1g.nSc7',
        'SNK': 'SS.s7',
        'CNK': 'NN.S7',
    },

    ('nom', 'pl'): {
        'Majka.cz': 'k1g.nPc1',
        'SNK': 'SS.p1',
        'CNK': 'NN.P1',
    },
    ('acc', 'pl'): {
        'Majka.cz': 'k1g.nPc4',
        'SNK': 'SS.p4',
        'CNK': 'NN.P4'
    },
    ('gen', 'pl'): {
        'Majka.cz': 'k1g.nPc2',
        'SNK': 'SS.p2',
        'CNK': 'NN.P2'
    },
    ('dat', 'pl'): {
        'Majka.cz': 'k1g.nPc3',
        'SNK': 'SS.p3',
        'CNK': 'NN.P3'
    },
    ('voc', 'pl'): {
        'Majka.cz': 'k1g.nPc5',
        'SNK': 'SS.p5',
        'CNK': 'NN.P5'
    },
    ('loc', 'pl'): {
        'Majka.cz': 'k1g.nPc6',
        'SNK': 'SS.p6',
        'CNK': 'NN.P6'
    },
    ('instr', 'pl'): {
        'Majka.cz': 'k1g.nPc7',
        'SNK': 'SS.p7',
        'CNK': 'NN.P7'
    },
    ('finite','1', 'sg'): {
        'Majka.sk': 'k5e.nSp1a.mI',
        'Majka.cz': 'k5e.a.mIp1nS',
        'SNK': 'VK.sa..',
        'CNK': 'VB.S...1P.*',
        'multext-east': 'V..ip1s*',
    },
    ('finite','2', 'sg'): {
        'Majka.sk': 'k5e.nSp2a.mI',
        'Majka.cz': 'k5e.a.mIp2nS',
        'SNK': 'VK.sb..',
        'CNK': 'VB.S...2P.*',
        'multext-east': 'V..ip2s*',
    },
    ('finite','3', 'sg'): {
        'Majka.sk': 'k5e.nSp3a.mI',
        'Majka.cz': 'k5e.a.mIp3nS',
        'SNK': 'VK.sc..',
        'CNK': 'VB.S...3P.*',
        'multext-east': 'V..ip3s*',
    },
    ('finite','1', 'pl'): {
        'Majka.sk': 'k5e.nPp1a.mI',
        'Majka.cz': 'k5e.a.mIp1nP',
        'SNK': 'VK.pa..',
        'CNK': 'VB.P...1P.*',
        'multext-east': 'V..ip1p*',
    },
    ('finite','2', 'pl'): {
        'Majka.sk': 'k5e.nPp2a.mI',
        'Majka.cz': 'k5e.a.mIp2nP',
        'SNK': 'VK.pb..',
        'CNK': 'VB.P...2P.*',
        'multext-east': 'V..ip2p*',
    },
    ('finite','3', 'pl'): {
        'Majka.sk': 'k5e.nPp3a.mI',
        'Majka.cz': 'k5e.a.mIp3nP',
        'SNK': 'VK.pc..',
        'CNK': 'VB.P...3P.*',
        'multext-east': 'V..ip3p*',
    },
    ('infinitive', ): {
        'Majka.sk': 'k5a.mF',
        'Majka.cz': 'k5eAa.mF',
        'SNK': 'VI.*',
        'CNK': 'Vf.*',
        'multext-east': 'V..n.*',
    },
    ('participle', ): {
        'Majka.sk': 'k5e.gMnSp1a.mA',
        'Majka.cz': 'k5e.a.mAgMnS',
        'SNK': 'VL.sc..',
        'CNK': 'VpMS....R.*'
    },

}

# cz & sk use the same Majka tags for N (but not for V).
#   Copy one into the other programmatically to avoid duplicating them by hand.
for num in ['sg', 'pl']:
    for k in ['nom', 'acc', 'gen', 'loc', 'dat', 'instr']:
        tags[(k, num)]['Majka.sk'] = tags[(k, num)]['Majka.cz']


def translate_tags_to(our_tags, encoding):
    return {our_tag: translate_tag_to(our_tag, encoding) for our_tag in our_tags}

def translate_tag_to(our_tag, encoding):
    return tags[our_tag][encoding]

def tags_for_present(encoding):  # tags for std 1/2/3 sg/pl
    return {(p, n): translate_tag_to( ('finite', p, n), encoding) for n in ['sg', 'pl'] for p in ['1', '2', '3']}

'''def translate_tags(self, paradigm):
        translated_tags = self.translate_tags_to(our_tags, encoding)
        copy_paradigm = paradigm
        paradigm = {}
        for tag in copy_paradigm.keys():
            paradigm[translated_tags.get(tag)] = copy_paradigm.get(tag)
        return paradigm'''

'''def create_dict_translated_tags(self):
        tags = ['k1g.nSc1', 'k1g.nSc4', 'k1g.nSc2', 'k1g.nSc6', 'k1g.nSc3', 'k1g.nSc7', 
                'k1g.nPc1', 'k1g.nPc4', 'k1g.nPc2', 'k1g.nPc6', 'k1g.nPc3', 'k1g.nPc7']
        features = [('nom', 'sg'),  ('acc', 'sg'), ('gen', 'sg'), ('loc', 'sg'), ('dat', 'sg'), ('instr', 'sg'), ('nom', 'pl'),  ('acc', 'pl'), ('gen', 'pl'), ('loc', 'pl'), ('dat', 'pl'), ('instr', 'pl')]
        translated_tags = {} #{'k1...':('nom, sg')}

        zipped_tags = (zip(tags, features))
        for tag, feature in zipped_tags:
            translated_tags[tag] = feature

        return translated_tags'''



# if __name__ == '__main__':
#     from paradig.language import Language
#     cz = Language('cz')
#     features = cz.features_for_category('v')
#     print(features)
#     our_tag = ('finite', '1', 'sg')
#     encoding = 'Majka.sk'
#     print(translate_tag_to(our_tag, encoding))