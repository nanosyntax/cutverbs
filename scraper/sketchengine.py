from scraper.sketch_base import SketchBase
from scraper.tags import translate_tags_to


class SketchEngine(SketchBase):

    def get_lemmas(self, tag_query, max):
        params = self.params.copy()
        params['q'] = f'q[tag="{tag_query}"]'
        params['pagesize'] = str(max)
        return self.get(params)

# get lemmas for Turkish - have to use longtag and ending of infinitive
    def get_tk_lemmas(self, tag_query, max):
        params = self.params.copy()
        params['q'] = f'q[longtag="{tag_query}" & word=".*m[ae]k"]'
        params['pagesize'] = str(max)
        return self.get(params)

# get lemmas for Japanese - maybe subset
    def get_jp_lemmas(self, tag_query, max):
        params = self.params.copy()
        # Concl.g | Attr.g??
        params['q'] = f'q[tag="{tag_query}" & infl_form="Concl.g"|tag="{tag_query}" & infl_form="Attr.g" ]'
        params['pagesize'] = str(max)
        return self.get(params)

    # new stuff - for getting verbs beginning by a certain letter

    def get_lemmas_letter(self, tag_query, beg_letter, max):
        params = self.params.copy()
        params['q'] = f'q[tag="{tag_query}" & lemma="{beg_letter}.*"]'
        params['pagesize'] = str(max)
        return self.get(params)

    def get_nominal_forms(self, nominative, lg):
        return self.get_paradigm(nominative, lg, 'n')

    def get_verbal_forms(self, infinitive, lg):
        return self.get_paradigm(infinitive, lg, 'v')

    # ------------- lower level helpers

    # override high level compute_paradigm rather than low level download_data,
    #   because we have no originals to cache: we download form by form and directly in the format we want

    def compute_paradigm(self, lemma, categ, lg):
        features = lg.features_for_category(categ)
        paradigm_tags = translate_tags_to(features, self.tag_type)
        paradigm = {'meta': {'category': categ}, }
        for our_tag, tag in paradigm_tags.items():
            q = f'q[lemma="{lemma}" & tag="{tag}"]'
            word = self.get_cql(q)
            if word == []:
                raise ValueError(
                    f'Got an empty result from SketchEngine for {lemma}, using {tag}')

            # temporary change to make a step forward for now --- uses the first element, 2nd = doublet
            elif type(word) == list and len(word) > 1:
                self.store_form_and_doublets(word, our_tag, paradigm)
            ###

            paradigm[our_tag] = word
        # cqls = {tag: f'q[lemma="{lemma}" & tag="{tag}"]' for tag in paradigm_tags}
        # paradigm = {tag: self.get_cql(cql) for tag, cql in cqls.items()}
        self.write_paradigm(paradigm, lemma, lg)
        return paradigm

    def __init__(self, corpus, lg=None, tag_type=None) -> None:
        self.tag_type = tag_type
        super().__init__(corpus, lg)



if __name__ == '__main__':
    sketch = SketchEngine('preloaded/sktenten11_rft1', 'sk')
    # sketch = SketchEngine('preloaded/cstenten17_mj2', 'cz)
    lemmas = sketch.get_lemmas('k1g.nSc1', 3)
    print(sketch.get_nominal_forms('mačka', 'sk'))
    # lemma  = list(lemmas)[-1]

    # paradigm = sketch.get_nominal_forms('hra')
    # print(paradigm)
