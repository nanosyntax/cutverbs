from scraper.sketch_paradigms import SketchParadigms

class SketchParadigmsUa(SketchParadigms):

    def __init__(self):
        super().__init__('preloaded/uktenten20_rft1', 'ukr', 'multext-east')
        self.dont_remove_ne = ['ненавидіти', 'yyy']
        self.neg_prefix     = 'ne'

    def adjust_paradigm(self, wparadigm, categ=None, lg=None):
        meta = wparadigm['meta']
        finite = wparadigm['finite']
        # raise ValueError('TODO')
        return wparadigm

    def disambiguate_person(self, forms, p, num, meta, finite):
        participles = ['ило', 'ала', 'али']
        without_participles = [form for form in forms if form[-3:] not in participles]
        n = len(without_participles)
        if n == 1:      return None, without_participles[0]
        elif n == 0:    return 'incomplete', forms
        else:           return 'ambiguous', forms


    def post_disambiguate_paradigm(self, finite, meta, infinitive):
        finite = self.remove_redundant(finite, infinitive)
        ambig  = [ws for ws in finite.values() if type(ws) == list]
        if ambig:   return 'ambiguous' 
        else:       return None

    def remove_reflexive(self, data, infinitive, meta):
        n_refl = 0
        refl = ['ся', 'сь']
        if infinitive[-2:] in refl:
            meta['reflexive'] = True
            infinitive = infinitive[:-2]
            n_refl = 1
        forms = data[('1', 'sg')] or data[('2', 'sg')] or data[('3', 'sg')]
        for form in forms:
            if form[-2:] in refl:
                n_refl += 1
        if n_refl > 0:
            meta['reflexive'] = True
            for ff, forms in data.items():
                clean = [form.strip('ся').strip('сь') for form in forms]
                data[ff] = clean
        # TODO: strip t in 3sg in one class
        return data, infinitive

    def pre_classify(self, wparadigm):
        if len(wparadigm) == 0:
            return None
        # if sg3 := wparadigm.get(('3', 'sg')):
        #     return sg3[-1]
        # if sg2 := wparadigm.get(('2', 'sg')):
        #     return sg2[-2]
        return None

