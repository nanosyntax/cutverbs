from scraper.sketch_paradigms import SketchParadigms
from unicodedata import normalize

class SketchParadigmsSk(SketchParadigms):

    # https://www.sketchengine.eu/sktenten-slovak-corpus/
    # https://www.sketchengine.eu/slovak-part-of-speech-tagset/

    def __init__(self):
        super().__init__('preloaded/sktenten11_rft1', 'svk', 'Majka.sk') # 'sktenten_2', 'SNK' ?
        self.dont_remove_ne = ['nést', 'yyy']
        self.neg_prefix     = 'ne'

    def disambiguate_person(self, forms, p, n, meta, finite):
        po = [w for w in forms if w[:2] == 'po']
        if len(po) == 1 and po[0][2:] in forms:
            forms.remove(po[0]) # rastiem/porastiem
            if len(forms) == 1:
                return None, forms[0]
        uc = [w for w in forms if w[-2:] == 'úc']
        if len(uc) > 0:
            assert len(uc) == 1
            forms.remove(uc[0])
            if len(forms) == 1:
                return None, forms[0]
        forms = remove_length_alternations(forms)
        if len(forms) == 1:
            return None, forms[0]
        return 'ambiguous', forms

    def pre_classify(self, wparadigm):
        if len(wparadigm) == 0:
            return None
        if sg3 := wparadigm.get(('3', 'sg')):
            return sg3[-1]
        if sg2 := wparadigm.get(('2', 'sg')):
            return sg2[-2]
        return None

    def post_disambiguate_paradigm(self, finite, meta, infinitive):
        finite = self.remove_redundant(finite, infinitive)
        finite = self.remove_imperatives(finite, infinitive)
        ambig  = [ws for ws in finite.values() if type(ws) == list]
        if ambig:   return 'ambiguous' 
        else:       return None

    def remove_imperatives(self, finite, infinitive):
        ambig  = [ws for ws in finite.values() if type(ws) == list]
        if ambig:
            finite = self.do_remove_imperatives(finite, infinitive)
        return finite


    def do_remove_imperatives(self, finite, infinitive):
        if  type(form := finite[('1', 'sg')]) == str:
            stem = form[:-1]
        elif type(form := finite[('2', 'sg')]) == str:
            stem = form[:-1]
        elif type(form := finite[('3', 'sg')]) == str:
            stem = form
        else:
            return finite
        simplified = False
        imperatives = mk_imperatives(stem)
        for ff, ws in finite.items():
            if type(ws) == list:
                no_imperatives = [w for w in ws if w not in imperatives]
                if len(no_imperatives) < len(ws):
                    finite[ff] = no_imperatives if len(no_imperatives) > 1 else no_imperatives[0]
                    simplified = True
        if simplified:  return self.remove_redundant(finite, infinitive)
        else:           return finite



def remove_length_alternations(forms):
    correspondances = []
    for w in forms:
        short = w.replace('í', 'i').replace('á', 'a')
        if short != w:
            correspondances.append(short)
    for short in correspondances:
        if short in forms:
            forms.remove(short) # will fail two accented correspond to the same short....
    return forms


def mk_imperatives(stem):
    imp = do_mk_imperatives(stem)
    ascii = normalize('NFKD', imp[0]).encode('ascii', 'ignore').decode()
    if ascii != imp[0]:
    # if not ascii == normalize('NFKD', stem).encode():
        # imp = imp + do_mk_imperatives(ascii)
        imp.append(ascii)
        imp.append(normalize('NFKD', imp[1]).encode('ascii', 'ignore').decode())
        imp.append(normalize('NFKD', imp[2]).encode('ascii', 'ignore').decode())
    return imp

def do_mk_imperatives(stem):
    # TODO: cleanup. This code is a mess.
    if stem[-1] in 'aá':        stem = stem + 'j'
    elif stem[-1] in 'í':       stem = pal(stem[:-1])  # vraví-m => 'vrav-me'
    elif stem[-3:] == 'uje':    stem = stem[:-1]
    elif stem[-2:] == 'me':
        stem = stem[:-1] + 'i'  # vezme => vezmi . But klame?
    elif stem[-1]  == 'e':      stem = pal(stem[:-1])        # jebe => jebme
    elif stem[-1] in 'd':       stem = stem[:-1] + 'ď'
    # elif stem[-2:] == 'dí':     stem = stem[:-2] + 'ď'  # prebudí -> prebuďme
    elif stem[-2:] == 'ží':     stem = stem[:-1]        # 'držím'
    return [stem, stem + 'me', stem + 'te']

def pal(stem):
    if stem[-1] == 'd':     return stem[:-1] + 'ď'
    if stem[-1] == 't':     return stem[:-1] + 'ť'
    # TODO more cases
    return stem

# def is_imperative_alternation(forms, fin_suff, imp_suff):
#     fin_matchers = [w for w in forms if w.endswith(fin_suff)]
#     imp_matchers = [w for w in forms if w.endswith(imp_suff)]
#     if len(fin_matchers) == 1 and len(imp_matchers) == 2:
#         # sometimes both forms end ime/Xme but not as imperative alternation (stime/stíme)
#         fin_stem = fin_matchers[0][:-3]
#         if (fin_stem + imp_suff) in forms: #  or f'{fin_stem}'{imp_suff}' in forms
#             return fin_matchers[0]
#     return False


if __name__ == '__main__':
    assert mk_imperatives('da')[1] == 'dajme'
    assert mk_imperatives('jebe')[1] == 'jebme'
    assert mk_imperatives('dakuje')[1] == 'dakujme'
    assert len(mk_imperatives('oddá')) == 6
    assert mk_imperatives('prebudí')[1] == 'prebuďme' 
    assert mk_imperatives('drží')[1] == 'držme'
