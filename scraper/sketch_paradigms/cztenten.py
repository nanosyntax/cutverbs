from scraper.sketch_paradigms import SketchParadigms

class SketchParadigmsCz(SketchParadigms):

    def __init__(self):
        super().__init__('preloaded/cstenten17_mj2', 'cze', 'Majka.cz')
        self.dont_remove_ne = ['nést', 'yyy']
        self.neg_prefix     = 'ne'

    def adjust_paradigm(self, wparadigm):
        meta = wparadigm['meta']
        finite = wparadigm['finite']
        if meta['class'] == 'u_i_alternant' and finite[('3', 'pl')][-1] == 'í':
            meta['subclass'] = 'u_i_3pl_í_only_cztenten'
            finite[('3', 'pl')] = finite[('3', 'pl')][:-1] + 'ou'
        return wparadigm

    def disambiguate_person(self, forms, p, n, meta, finite):
        if p == '1' and n == 'sg':
            if best_alternant := is_alternating_between_endings(forms, ['u', 'i']): # is_uju_uji(forms)
                meta['class'] = 'u_i_alternant'
                return None, best_alternant
        if p == '3' and n == 'pl':
            last = [w[-1] for w in forms]
            last.sort()
            if last == ['í', 'í']:
                jí0 = forms[0][-2:] == 'jí' # either ějí or ejí, so take only 2
                jí1 = forms[1][-2:] == 'jí'
                if jí0 and not jí1:
                    meta['class'] = 'ejí'
                    meta['ambig'] = 'í'
                    return None, forms[0]
                elif jí1 and not jí0:
                    meta['class'] = 'ejí'
                    meta['ambig'] = 'í'
                    return None, forms[1]
            if best_alternant := is_alternating_between_endings(forms, ['u', 'í']):
                meta['class'] = 'u_i_alternant'
                return None, best_alternant
        return 'ambiguous', forms

    def pre_classify(self, wparadigm):
        if len(wparadigm) == 0:
            return None
        if sg3 := wparadigm.get(('3', 'sg')):
            return sg3[-1]
        if sg2 := wparadigm.get(('2', 'sg')):
            return sg2[-2]
        return None


    # def parse_paradigm(self, data, word, categ):
    #     finite = self.remove_negation(data, word)
    #     meta = {}
    #     failure = None
    #     for (p, num), forms in finite.items():
    #         if len(forms) == 1:
    #             finite[(p, num)] = forms[0]
    #             continue
    #         elif len(forms) == 0:
    #             failure = 'incomplete'
    #             # finite[(p, num)] = False # None? use one for verb has no form, other for corpus doesn't have the form
    #         else: # >1
    #             err, winner = disambiguate_person(forms, p, num, meta)
    #             finite[(p, num)] = winner
    #             if err: failure = err
    #     if failure:
    #         return False
    #     if not meta.get('class') and (conj_class := classify(finite)):
    #         meta['class'] = conj_class
    #     finite, meta = self.post_parse_adjustments(finite, meta)
    #     return {'finite': finite, 'meta': meta}


# def remove_negation(finite, infinitive):
#     dont_remove = ['nést', 'yyy']
#     if infinitive in dont_remove:
#         return finite
#     positive = {}
#     for ff, forms in finite.items():
#         clean = []
#         for form in forms:
#             if form.startswith('ne'):
#                 form = form[2:]
#             if not form in clean:
#                 clean.append(form)
#         positive[ff] = clean
#     return positive

# def is_uju_uji(forms):
#     sorted = {}
#     for form in forms:
#         last = form[-1]
#         if not last in ['u', 'i']: # sometimes more forms!!
#             return False
#         sorted[last] = form
#     return sorted['u']

def is_alternating_between_endings(forms, endings): # first ending is the winner
    sorted = {}
    for form in forms:
        last = form[-1]
        if not last in endings: # sometimes more forms!!
            return False
        sorted[last] = form
    if len(sorted) != len(endings):
        return False
    return sorted[endings[0]]

