from scraper.sketch_base import SketchBase
from scraper.tags import tags_for_present
import ast

class SketchParadigms(SketchBase):

    def download_data(self, lemma, _categ):
        tags = tags_for_present(self.tagset) # cache it?
        return {ff: self.get_tag(t, lemma) for ff, t in tags.items()}

    def write_originals(self, data, word):
        super().write_originals(str(data), word) # convert dict to string

    def cached_raw_data(self, word):
        text = super().cached_raw_data(word)
        return ast.literal_eval(text) if text else None


    # Quick fix to downloader verbal paradigms from sketchengine, form by form.
    # Later try using the SketchEngine version of compute_paradigm()
    # TODO = Write the parse_later, reusing finite_from_sketch

    # # go back to original Scraper behaviour: we now store raw originals for ALL requests, not only partial ones.
    # def compute_paradigm(self, word, categ, lg):
    #     data = self.get_raw_data(word, categ)
    #     paradigm = self.extract_paradigm(data, word, categ, lg)
    #     return paradigm



    ### --------  parse_paradigm helpers

    def parse_paradigm(self, data, lemma, categ):
        meta = {}
        finite, lemma = self.pre_disambiguate_paradigm(lemma, data, meta)
        failure = None
        for (p, num), forms in finite.items():
            if len(forms) == 1:
                finite[(p, num)] = forms[0]
                continue
            elif len(forms) == 0:
                failure = 'incomplete'
                # finite[(p, num)] = False # None? use one for verb has no form, other for corpus doesn't have the form
            else: # >1
                err, winner = self.disambiguate_person(forms, p, num, meta, finite)
                if err: failure = err
                else:   finite[(p, num)] = winner
        if failure: # disambiguate now that cleaned up (even if some persons are missing!)
            failure = self.post_disambiguate_paradigm(finite, meta, lemma)
            if failure == 'ambiguous':
                print('still ambig', winner)
        if failure:
            return False
        if not meta.get('class') and (conj_class := self.pre_classify(finite)):
            meta['class'] = conj_class
        return {'finite': finite, 'meta': meta}
        # pdgm = self.adjust_paradigm(pdgm)
        # return pdgm

    def pre_disambiguate_paradigm(self, infinitive, data, meta):
        data = self.remove_negation(data, infinitive)
        data, infinitive = self.remove_reflexive(data, infinitive, meta)
        return data, infinitive

    def post_disambiguate_paradigm(self, finite, meta, infinitiv):
        return 'ambiguous'

    def disambiguate_person(self, forms, p, num, meta, finite):
        raise ValueError('subclass needs to implement this function')

    def pre_classify(self, finite):
        raise ValueError('subclass needs to implement this function')
    
    def remove_reflexive(self, data, infinitive, meta):
        return data, infinitive

    def remove_negation(self, finite, infinitive):
        if infinitive in self.dont_remove_ne:
            return finite
        positive = {}
        neg_len = len(self.neg_prefix)
        for ff, forms in finite.items():
            clean = []
            for form in forms:
                if form.startswith(self.neg_prefix):
                    form = form[neg_len:]
                if not form in clean:
                    clean.append(form)
            positive[ff] = clean
        return positive


    def remove_redundant(self, finite, infinitive):
        simplified = False
        unambig = [w for w in finite.values() if type(w) == str]
        for ff, ws in finite.items():
            if type(ws) == list:
                no_redundant = [w for w in ws if w not in unambig and w != infinitive]
                if len(no_redundant) < len(ws):
                    finite[ff] = no_redundant[0] if len(no_redundant) == 1 else no_redundant
                    simplified = True
        if simplified:  return self.remove_redundant(finite, infinitive)
        else:           return finite

