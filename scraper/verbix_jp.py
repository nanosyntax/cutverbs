import time, random
from urllib.parse import quote_plus
from pathlib      import Path
from bs4          import BeautifulSoup
from playwright.sync_api import sync_playwright


def get_paradigm(word):
    data     = get_html(word)
    return parse_paradigm(data, word)

kuru_parse = {
('Nonpast Indicative', 'plain', 'positive'): 'くる', 
 ('Nonpast Indicative', 'polite', 'positive'): 'きます', 
 ('Nonpast Indicative', 'plain', 'negative'): 'こない', 
 ('Nonpast Indicative', 'polite', 'negative'): 'きません', 
 ('Past Indicative', 'plain', 'positive'): 'きた', 
 ('Past Indicative', 'polite', 'positive'): 'きました', 
 ('Past Indicative', 'plain', 'negative'): 'こなかゝた', 
 ('Volitional', 'plain', 'positive'): 'こよう', 
 ('Volitional', 'polite', 'positive'): 'きましょう', 
 ('Volitional', 'plain', 'negative'): 'くるまい', 
 ('Volitional', 'polite', 'negative'): 'きますまい', 
 ('Continuative (te-form)', 'polite i', 'positive'): 'きて', 
 ('Continuative (te-form)', 'polite', 'positive'): 'きまして', 
 ('Continuative (te-form)', 'polite ii', 'negative'): 'こなくて', 
 ("Continuative (ren'youkei)",): 'き', 
 ('Imperative', 'plain', 'positive'): 'きなさい', 
 ('Imperative', 'abrupt', 'negative'): 'くるな', 
 ('Provisional', 'positive'): 'くれば', 
 ('Provisional', 'negative'): 'こなければ', 
 ('Conditional', 'plain', 'positive'): ' きたら', 
 ('Conditional', 'polite', 'positive'): 'きましたら', 
 ('Conditional', 'plain', 'negative'): 'こなかゝたら', 
 ('Alternative', 'plain', 'positive'): 'きたり', 
 ('Alternative', 'polite', 'positive'): 'きましたり', 
 ('Alternative', 'plain', 'negative'): 'こなかゝたり', 
 ('Potential',): 'こられる', 
 ('Passive / Respectful',): 'こられる',
 ('Causative',): 'こさせる', 
 ('Causative Passive',): 'こさせられる', 
 ('Honorific', 'infinitive i'): 'いらゝしゃる', 
 ('Humble', 'infinitive i'): 'まいる'
 }


# ---- PRIVATE ------------------------------------

html_dir = Path('data/external/verbix/jp/html')
html_dir.mkdir(parents=True, exist_ok=True)

def get_html(word):
    file_path = html_dir / word
    if file_path.is_file():
        return file_path.read_text('utf-8')

    time.sleep(random.randint(5, 16))
    escaped_word = quote_plus(word)
    # url = f'https://www.verbix.com/webverbix/go.php?&D1=31&T1={escaped_word}' # 'https://www.verbix.com/webverbix/go.php?&D1=31&T1=gitmek'
    url = f'https://www.verbix.com/webverbix/japanese/{escaped_word}'
    html = fetch_remote_html(url)

    cache_original(html, word)
    return html

def cache_original(data, word):
    file_path = html_dir / word  # use nom.sg / 1.sg.pres instead of word?
    with open(file_path, 'w', encoding='utf-8') as f:
        f.write(data)

def fetch_remote_html(url):
    with sync_playwright() as playwright: # hyper slow, creates a browser for each page that we download...
        browser = playwright.chromium.launch(headless=True)
        page = browser.new_page()
        page.goto(url)
        html = page.content()
        return(html)


def parse_paradigm(html, word):
    bhtml = BeautifulSoup(html)
    table = bhtml.find("div", id="verbixConjugations")
    tense_nodes = table.find('section', class_='verbtable', recursive=False)
    tense_nodes = tense_nodes.find("div", class_="columns-main")
    tense_nodes = tense_nodes.find_all(recursive=False)
    verb_class = tense_nodes[0].text.split()[-1]
    tense_nodes = tense_nodes[2:-5]   # 1st=verb-type, 2nd=glossary. End: kanji, similar-V, translations, info, sample sentences

    tenses = [parse_tense(node) for node in tense_nodes]
    paradigm = dict(tenses)
    paradigm['meta'] = {'jp_type': verb_class}
    return paradigm

def parse_tense(node):
    # each tense is organized in 3 tables of positive forms (romanised, kanji, kana)
    #   and optionally 3 tables of negative forms
    [name_n, data_n] = node.find_all(recursive=False)
    tense_name = name_n.text
    data = data_n.find_all(recursive=False)
    tense = {}
    if len(data) == 6: # this tense has both positive and negative forms
        for features, v_form in parse_tense_polarity(data[2], 'Affirmative'):
            features.append('positive')
            tense[tuple(features)] = v_form
        for features, v_form in parse_tense_polarity(data[3], 'Negative'):
            features.append('negative')
            tense[tuple(features)] = v_form
    else:
        for features, v_form in parse_tense_polarity(data[1], 'Affirmative'):
            tense[tuple(features)] = v_form # no positive/negative contrast, so don't add those features
        if len(tense) == 1 and tense.get( () ):
            tense = tense[ () ]
    return tense_name, tense

def parse_tense_polarity(node, title):
    # [title_n, content_n] = node.find_all(recursive=False)
    kids = node.find_all(recursive=False)
    assert kids[0].text == title
    content_n = kids[1]
    if content_n.name == 'table':
        rows = content_n.find_all('tr')
        return [parse_row(r) for r in rows]
    elif content_n.name == 'span':
        return [([], content_n.text)] # a list of one feature-value pair, where features is empty...
    raise ValueError(f"I don't understand this verb form tag {content_n.name}")

def parse_row(row):
    [features_n, verb_n] = row.find_all('td')
    return [features_n.text.lower()], verb_n.text


if __name__ == '__main__':
    p = get_paradigm('hanasu')
    print(p)
