import asyncio
from playwright.async_api import async_playwright

"""
For future Russian downloader? (From phind)
https://conjugator.reverso.net/conjugation-russian.html
The conjugator is based on a large database of real-world examples and is constantly updated
"""

async def get_conjugations(verb: str):
    async with async_playwright() as p:
        browser = await p.chromium.launch()
        context = await browser.new_context()
        page = await context.new_page()
        await page.goto('https://conjugator.reverso.net/conjugation-ukrainian.html')

        await page.fill('input#search-conj', verb)
        await page.click('button#btn-conj')
        await page.wait_for_load_state("networkidle")

        conjugation_headers = await page.query_selector_all("div.blue-wrap div.conj-tense-block h4")
        conjugation_tables = await page.query_selector_all("div.blue-wrap div.conj-tense-block table")

        conjugations = {}
        for header, table in zip(conjugation_headers, conjugation_tables):
            tense = await header.inner_text()
            forms = await table.query_selector_all("tr")
            conjugations[tense] = {}

            for form in forms:
                pronoun, conjugated_verb = await form.query_selector_all("td")
                pronoun_text = await pronoun.inner_text()
                conjugated_verb_text = await conjugated_verb.inner_text()
                conjugations[tense][pronoun_text] = conjugated_verb_text

        await browser.close()
        return conjugations

if __name__ == '__main__':
    verb = "читати"
    conjugations = asyncio.run(get_conjugations(verb))
    print(conjugations)
