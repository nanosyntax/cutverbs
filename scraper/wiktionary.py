import requests
from bs4 import BeautifulSoup
from urllib.parse import unquote


base_url = 'https://en.wiktionary.org'
# page_url = "https://en.wiktionary.org/wiki/Category:Ukrainian_verbs"

lg_path = {
    'ukr':  'Ukrainian_verbs',
    'svk': 'Slovak_verbs',
    'cze':  'Czech_verbs',
}

class Wiktionary():

    def __init__(self, lg):
        self.lg = lg
        # name = 'wiktionary_' + lg
        # super().__init__(name)
        self.url = f'{base_url}/wiki/Category:{lg_path[lg]}'

    def extract_all_verbs(self):
        soup = None
        page_url = self.url
        with open(f"data/paradigms/{self.lg}/wiktionary_{self.lg}.txt", "w") as file:
            while True:
                if soup:
                    page_path = get_next_page_url(soup)
                    if not page_path:
                        break
                    page_url  = f'{base_url}{page_path}'

                verbs, soup = self.extract_verbs_from_page(page_url)
                if not verbs:
                    raise ValueError('no verbs on this page???')

                print(f"Got {len(verbs)} verbs")
                file.write("\n".join(verbs) + "\n")


    def extract_verbs_from_page(self, page_url):
        response = requests.get(page_url)
        soup = BeautifulSoup(response.text, "html.parser")

        mw_pages_div = soup.find(id="mw-pages")
        # Find all the links in the category
        links = mw_pages_div.find_all("a", href=lambda x: x and x.startswith('/wiki/'))
        all_links = mw_pages_div.find_all("a", href=lambda x: x and x.startswith('https://en.wiktionary.org/wiki/') and x.endswith('#Ukrainian'))
        # https://en.wiktionary.org/wiki/%D0%B0%D0%B1%D0%BE%D1%80%D1%82%D1%83%D0%B2%D0%B0%D1%82%D0%B8#Ukrainian

        # Extract the verbs
        verbs = []
        for link in links:
            url_path = link["href"]
            # parsed_url = urlparse(url_path)
            # Extract the part after /wiki/ and before '#Ukrainian'
            # encoded_verb = parsed_url.path.split('#')[0].split('/')[-1]
            encoded_verb = url_path.split('/')[-1]
            # Convert the URL-encoded part to normal text
            decoded_verb = unquote(encoded_verb)
            verbs.append(decoded_verb)
        return verbs, soup

def get_next_page_url(page_soup):
    next_page_links = page_soup.find_all("a", text="next page")
    if not next_page_links:
        return None
    return next_page_links[0]["href"]


if __name__ == '__main__':
    scraper = Wiktionary('svk') # cze ukr svk
    scraper.extract_all_verbs()
