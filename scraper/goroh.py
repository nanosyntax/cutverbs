import re
from bs4 import BeautifulSoup

from cutverbs.utils import walk
from scraper import Scraper

class Goroh(Scraper):

    def url(self, escaped_word):
        base_url = 'https://goroh.pp.ua/%D0%A1%D0%BB%D0%BE%D0%B2%D0%BE%D0%B7%D0%BC%D1%96%D0%BD%D0%B0'
        return f'{base_url}/{escaped_word}'

    def parse_paradigm(self, data, word, categ):
        return parse_paradigm(data, word)

    def __init__(self):
        super().__init__('goroh', 'ukr')




####### initial non-class version

# ---- PRIVATE ------------------------------------

# from html import parser # HTMLParser
def parse_paradigm(html, word):
    bhtml = BeautifulSoup(html, 'html.parser') # , 'lxml' html.parser HTMLParser
    table = bhtml.find("table", class_="table")
    if not table:
        print(f'  ****  bad html for {word}')
        return False

    rows = table.findChildren(['tr'])
    del rows[1]  # remove second row

    output = []
    for i in range(len(rows)):
        row = rows[i]

        # remove the rows with names of tenses, vform etc.
        if not re.search("subgroup-header", str(row)):
            row_td = row.findChildren(['td'])
            section = []
            # class of row under "Безособова форма"
            if not re.search("cell ta-center", str(row)):
                # strip the left header and add text from other cells (grouped by rows)
                for j in range(1, len(row_td)):
                    cell = row_td[j]
                    section.append(cell.text)
            else:
                section.append(row_td[0].text)
            output.append(section)
    return create_dict(output)


def create_dict(data):
    n = 6 if len(data) > 11 else 3 # where does tensed form start - row 6 for perfective, row 3 for imperfective
    if len(data) < n+6:
        print(f'  ****  skipping bad data, eg: {data[0]}')
        return False # too little data, we will crash when asking data[n+5] below
    paradigm = {
        'meta': {'category': 'v'},
        'infinitive': data[0][0],
        'imperative': {
            ('1', 'pl'): data[1][1],
            ('2', 'sg'): data[2][0],
            ('2', 'pl'): data[2][1]},
        'finite': {
            ('1', 'sg'): data[n][0],
            ('2', 'sg'): data[n+1][0],
            ('3', 'sg'): data[n+2][0],
            ('1', 'pl'): data[n][1],
            ('2', 'pl'): data[n+1][1],
            ('3', 'pl'): data[n+2][1]},
        'participle': {
            ('masc', 'sg'): data[n+3][0],
                ('fem', 'sg'): data[n+4][0],
            ('neut', 'sg'): data[n+5][0],
            ('pl'): data[n+3][1]},
    }
    if n == 6: # imperfective
        paradigm['meta']['aspect'] = 'imperfective'
        paradigm['future'] = {
            ('1', 'sg'): data[3][0],
            ('2', 'sg'): data[4][0],
            ('3', 'sg'): data[5][0],
            ('1', 'pl'): data[3][1],
            ('2', 'pl'): data[4][1],
            ('3', 'pl'): data[5][1]}
    if len(data) > n+6:
        paradigm['impersonal'] = data[n+6][0]
    paradigm = adjust_paradigm(paradigm)
    return paradigm

def adjust_paradigm(paradigm):
    infinitive = paradigm['infinitive']
    if is_reflexive := infinitive[-2:] in ['сь', 'ся']:
        paradigm['meta']['reflexive'] = True
    for ff, forms_s, tense, f_above in walk(paradigm):
        if len(forms_s) <= 2: # '—'
            return False
        if ',' in forms_s:
            if is_reflexive:
                forms = [strip_reflexive(f, ff) for f in forms_s.split(',') if f.endswith('ся')] # filter out сь forms
            else:
                forms = [f.strip() for f in forms_s.split(',')]
            if len(forms) > 1:
                forms = disambiguate(forms, ff, f_above)
                if not forms:
                    print('  **** could not disambiguate:', forms_s)
                    return False
            forms = forms[0]
        else:
            if is_reflexive:
                assert forms_s.endswith('ся')
                forms = strip_reflexive(forms_s, ff)
            else:
                forms = forms_s
        tense[ff] = forms
    # for ff, form, tense, f_above in walk(paradigm):
    #     if len(form) <= 2: # '—'
    #         return False
    #     if ',' in form:
    #         forms = [f.strip() for f in form.split(',')]
    #         winner = disambiguate(forms, ff, f_above)
    #         if not winner:
    #             print('  **** could not disambiguate:', form)
    #             return False
    #         tense[ff] = winner[0]

    # if infinitive[-4:] in ['тись', 'тися']:
    #     adjust_reflexives(paradigm)
    #     #Nataliya's code
    #     sg3 = paradigm['finite'][('3', 'sg')]
    #     if sg3.endswith('єть'):
    #         paradigm['finite'][('3', 'sg')] = sg3.replace('єть', 'є')
    #     elif sg3.endswith('є́ ть'):
    #         paradigm['finite'][('3', 'sg')] = sg3.replace('є́ ть', 'є́ ')
    #     elif sg3.endswith('еть'):
    #         paradigm['finite'][('3', 'sg')] = sg3.replace('еть', 'е')
    #     elif sg3.endswith('е́ть'):
    #         paradigm['finite'][('3', 'sg')] = sg3.replace('е́ть', 'е́')
    #     return paradigm

    # # 4 places typically have alternations, choose one alternant:
    # if len(data := paradigm['imperative'][('2', 'sg')].split(',')) > 1:     # ['продава́й', ' продава́й-но']
    #     paradigm['imperative'][('2', 'sg')] = data[0]
    # if len(data := paradigm['imperative'][('1', 'pl')].split(',')) > 1:     # ['збері́м', ' збері́мо']
    #     paradigm['imperative'][('1', 'pl')] = data[1].strip()
    # if len(data := paradigm['finite'][('1', 'pl')].split(',')) > 1:         # ['продає́м', ' продаємо́']
    #     paradigm['finite'][('1', 'pl')] = data[1].strip()
    # if paradigm.get('future') and len(data := paradigm['future'][('1', 'pl')].split(',')) > 1: # ['продава́тимем', ' продава́тимемо']
    #     paradigm['future'][('1', 'pl')] = data[1].strip()
    return paradigm


def strip_reflexive(form, ff):
    form = form.strip()
    if ff == ('3', 'sg') and form[-5:] in ['ється', 'еться']:
        return form[:-4]
    elif ff == ('3', 'sg') and form[-6:] in ['е́ться', 'є́ться']:
        return form[:-4]
    else:
        return form[:-2]

def disambiguate(forms, ff, tense_ff):
    if tense_ff == ['imperative'] and ff == ('2', 'sg'):     # ['продава́й', ' продава́й-но']
        clean = [f for f in forms if not f.endswith('-но')]
        if len(clean) != 1:
            raise ValueError('please inspect the situation with', forms)
        return clean[0]
    elif ff == ('1', 'pl'):     # ['збері́м', ' збері́мо']  , ['продає́м', ' продаємо́'] , ['продава́тимем', ' продава́тимемо']
        trimmed = [f for f in forms if f[-1] == 'о' or f[-2:] == 'о́'] # looks like ascii o, but is cyrillic u043e
    if len(trimmed) == 1:  return trimmed
    else:                    return False

def adjust_reflexives(paradigm):
    paradigm['meta']['reflexive'] = True
    for tense_name, tense_val in paradigm.items():
        if tense_name == 'meta':
            continue
        elif type(tense_val) == str:
            paradigm[tense_name] = last_option_no_sja(tense_val)
        else:
            for features, forms in tense_val.items():
                tense_val[features] = last_option_no_sja(forms)

def last_option_no_sja(value):
    if value == '—':
        return ''
    last = value.split(',')[-1].strip()
    if not (last[-2:] in ['ся', 'сь']):
        raise ValueError('reflexive not ending in ся/сь ???')
    return last[:-2]


if __name__ == '__main__':
    print(Goroh().get_paradigm('доїть'))
    print(Goroh().get_paradigm('стрибати'))