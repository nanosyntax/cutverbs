from scraper.sketchengine import SketchEngine
from scraper.prirucka import Prirucka
from scraper.dictcom_sk import DictComSk
from scraper.goroh      import Goroh
from scraper.portalsnk  import PortalSnk

def scraper_named(name):
    # returns instances - consider returning class and creating instances on the fly
    if name == 'příručka':      return Prirucka()
    elif name == 'dictcom_sk':  return DictComSk()
    elif name == 'goroh':       return Goroh()
    elif name == 'portal_snk':  return PortalSnk()
    elif type(name) == list and name[0] == 'sketch':  # ['sketch', lg, corpusName, (tag_type)]
        tag_type = None if len(name) == 3 else name[3]
        return SketchEngine(f'preloaded/{name[1]}', name[2], tag_type)
    else:
        raise ValueError(f"Cannot create a scraper because I don't understand {name}")