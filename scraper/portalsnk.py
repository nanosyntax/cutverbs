from scraper import Scraper
from bs4     import BeautifulSoup


class PortalSnk(Scraper):

    def url(self, escaped_word):
        return f'https://slovnik.juls.savba.sk/?w={escaped_word}&s=exact&c=N7fd&cs=&d=verbdb#'

    def parse_paradigm(self, html, word, categ):
        bhtml   = BeautifulSoup(html) #, 'lxml'    
        results = bhtml.find("div", class_="resultbody")
        if not results:
            self.delete_html_file(word)
            if len(bhtml.select('span.notfound')) > 0:
                self.note_word_not_available(word)
                return False
            ## below is from Prirucka, adapt it to whatever errors Portal throws, if any
            # if len(bhtml.select('#content span.odsazeno')) > 0:
            #     raise ValueError(f'    -- This verb {word} is ambiguous between multiple meanings, please choose another one')
            # elif bhtml.find(id="protest"): # check if id=protest is still the best option when such a word doesn't exist in Priurcka
            #     print(f'    -- Prirucka did not find declension table -- skipping: {word}')
            #     return False # this is not html with paradigm data
            raise ValueError(f'    -- PortalSnk cannot parse the paradigm data for: {word} -- please fix it!')

        items = children(results)
        data = parse_rows(items)
        # assert data['infinitive'] == word # need to strip it to ascii
        return data

    def __init__(self):
        super().__init__('portalsnk', 'svk')



## -- private low level functions

def parse_rows(items):
    data = parse_lemma(items)
    categ = data['meta']['lemma_type']
    if 'sloveso' in categ: # eg dokonavé sloveso, 
        data['meta']['categ'] = 'v'
        data = parse_verb(items, data)
    elif 'meno' in categ:  # eg podstatné meno, ženský rod
        data['meta']['categ'] = 'n'
        data = parse_noun(items, data)
    else:
        raise ValueError('Type of word not handled yet, please fix:' + categ)
    return data

def parse_verb(items, data):
    data = parse_spans(items, data) # can give us data for multiple homphonous words
    verb_data = data if type(data) == dict else data[0]
    if imperatives := verb_data.get('imperative'):
        for ffs, form in imperatives.items():
            imperatives[ffs] = form.rstrip('!')
    return data

def parse_noun(items, data):
    item = nextt(items)
    assert item.name == 'p'
    sub_items = children(item)
    singulars = parse_spans(sub_items, {}, with_tags=False)
    singulars = {(f, 'sg'): v for f, v in singulars.items()}
    item = nextt(items)
    assert item.name == 'p'
    sub_items = children(item)
    plurals   = parse_spans(sub_items, {}, with_tags=False)
    plurals   = {(f, 'pl'): v for f, v in plurals.items()}
    return {**data, **singulars, **plurals}  # merge singulars and plurals

def parse_spans(items, data, with_tags = True):
    while item := nextt(items):
        if item.name == 'br':
            continue
        if item.get('class') and ('hidden' in item['class']):
            continue
        if item.name == 'hr': # homophonous word, non-first homophone starts here
            more = parse_rows(items)
            if more:
                data = [data, more] if type(more) == dict else [data] + more
            continue
        if with_tags:
            categ, ffs, form = parse_tagged_form(item, items)
            if ffs:
                if not data.get(categ):
                    data[categ] = {}
                data[categ][ffs] = form
            else:
                data[categ] = form
        else:
            categ, form = parse_untagged_form(item, items)
            data[categ] = form
    return data

def parse_lemma(items):
    data = {}
    lemma = nextt(items)
    assert lemma['class'][0] == 'b0'
    assert lemma.name == 'b'
    lemma = clean(lemma)
    data['meta'] = {'lemma': lemma}

    lemma_type = nextt(items)
    assert lemma_type.name == 'span'
    assert lemma_type['class'][0] == 'attr'
    data['meta']['lemma_type'] = clean(lemma_type)
    return data

def parse_untagged_form(first, items):
    assert first.name == 'span'
    assert first['class'][0] == 'exam'
    k    = noun_tags[first.text]
    form = clean(nextt(items))
    if form[-1] == ';':
        form = form[:-1]
    return k, form

noun_tags = {
    '(jedna)': 'nom',
    '(dve)':   'nom',
    '(bez)':   'gen',
    '(k)':     'dat',
    '(vidÃ\xadm)': 'acc',  # '(vidím)'
    '(o)':     'loc',
    '(so)':    'instr',
}

def parse_tagged_form(first, items):
    assert first.name == 'span'
    assert first['class'][0] == 'exam'
    form = clean(nextt(items))
    tag  = nextt(items)
    assert tag.name == 'span'
    assert tag['class'][0] == 'attr'
    tag = clean(tag)
    categ, ffs = tag_to_ffs(tag)
    return categ, ffs, form

def children(el):
    return filter(lambda e: e not in [' ', '; ', '\n'], el.children)

# TODO: centralise tag info elsehwere, unifying with tags.py for nouns
def tag_to_ffs(tag):
    # SNK tagset: https://www.sketchengine.eu/slovak-part-of-speech-tagset/
    # the 3rd position is finite/non/both, but unreliable, ignore it.
    if (t2 := tag[:2]) == 'VK': # VKesa+, VKesb+, VKesc+, VKepa+, VKesp+, VKepc+
        assert len(tag) == 6
        match(tag[-3:]):
            case 'sa+':  ffs = ('finite', ('1', 'sg'))
            case 'sb+':  ffs = ('finite', ('2', 'sg'))
            case 'sc+':  ffs = ('finite', ('3', 'sg'))
            case 'pa+':  ffs = ('finite', ('1', 'pl'))
            case 'pb+':  ffs = ('finite', ('2', 'pl'))
            case 'pc+':  ffs = ('finite', ('3', 'pl'))
    elif t2 == 'VL': # VLesam+, VLesaf+, VLesan+, VLepah+
        assert len(tag) == 7
        match(tag[-4:]):
            case 'sam+': ffs = ('participle', ('m', 'sg'))
            case 'saf+': ffs = ('participle', ('f', 'sg'))
            case 'san+': ffs = ('participle', ('n', 'sg'))
            case 'pah+': ffs = ('participle', ('pl',))
    elif t2 == 'VM': # VMesb+, VMepa+, VMepb+
        assert len(tag) == 6
        match(tag[-3:]):
            case 'sb+':  ffs = ('imperative', ('2', 'sg'))
            case 'pa+':  ffs = ('imperative', ('1', 'pl'))
            case 'pb+':  ffs = ('imperative', ('2', 'pl'))
    elif t2 == 'VH': # 'VHe+', 'VHj+', 'VHd+'
        assert len(tag) == 4
        assert tag[-1]  == '+'
        ffs = ('gerund', ())
    return ffs

# def tag_to_ffs(tag):
#     # SNK tagset: https://www.sketchengine.eu/slovak-part-of-speech-tagset/
#     match(tag):
#         case 'VKesa+':  ffs = ('finite', ('1', 'sg'))
#         case 'VKesb+':  ffs = ('finite', ('2', 'sg'))
#         case 'VKesc+':  ffs = ('finite', ('3', 'sg'))
#         case 'VKepa+':  ffs = ('finite', ('1', 'pl'))
#         case 'VKepb+':  ffs = ('finite', ('2', 'pl'))
#         case 'VKepc+':  ffs = ('finite', ('3', 'pl'))
#         case 'VLesam+': ffs = ('participle', ('m', 'sg'))
#         case 'VLesaf+': ffs = ('participle', ('f', 'sg'))
#         case 'VLesan+': ffs = ('participle', ('n', 'sg'))
#         case 'VLepah+': ffs = ('participle', ('pl',))
#         case 'VMesb+':  ffs = ('imperative', ('2', 'sg'))
#         case 'VMepa+':  ffs = ('imperative', ('1', 'pl'))
#         case 'VMepb+':  ffs = ('imperative', ('2', 'pl'))
#         case 'VHe+':    ffs = ('gerund', ())

#         # j = both finite and non-finite...
#         case 'VKjsa+':  ffs = ('finite', ('1', 'sg'))
#         case 'VKjsb+':  ffs = ('finite', ('2', 'sg'))
#         case 'VKjsc+':  ffs = ('finite', ('3', 'sg'))
#         case 'VKjpa+':  ffs = ('finite', ('1', 'pl'))
#         case 'VKjpb+':  ffs = ('finite', ('2', 'pl'))
#         case 'VKjpc+':  ffs = ('finite', ('3', 'pl'))
#         case 'VLjsam+': ffs = ('participle', ('m', 'sg'))
#         case 'VLjsaf+': ffs = ('participle', ('f', 'sg'))
#         case 'VLjsan+': ffs = ('participle', ('n', 'sg'))
#         case 'VLjpah+': ffs = ('participle', ('pl',))
#         case 'VMjsb+':  ffs = ('imperative', ('2', 'sg'))
#         case 'VMjpa+':  ffs = ('imperative', ('1', 'pl'))
#         case 'VMjpb+':  ffs = ('imperative', ('2', 'pl'))
#         case 'VHj+':    ffs = ('gerund', ())

#     return ffs


# portal says it uses utf8 but in fact uses iso-8859-1...
def clean(el):
    return bytes(el.text.strip(),'iso-8859-1').decode('utf-8')

def nextt(iterator): # instead of raising past end, return None
    try:
        return next(iterator)
    except:
        return None


if __name__ == '__main__':
    portal = PortalSnk()
    pdgm = portal.get_paradigm("stať") # stať robit'
    print(pdgm)

# URL: https://slovnik.juls.savba.sk/?d=verbdb
