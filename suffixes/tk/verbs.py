suffixes = {
    ('1', 'sg', 'bare') : 'm',
    ('1', 'sg', 'i') : 'im',
    ('1', 'sg', 'ı') : 'ɯm',
    ('1', 'sg', 'ü') : 'ym',
    ('1', 'sg', 'u') : 'um',
    ('1', 'sg', 'yi') : 'jim',
    ('1', 'sg', 'yı') : 'jɯm',
    ('1', 'sg', 'yü') : 'jym',
    ('1', 'sg', 'yu') : 'jum',
    ('2', 'sg', 'bare') : 'n',
    ('2', 'sg', 'i') : 'sin',
    ('2', 'sg', 'ı') : 'sɯn',
    ('2', 'sg', 'ü') : 'syn',
    ('2', 'sg', 'u') : 'sun',
    ('3', 'sg') : 'Ø',
    ('1', 'pl', 'bare') : 'k',
    ('1', 'pl', 'i') : 'iz',
    ('1', 'pl', 'ı') : 'ɯz',
    ('1', 'pl', 'ü') : 'yz',
    ('1', 'pl', 'u') : 'uz',
    ('1', 'pl', 'yi') : 'jiz',
    ('1', 'pl', 'yı') : 'jɯz',
    ('1', 'pl', 'yü') : 'jyz',
    ('1', 'pl', 'yu') : 'juz',
    ('2', 'pl', 'ı') : 'nɯz',
    ('2', 'pl', 'i') : 'niz',
    ('2', 'pl', 'ü') : 'nyz',
    ('2', 'pl', 'u') : 'nuz',
    ('2', 'pl', 'sı') : 'sɯnɯz',
    ('2', 'pl', 'si') : 'siniz',
    ('2', 'pl', 'sü') : 'synyz',
    ('2', 'pl', 'su') : 'sunuz',
    ('3', 'pl', 'a') : 'laɾ',
    ('3', 'pl', 'e') : 'leɾ',
    ('negative', 'a') : 'ma', 
    ('negative', 'e') : 'me',
    ('negative', 'u') : 'mu',
    ('negative', 'ı') : 'mɯ',
    ('negative', 'ü') : 'my',
    ('negative', 'i') : 'mi,',
    ('optative','1', 'sg', 'e') : 'ejim',
    ('optative','1', 'sg', 'a') : 'ajɯm',
    ('optative','1', 'sg', 'ye') : 'jejim',
    ('optative','1', 'sg', 'ya') : 'jajɯm',
    ('optative','2', 'sg', 'e') : 'esin',
    ('optative','2', 'sg', 'a') : 'asɯn',
    ('optative','2', 'sg', 'ye') : 'jesin',
    ('optative','2', 'sg', 'ya') : 'jasɯn',
    ('optative','3', 'sg', 'e') : 'e',
    ('optative','3', 'sg', 'a') : 'a',
    ('optative','3', 'sg', 'ye') : 'je',
    ('optative','3', 'sg', 'ya') : 'ja',
    ('optative','1', 'pl', 'e') : 'elim',
    ('optative','1', 'pl', 'a') : 'alɯm',
    ('optative','1', 'pl', 'ye') : 'jelim',
    ('optative','1', 'pl', 'ya') : 'jalɯm',
    ('optative','2', 'pl', 'e') : 'esiniz',
    ('optative','2', 'pl', 'a') : 'asɯnɯz',
    ('optative','2', 'pl', 'ye') : 'jesiniz',
    ('optative','2', 'pl', 'ya') : 'jasɯnɯz',
    ('optative','3', 'pl', 'e') : 'eleɾ',
    ('optative','3', 'pl', 'a') : 'alaɾ',
    ('optative','3', 'pl', 'ye') : 'jeleɾ',
    ('optative','3', 'pl', 'ya') : 'jalaɾ',
    ('imperative','2', 'sg') : 'Ø',
    ('imperative','3', 'sg', 'ı') : 'sɯn',
    ('imperative','3', 'sg', 'i') : 'sin',
    ('imperative','3', 'sg', 'u') : 'sun',
    ('imperative','3', 'sg', 'ü') : 'syn',
    ('imperative','2', 'pl', 'ı') : 'ɯn',
    ('imperative','2', 'pl', 'i') : 'in',
    ('imperative','2', 'pl', 'u') : 'un',
    ('imperative','2', 'pl', 'ü') : 'yn',
    ('imperative','2', 'pl', 'yı') : 'jɯn',
    ('imperative','2', 'pl', 'yi') : 'jin',
    ('imperative','2', 'pl', 'yu') : 'jun',
    ('imperative','2', 'pl', 'yü') : 'jyn',
    ('imperative','2', 'pl', 'formal', 'ı') : 'ɯnɯz',
    ('imperative','2', 'pl', 'formal', 'i') : 'iniz',
    ('imperative','2', 'pl', 'formal', 'u') : 'unuz',
    ('imperative','2', 'pl', 'formal', 'ü') : 'ynyz',
    ('imperative','2', 'pl', 'formal', 'yı') : 'jɯnɯz',
    ('imperative','2', 'pl', 'formal', 'yi') : 'jiniz',
    ('imperative','2', 'pl', 'formal', 'yu') : 'junuz',
    ('imperative','2', 'pl', 'formal', 'yü') : 'jynyz',
    ('imperative','2', 'pl', 'ı') : 'sɯnlaɾ',
    ('imperative','2', 'pl', 'i') : 'sinleɾ',
    ('imperative','2', 'pl', 'u') : 'sunlaɾ',
    ('imperative','2', 'pl', 'ü') : 'synleɾ',
    ('aorist', 'e') : 'eɾ',
    ('aorist', 'a') : 'aɾ',
    ('aorist', 'i') : 'iɾ',
    ('aorist', 'ı') : 'ɯɾ',
    ('aorist', 'ü') : 'yɾ',
    ('aorist', 'u') : 'uɾ',
    ('aorist', 'Ø') : 'ɾ',
    ('neg', 'aorist', 'e') : 'mez',
    ('neg', 'aorist', 'a') : 'maz',
    ('impotential', 'aorist', 'e') : 'emez',
    ('impotential', 'aorist', 'a') : 'amaz',
    ('impotential', 'aorist', 'ye') : 'jemez',
    ('impotential', 'aorist', 'ya') : 'jamaz',
    ('impotential', 'e') : 'eme',
    ('impotential', 'a') : 'ama',
    ('impotential', 'ye') : 'jeme',
    ('impotential', 'ya') : 'jama',
    ('potential', 'e') : 'ebil',
    ('potential', 'a') : 'abil',
    ('potential', 'ye') : 'jebil',
    ('potential', 'ya') : 'jabil',
    ('imperfective', 'ı') : 'ɯjoɾ',
    ('imperfective', 'i') : 'ijoɾ',
    ('imperfective', 'u') : 'ujoɾ',
    ('imperfective', 'ü') : 'yjoɾ',
    ('negative', 'cont') : 'joɾ',
    ('infinitive', 'e') : 'mek',
    ('infinitive', 'a') : 'mak',
    ('past', 'ı') : 'dɯ',
    ('past', 'i') : 'di',
    ('past', 'u') : 'du',
    ('past', 'ü') : 'dy',
    ('past', 'tı') : 'tɯ',
    ('past', 'ti') : 'ti',
    ('past', 'tu') : 'tu',
    ('past', 'tü') : 'ty',
    ('past', 'ydı') : 'jdɯ',
    ('past', 'ydi') : 'jdi',
    ('past', 'ydu') : 'jdu',
    ('past', 'ydü') : 'jdy',
    ('future', 'e') : 'edžek',
    ('future', 'a') : 'adžak',
    ('future', 'ja') : 'jadžak',
    ('future', 'ye') :'yedžek',
    ('future', 'ya') :'yadžak',
    ('inferential', 'perfective', 'ı') : 'mɯš',
    ('inferential', 'perfective',  'i') : 'miš',
    ('inferential', 'perfective',  'u') : 'muš',
    ('inferential', 'perfective',  'ü') : 'myš',
    ('inferential', 'perfective', 'yı') : 'jmɯš',
    ('inferential', 'perfective',  'yi') : 'jmiš',
    ('inferential', 'perfective',  'yu') : 'jmuš',
    ('inferential', 'perfective',  'yü') : 'jmyš',
    ('conditional', 'a') : 'sa',
    ('conditional', 'e') : 'se',
    ('conditional', 'jsa') : 'jsa',
    ('conditional', 'jse') : 'jse',
    ('progressive', 'a') : 'makta',
    ('progressive', 'e') : 'mekte',
    ('necessitative', 'a') : 'malɯ',
    ('necessitative', 'e') : 'meli'
}