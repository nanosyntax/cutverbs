paradigms = {
        ## formal versions not present

        # dělat
        "a": {
                ('1', 'sg'): "a:m",
                ('2', 'sg'): "a:š",
                ('3', 'sg'): "a:",

                ('1', 'pl'): "a:me",
                ('2', 'pl'): "a:te",
                ('3', 'pl'): "aji:"
        },

        # prosit
        "i": {
                ('1', 'sg'): "i:m",
                ('2', 'sg'): "i:š",
                ('3', 'sg'): "i:",

                ('1', 'pl'): "i:me",
                ('2', 'pl'): "i:te",
                ('3', 'pl'): "i:"
        },

        # dít_dějí
        "eji": {
                ('1', 'sg'): "i:m",
                ('2', 'sg'): "i:š",
                ('3', 'sg'): "i:",

                ('1', 'pl'): "i:me",
                ('2', 'pl'): "i:te",
                ('3', 'pl'): "ᐞeji:"
        },


        # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        # E suffix

        # asistovat asistuju
        "e_uj": {
                ('1', 'sg'): "uju",
                ('2', 'sg'): "uješ",
                ('3', 'sg'): "uje",

                ('1', 'pl'): "ujeme",
                ('2', 'pl'): "ujete",
                ('3', 'pl'): "ujou",
        },


        # blbnout
        "e_n": {
                ('1', 'sg'): "nu",
                ('2', 'sg'): "neš",
                ('3', 'sg'): "ne",

                ('1', 'pl'): "neme",
                ('2', 'pl'): "nete",
                ('3', 'pl'): "nou"
        },

        # nést
        "e": {
                ('1', 'sg'): "u",
                ('2', 'sg'): "eš",
                ('3', 'sg'): "e",

                ('1', 'pl'): "eme",
                ('2', 'pl'): "ete",
                ('3', 'pl'): "ou"
        },


        #### Irregulars

        # chtít
        "e_eji_chtít": {
                ('1', 'sg'): "i",
                ('2', 'sg'): "eš",
                ('3', 'sg'): "e",

                ('1', 'pl'): "eme",
                ('2', 'pl'): "ete",
                ('3', 'pl'): "ᐞeji:"
        },

}
