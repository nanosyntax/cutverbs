paradigms = {

# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
# I suffix

# # chcát chčiji
# "i_suffix_epen_formal": {
#            ('1', 'sg'): "i-j-i",
#            ('2', 'sg'): "iješ",
#            ('3', 'sg'): "ije",

#            ('1', 'pl'): "ijeme",
#            ('2', 'pl'): "ijete",
#            ('3', 'pl'): "iji:"
#    },

# # chcát chčiju
# "i_suffix_epen_informal": {
#             ('1', 'sg'): "iju",
#             ('2', 'sg'): "iješ",
#             ('3', 'sg'): "ije",

#             ('1', 'pl'): "ijeme",
#             ('2', 'pl'): "ijete",
#             ('3', 'pl'): "ijou"
#     },

## adjust for long-e -> i:
# dít_dějí
"i_suffix_3pl_eji": {
            ('1', 'sg'): "i:m",
            ('2', 'sg'): "i:š",
            ('3', 'sg'): "i:",

            ('1', 'pl'): "i:me",
            ('2', 'pl'): "i:te",
            ('3', 'pl'): "ᐞeji:"
    },

# prosit
"i": {
            ('1', 'sg'): "i:m",
            ('2', 'sg'): "i:š",
            ('3', 'sg'): "i:",

            ('1', 'pl'): "i:me",
            ('2', 'pl'): "i:te",
            ('3', 'pl'): "i:"
    },

# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
# E suffix

# asistovat asistuji
# "e_suffix_epen_formal": {
#         ('1', 'sg'): "uji",
#         ('2', 'sg'): "uješ",
#         ('3', 'sg'): "uje",

#         ('1', 'pl'): "ujeme",
#         ('2', 'pl'): "ujete",
#         ('3', 'pl'): "uji:"
#         'meta': {
#         'real_class': 'u_suffix_epen_formal'
#         }
#    },

# asistovat asistuju
"e_uj": {
        ('1', 'sg'): "uju",
        ('2', 'sg'): "uješ",
        ('3', 'sg'): "uje",

        ('1', 'pl'): "ujeme",
        ('2', 'pl'): "ujete",
        ('3', 'pl'): "ujou",
    },

# # přát přeji
# "e_suffix_epen_formal": {
#            ('1', 'sg'): "ᐞeji",
#            ('2', 'sg'): "ᐞeješ",
#            ('3', 'sg'): "ᐞeje",

#            ('1', 'pl'): "ᐞejeme",
#            ('2', 'pl'): "ᐞejete",
#            ('3', 'pl'): "ᐞeji:" # přej-i
#    },

# # přát přeju
#     "e_suffix_epen_informal": {
#             ('1', 'sg'): "ᐞeju",
#             ('2', 'sg'): "ᐞeješ",
#             ('3', 'sg'): "ᐞeje",

#             ('1', 'pl'): "ᐞejeme",
#             ('2', 'pl'): "ᐞejete",
#             ('3', 'pl'): "ᐞejou"        # přej-ou
#     },

# blbnout
    "e_n": {
            ('1', 'sg'): "nu",
            ('2', 'sg'): "neš",
            ('3', 'sg'): "ne",

            ('1', 'pl'): "neme",
            ('2', 'pl'): "nete",
            ('3', 'pl'): "nou"
    },

# nést
"e_suffix": {
        ('1', 'sg'): "u",
        ('2', 'sg'): "eš",
        ('3', 'sg'): "e",

        ('1', 'pl'): "eme",
        ('2', 'pl'): "ete",
        ('3', 'pl'): "ou"
},

## formal version not present

# chtít
    "e__eji_chtít": {
            ('1', 'sg'): "i",
            ('2', 'sg'): "eš",
            ('3', 'sg'): "e",

            ('1', 'pl'): "eme",
            ('2', 'pl'): "ete",
            ('3', 'pl'): "ᐞeji:"
    },

#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
# A suffix

# dělat
"a": {
            ('1', 'sg'): "a:m",
            ('2', 'sg'): "a:š",
            ('3', 'sg'): "a:",

            ('1', 'pl'): "a:me",
            ('2', 'pl'): "a:te",
            ('3', 'pl'): "aji:"
    },


}