paradigms = {

    "a_va": {
            ('1', 'sg'): "avam",
            ('2', 'sg'): "avaš",
            ('3', 'sg'): "ava",

            ('1', 'pl'): "avame",
            ('2', 'pl'): "avatje",
            ('3', 'pl'): "avaju:"
    },

    "a_sffx_length": {
            ('1', 'sg'): "^a:m",
            ('2', 'sg'): "^a:š",
            ('3', 'sg'): "^a:",

            ('1', 'pl'): "^a:me",
            ('2', 'pl'): "^a:tje",
            ('3', 'pl'): "aju:"
    },

    "a_root_length": {          # root or no length in case of -ja- diphthong
            ('1', 'sg'): "^am",
            ('2', 'sg'): "^aš",
            ('3', 'sg'): "^a",

            ('1', 'pl'): "^ame",
            ('2', 'pl'): "^atje",
            ('3', 'pl'): "aju:"
    },

    "i_sffx_length": {
            ('1', 'sg'): "^i:m",
            ('2', 'sg'): "^i:š",
            ('3', 'sg'): "^i:",

            ('1', 'pl'): "^i:me",
            ('2', 'pl'): "^i:tje",
            ('3', 'pl'): "ja"
    },

    "i_root_length": {
            ('1', 'sg'): "^im",
            ('2', 'sg'): "^iš",
            ('3', 'sg'): "^i",

            ('1', 'pl'): "^ime",
            ('2', 'pl'): "^itje",
            ('3', 'pl'): "ja"
    },

    "e_3pl_preserved": {
            ('1', 'sg'): "jem",
            ('2', 'sg'): "ješ",
            ('3', 'sg'): "je",

            ('1', 'pl'): "jeme",
            ('2', 'pl'): "jetje",
            ('3', 'pl'): "^eju:"
    },

    "e_uj": {
            ('1', 'sg'): "ujem",
            ('2', 'sg'): "uješ",
            ('3', 'sg'): "uje",

            ('1', 'pl'): "ujeme",
            ('2', 'pl'): "ujetje",
            ('3', 'pl'): "uju:"
    },

    "e_n_sffx_length": {
            ('1', 'sg'): "njem",
            ('2', 'sg'): "nješ",
            ('3', 'sg'): "nje",

            ('1', 'pl'): "njeme",
            ('2', 'pl'): "njetje",
            ('3', 'pl'): "nu:"
    },

    "e_n_root_length": {
            ('1', 'sg'): "njem",
            ('2', 'sg'): "nješ",
            ('3', 'sg'): "nje",

            ('1', 'pl'): "njeme",
            ('2', 'pl'): "njetje",
            ('3', 'pl'): "nu"
    },

    "e_no_length": {
            ('1', 'sg'): "^em",
            ('2', 'sg'): "^eš",
            ('3', 'sg'): "^e",

            ('1', 'pl'): "^eme",
            ('2', 'pl'): "^etje",
            ('3', 'pl'): "u:"
    },

    "e_root_length": {
            ('1', 'sg'): "^em",
            ('2', 'sg'): "^eš",
            ('3', 'sg'): "^e",

            ('1', 'pl'): "^eme",
            ('2', 'pl'): "^etje",
            ('3', 'pl'): "u"
    },

    "v_byť": {
            ('1', 'sg'): "om",
            ('2', 'sg'): "i",
            ('3', 'sg'): "e",

            ('1', 'pl'): "me",
            ('2', 'pl'): "tje",
            ('3', 'pl'): "u:"
    },

}
