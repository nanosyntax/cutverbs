# 28_03_2023

from suffixes.cz.verbs import paradigms

from scraper.prirucka import get_paradigm

from ipa.cz import paradigm_to_ipa

from suffixes.cz.verbs import paradigms

from guesser import guess_paradigm

from workspace.seed.seed_cz2 import get_czech_lemmas

# list_of_verbsA = get_czech_lemmas('a')
# print(list_of_verbsA)

# list_of_verbs1 = {'asistovat':'u_suffix_epen_informal', 'apelovat':'u_suffix_epen_informal', 'adresovat':'u_suffix_epen_informal', 'balit':'i_suffix','běsnit':'i_suffix'}
# list_of_verbs2 = {'blbnout':'e_suffix_n_suffix','bít':'i_suffix_epen_informal','blahopřát':'e_suffix_epen_informal','bodnout':'e_suffix_n_suffix', 'bulet':'i_suffix_3pl_eji'}
# list_of_verbs3 = {'bát':'i_suffix', 'brát':'e_suffix', 'bumbat':'a_suffix', 'brousit':'i_suffix', 'cákat':'a_suffix'}
# list_of_verbs4 = {'cálovat':'u_suffix_epen_informal', 'culit':'i_suffix', 'česat':'e_suffix','znamenat': 'a_suffix', 'chápat':'e_suffix'}
# list_of_verbs5 = {'civět':'v_dít', 'číst':'v_nést', 'čumět':'v_dít', 'couvat':'v_dělat', 'cvaknout':'v_blbnout'}
# list_of_verbs6 = {'ctít': 'i_suffix', 'dát':'a_suffix', 'darovat':'u_suffix_epen_informal', 'dít':'i_suffix_3pl_eji', 'dodat':'a_suffix'}
list_of_verbs7 = {"vědět":"i_suffix",'jíst':'i_suffix','hrozit':'i_suffix'}

# -- private

def belongs_to_paradigm(conjugated_verb, paradigm):
    roots = set()
    for features, verb_ipa in conjugated_verb.items():
        suffix_ipa = paradigm[features]
        if not verb_ipa.endswith(suffix_ipa):
            return(False)
        root = verb_ipa[:-len(suffix_ipa)]
        roots.add(root)
    return len(roots) == 1, roots
    # return(True)

for verb, expected_paradigm in list_of_verbs7.items():
    paradigma_z_prirucky = get_paradigm(verb)
    finite_paradigma_z_prirucky = paradigma_z_prirucky['finite']
    ipa_finite_paradigma_z_prirucky = paradigm_to_ipa(finite_paradigma_z_prirucky)
    a = belongs_to_paradigm(ipa_finite_paradigma_z_prirucky, paradigms[expected_paradigm])
    print(a, ipa_finite_paradigma_z_prirucky)

#for verb in list_of_verbs7.keys():
#    paradigma_z_prirucky = get_paradigm(verb)
#    finite_paradigma_z_prirucky = paradigma_z_prirucky['finite']
#    ipa_finite_paradigma_z_prirucky = paradigm_to_ipa(finite_paradigma_z_prirucky)
#    guessed_paradigm = guess_paradigm(ipa_finite_paradigma_z_prirucky,paradigms)
#    print(ipa_finite_paradigma_z_prirucky)
#    expected_paradigm = list_of_verbs7[verb]
#    if expected_paradigm != guessed_paradigm:
#        print(f'wrong result for {verb}, expected {expected_paradigm} vs. guessed {guessed_paradigm} ')
#    else:
#        print(f'okay for {verb}')
   









