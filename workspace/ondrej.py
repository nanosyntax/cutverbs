from suffixes.cz.verbs import paradigms

from scraper.prirucka import get_paradigm

from ipa.cz import paradigm_to_ipa

from suffixes.cz.verbs import paradigms

from guesser import guess_paradigm


ondrej_verbs_1 = {
    "objet": "e_suffix",
    "objevit": "i_suffix",
    "odkládat": "a_suffix"
}

ondrej_verbs_2 = {
    "odnést": "e_suffix",
    "opsat": "e_suffix"
}
    
ondrej_verbs_3 = {
    "otřást": "e_suffix",
    "otupět": "i_suffix_3pl_eji",
    "ozvat": "e_suffix"
}

ondrej_verbs_4 = {
    "pařit": "i_suffix",
    "počít": "e_suffix_n_suffix",
    "pást": "e_suffix",
    "péct": "e_suffix"
}
   
ondrej_verbs_5 = {
    "pitvat": "a_suffix",
    "píchat": "a_suffix",
    "planout": "e_suffix_n_suffix",
    "pobýt": "e_suffix"
}

ondrej_verbs_6 = {
    "podat": "a_suffix",
    "podvést": "e_suffix"
}


ondrej_verbs_no_working_with_new_function = {
    "odpovědět": "i_suffix", # problemem je "odpovjedi:" – je potreba ten kmen rozsirit v tyhle osobe, zavist neco jako vyjimku?
    "otupit": "i_suffix", # problem = 6. osoba "otupjeji:" - podobne jako u "odpovedet"
    "pomoct": "e_suffix", # "pomohu", ale "pomůžeš" – kdyby bylo "pomůžu" v 1. osobě (resp. v 6.), tak je po problému – řešit jako výjimku
    "opít": "i_suffix_epen_informal", # rikam "opiju", ne "opiji"
    "otázat": "i_suffix_epen_informal", # "otážu"
    "posrat": "e_suffix", # neni v prirucce
    "podělkovat": "i_suffix_epen_informal", # neni v prirucce
    "ožít": "i_suffix_epen_informal", # "ožiju"
    "pět": "i_suffix_epen_informal", # je homonymní s číslovnkou
    "obřezat": "a_suffix", # vůbec nevím proč to nejde
    "plavat": "e_suffix", # vůbec nevím proč to nejde 
    "odvážet": "i_suffix_3pl_eji", # ve 3pl chce "odváží", ale já mám "odvážejí"
}

for verb in ondrej_verbs_6.keys():
    paradigma_z_prirucky = get_paradigm(verb)
    finite_paradigma_z_prirucky = paradigma_z_prirucky['finite']
    ipa_finite_paradigma_z_prirucky = paradigm_to_ipa(finite_paradigma_z_prirucky)
    guessed_paradigm = guess_paradigm(ipa_finite_paradigma_z_prirucky,paradigms)
    print(ipa_finite_paradigma_z_prirucky)
    expected_paradigm = ondrej_verbs_6[verb]

    if expected_paradigm != guessed_paradigm:
        print(f'wrong result for {verb}, expected {expected_paradigm} vs. guessed {guessed_paradigm} ')
    else:
        print(f'okay for {verb}')

print("HOTOVO.")