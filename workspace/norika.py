from scraper.verbix_jp import get_paradigm, kuru_paradigm
from utils import flatten_paradigm
from parse import parse_all, all_nonempty_parses
from ipa.jp import paradigm_to_ipa
from suffixes.jp.verbs import suffixes

jp_suffix = list(suffixes.values())

test_verbs = ['あるく', 'たべる', 'くる', 'そうじする'] 

aruku_paradigm = get_paradigm(test_verbs[0])
flat_aruku = flatten_paradigm(aruku_paradigm)
flat_ipa_aruku = paradigm_to_ipa(flat_aruku)

taberu_paradigm = get_paradigm(test_verbs[1])
flat_taberu = flatten_paradigm(taberu_paradigm)
flat_ipa_taberu = paradigm_to_ipa(flat_taberu)
print(flat_ipa_taberu)

kuru_paradigm = kuru_parse
flat_kuru = flatten_paradigm(kuru_paradigm)
flat_ipa_kuru = paradigm_to_ipa(flat_kuru)
print(flat_ipa_kuru)

souzisuru_paradigm = get_paradigm(test_verbs[3])
flat_souzisuru = flatten_paradigm(souzisuru_paradigm)
flat_ipa_souzisuru = paradigm_to_ipa(flat_souzisuru)
print(flat_ipa_souzisuru)

for verb in flat_ipa_aruku.values():
    parsed_aruku = all_nonempty_parses(verb, jp_suffix)
    print(parsed_aruku)

for verb in flat_ipa_taberu.values():
    parsed_taberu = all_nonempty_parses(verb, jp_suffix)
    print(parsed_taberu) 

for verb in flat_ipa_kuru.values():
    parsed_kuru = all_nonempty_parses(verb, jp_suffix)
    print(parsed_kuru)

for verb in flat_ipa_souzisuru.values():
    parsed_souzi = all_nonempty_parses(verb, jp_suffix)
    print(parsed_souzi)

