from workspace.stats.sk_stats import sk_short_stats, sk_full_stats
from workspace.stats.cz_stats import cz_short_stats, cz_full_stats
from workspace.stats.ua_stats import ua_short_stats, ua_full_stats

def short_stats():
    cz_short_stats()
    ua_short_stats()
    sk_short_stats()

def full_stats():
    cz_full_stats()
    ua_full_stats()
    sk_full_stats()


if __name__ == '__main__':
    #short_stats()
    full_stats()