from scraper.dictcom_sk import DictComSk
from scraper.portalsnk  import PortalSnk
from scraper.sketch_paradigms.sktenten import SketchParadigmsSk
from os import listdir

from cutverbs.language import Language

def short_stats(data):  return stats(data, quiet = True)
def full_stats(data):   return stats(data, quiet = False)

def stats(data, quiet = False, return_data=False):
    sources = data['sources']
    lg_code = data['lg_code']
    list_all = data['list_all']

    raw   = {}
    clean = {}
    scrapers  = list(sources.values())
    clean_all = listdir(scrapers[0].dir_all) # roundabout way to get to the all/ dir
    list_all  = set(list_all)
    manual    = listdir(scrapers[0].dir_manual)

    # load raw & cleaned up data
    for name, scraper in sources.items():
        raw[name]   = set(listdir(scraper.html_dir))
        this = clean[name] = set(listdir(scraper.parsed_dir))
        # sanity check: cleaned up verbs of each scraper are also in all/
        diff = set.difference(this, clean_all)
        if len(diff) > 0:
            print(f' **** {len(diff)} verbs missing in all/ compared to {name}: {diff}')
            exit()

    raw_all      = set.union(*raw.values())
    clean_all    = set.union(*clean.values())
    clean_all    = set.union(clean_all, manual)
    known_lemmas = set.union(raw_all, list_all)
    if quiet:
        print_quiet(lg_code, known_lemmas, raw_all, clean_all, list_all)
    else:
        print_verbose(lg_code, known_lemmas, raw, clean, raw_all, clean_all, list_all)

    if return_data:
        return [lg_code, known_lemmas, raw, clean, raw_all, clean_all, list_all]


def print_quiet(lg_code, known_lemmas, raw_all, clean_all, list_all):
    print(f'\n{lg_code} knows {len(known_lemmas)} verbs')
    print(f'\t{len(raw_all)}\tare downloaded ({len(list_all.difference(raw_all))} not downloaded)')
    print(f"\t{len(clean_all)}\thave data for the full paradigm ({len(raw_all) - len(clean_all)} don't)")



def print_verbose(lg_code, known_lemmas, raw, clean, raw_all, clean_all, list_all):
    print(f'\n{lg_code} knows {len(known_lemmas)} verbs')
    print(f'\t{len(list_all)}\tin lists of lemmas')

    # list -> raw
    print(f'\n{len(raw_all)} are downloaded ({len(list_all.difference(raw_all))} not downloaded)')
    for name, r in raw.items():
        print(f'\t{len(r)}\tin {name}')

    # raw -> clean
    print(f"\n{len(clean_all)} have data for the full paradigm ({len(raw_all) - len(clean_all)} don't, {inv_percent(clean_all, raw_all)})")
    for name, c in clean.items():
        print(f'\t{len(c)}\tin {name}')
    # print(f'\t> {len(known_lemmas.difference(clean_all))}\tknown verbs are not available as paradigms')

    success, errs = assign_paradigms(lg_code, clean_all)

    print(f'\n{len(success)} were assigned a paradigm by guess_paradigm')
    print(f"\t{len(errs)} hence don't fit into our paradigms: ({ratio(errs, success)})")
    print(f'\n{", ".join(errs)}')
    print('\n-------------------')

def inv_percent(part, whole):   return spercent(100 - percent(len(part), len(whole)))
def ratio(part1, part2):        return spercent(percent(len(part1), len(part1)+len(part2)))
def spercent(p):                return f'{p}%'
def percent(part, whole):       return int(100 * float(part)/float(whole))

def assign_paradigms(lg_code, clean_all):
    lg = Language(lg_code)
    guesses = {lemma: lg.guess_stem_and_paradigm(lemma, 'v') for lemma in clean_all}
    success = {lemma: guess for lemma, guess in guesses.items() if guess[0]}
    errs    = [lemma for lemma, guess in guesses.items() if guess[0] is None]
    return success, errs




def stats_data():
    data_path = 'data/paradigms/svk/lemmas'
    sources = {
        'snk': PortalSnk(),
        'dictcom': DictComSk(),
        'sketch': SketchParadigmsSk(),
    }
    with open(f"{data_path}/sk_sketch_2023-05", "r", encoding='utf-8')  as f:   list_sketch_01 = f.read().split('\n')
    with open(f"{data_path}/wiktionary_svk.txt", "r", encoding='utf-8') as f:   list_wikt = f.read().split('\n')
    list_all = set(list_wikt); list_all.update(list_sketch_01)
    return {
        'lg_code': 'sk',
        'sources':  sources,
        'list_all': list_all,
    }

def get_wrongly_guesses_paradigms():
    data = stats_data()
    results = stats(data, quiet=True, return_data=True)
    lg_code = results[0]
    clean_all = results[5]
    success, errs = assign_paradigms(lg_code, clean_all)
    return errs

def sk_short_stats():
    short_stats(stats_data())

def sk_full_stats():
    full_stats(stats_data())

# print('TODO: how many are successfully categorized')
if __name__ == '__main__':
    data = stats_data()
    short_stats(data)
    full_stats(data)
    # short_stats(sources, lg_code, list_all)
    # stats(sources, lg_code, list_all)