from scraper.goroh  import Goroh
from scraper.sketch_paradigms.uatenten import SketchParadigmsUa
from workspace.stats.sk_stats import short_stats, full_stats

def ua_short_stats():
    short_stats(stats_data())

def ua_full_stats():
    full_stats(stats_data())

def stats_data():
    sources = {
    'goroh': Goroh(),
    'sketch': SketchParadigmsUa(),
    }

    with open(f"data/paradigms/ukr/wiktionary_ukr.txt", "r", encoding='utf-8') as f: list_all = f.read().split('\n')

    return {
        'lg_code': 'ua',
        'sources':  sources,
        'list_all': list_all,
    }


if __name__ == '__main__':
    ua_short_stats()