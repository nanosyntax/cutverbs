from scraper.prirucka  import Prirucka
from scraper.sketch_paradigms.cztenten import SketchParadigmsCz
from workspace.stats.sk_stats import short_stats, full_stats

def cz_short_stats():
    short_stats(stats_data())

def cz_full_stats():
    full_stats(stats_data())

def stats_data():
    sources = {
        'prirucka': Prirucka(),
        'sketch': SketchParadigmsCz(),
    }

    with open(f"data/paradigms/cze/wiktionary_cze.txt", "r", encoding='utf-8') as f: list_all = f.read().split('\n')

    return {
        'lg_code': 'cz',
        'sources':  sources,
        'list_all': list_all,
    }


if __name__ == '__main__':
    cz_full_stats()