
# c = ts 
# č = tš
# ch = x
# x = ks
# ř = rž
# y = i
# á = a:
# é = e:
# í = i:
# ý = i:
# ó = o:
# ú = u:
# ů = u:
# ě = je
# di = dji
# ti = tji
# ni = nji
# ď = dj
# ť = tj
# ň = nj
# mě = mnje

def ipa_replacement(w):
    ipa_word = word.replace('ch','x').replace('č','tš').replace('c','ts').replace('ř','rž').replace('y','i').replace('á','a:').replace('é','e:').replace('í','i:').replace('ý','i:').replace('ó','o:').replace('ú','u:').replace('ů','u:').replace('ě','je').replace('ď','dj').replace('ť','tj').replace('ň','nj').replace('di','dji').replace('ti','tji').replace('ni','nji').replace('mě','mnje')
    return(ipa_word)

word = input("Type a word: ")
to_ipa = ipa_replacement(word)
print(f'IPA form: {to_ipa}')