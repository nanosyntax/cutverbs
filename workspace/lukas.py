from suffixes.cz.verbs import paradigms

from scraper.prirucka import get_paradigm

from ipa.cz import paradigm_to_ipa

from suffixes.cz.verbs import paradigms

from guesser import guess_paradigm

from workspace.seed.seed_cz2 import get_czech_lemmas




list_of_verbs_1 = {
    'ladit':'i_suffix', 
    'lekat':'a_suffix', 
    'léčit':'i_suffix',
    'lepit':'i_suffix'
    }

list_of_verbs_2 = {
    'lajknout':'e_suffix_n_suffix',
    'líčit':'i_suffix',
    'lízat':'a_suffix',
    'lokat':'a_suffix'
}

list_of_verbs_3 = {
    'lenošit':'i_suffix',
    'lapit':'i_suffix',
    'lhát':'e_suffix',
    'lokat':'a_suffix',
    'líbat':'a_suffix'
}

list_of_verbs_4 = {
    'loupit':'i_suffix',
    'lesknout':'e_suffix_n_suffix',
    'líčit':'i_suffix',
    'línat':'a_suffix',
    'loupat':'a_suffix'
}

list_of_verbs_5 = {
    'nastat':'e_suffix_n_suffix',
    'nenávidět':'i_suffix',
    'nandat':'a_suffix',
    'natáčet':'i_suffix_3pl_eji',
    'nakrájet':'i_suffix_3pl_eji',
    'narážet':'i_suffix_3pl_eji'
}

list_of_verbs_6 = {
    'lít':'e_suffix_epen_informal',
    'lelkovat':'u_suffix_informal_ending',
    'lyžovat':'u_suffix_informal_ending',
    'lajkovat':'u_suffix_informal',
    'malovat':'u_suffix_informal',
    'naznačovat':'u_suffix_epen_informal',
    'lpět':'i_suffix_3pl_eji'
}


list_of_not_working_verbs = {
    # 'lít':'e_suffix_epen_informal',     
    'lelkovat':'u_suffix_epen_informal',  
    'lyžovat':'u_suffix_epen_informal',   
    'lajkovat':'u_suffix_epen_informal',         
    'malovat':'u_suffix_epen_informal',          
    'natáčet':'i_suffix', 	   
    'nakrájet':'i_suffix_3pl_eji',     
    'narážet':'i_suffix'       
}

list_of_verbs_7 = {
    'vidět','i_suffix',
}

for verb in list_of_not_working_verbs.keys():
    paradigma_z_prirucky = get_paradigm(verb)
    finite_paradigma_z_prirucky = paradigma_z_prirucky['finite']
    ipa_finite_paradigma_z_prirucky = paradigm_to_ipa(finite_paradigma_z_prirucky)
    guessed_paradigm = guess_paradigm(ipa_finite_paradigma_z_prirucky,paradigms)
    print(ipa_finite_paradigma_z_prirucky)
    expected_paradigm = list_of_not_working_verbs[verb]

    if expected_paradigm != guessed_paradigm:
        print(f'wrong result for {verb}, expected {expected_paradigm} vs. guessed {guessed_paradigm} ')
    else:
        print(f'okay for {verb}')


