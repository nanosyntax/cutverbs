from scraper.sketch_paradigms.cztenten import SketchParadigmsCz
from scraper.sketch_paradigms.sktenten import SketchParadigmsSk
from scraper.sketch_paradigms.uatenten import SketchParadigmsUa

from os import listdir

"""
Use sketchengine to download all present tense forms of the verbs scraped from wiktionary.
"""

def download_wiktionary_verbs(wik_path, fetcher, lemma_start = None):
    if lemma_start:
        ln = len(lemma_start)
    raw = listdir(fetcher.html_dir) # get the list once instead of looking for each file individually
    with open(wik_path, "r", encoding='utf-8') as wk:
        for lemma in wk:
            if lemma_start and len(lemma) > ln and lemma[:ln] != lemma_start:
                continue # optionally restrict the download to words starting with lemma_start
            lemma = lemma.rstrip('\n')
            if lemma in raw:
                continue
            print(f' -- handling {lemma}')
            fetcher.get_raw_data(lemma)

def download_cze_wiktionary_verbs(starting_with = None):
    fetcher = SketchParadigmsCz()
    return download_wiktionary_verbs("data/paradigms/cze/wiktionary_cze.txt", fetcher, starting_with)

def download_svk_wiktionary_verbs():
    fetcher = SketchParadigmsSk()
    return download_wiktionary_verbs("data/paradigms/svk/wiktionary_svk.txt", fetcher)

def download_ukr_wiktionary_verbs(starting_with = None):
    fetcher = SketchParadigmsUa()
    return download_wiktionary_verbs("data/paradigms/ukr/wiktionary_ukr.txt", fetcher, starting_with)


# Corpus info
# 
# https://app.sketchengine.eu/#dashboard?corpname=preloaded%2Fuktenten20_rft1
# https://www.sketchengine.eu/uktenten-ukrainian-corpus/
#   tagset: https://www.sketchengine.eu/multext-east-ukrainian-part-of-speech-tagset/
#   https://www.sketchengine.eu/tagsets/universal-pos-tags/
# https://www.sketchengine.eu/documentation/api-documentation/




if __name__ == '__main__':
    download_cze_wiktionary_verbs(starting_with = 't')

    from scraper.tags import tags_for_present
    fetcher = SketchParadigmsSk()
    tags = tags_for_present('Majka.sk') # 'SNK'
    resu  = fetcher.get_tag(tags[0])
    print(resu)

    # lemma = 'автоматизувати'
    # tag   = f'q[tag="k5eA.*mI.*{p}n{n}" & lemma="{lemma}"]'
    # tag   = 'q[tag="V..n.*"]'  # works
    # tag   = f'q[tag="V..n.*" & lemma="{lemma}"]'  # works
    # tag   = f'q[tag="V....*" & lemma="{lemma}"]'  # works
    # tag   = f'q[tag="V.*" & lemma="{lemma}"]'  # works
    # tag   = f'q[lemma="{lemma}"]'  # works
    # tag   = f'q[tag="V..ip3s*" & lemma="{lemma}"]'
    # resu  = fetcher.get_cql(tag)
    # tag   = f'q[tag="V..ip3p*" & lemma="{lemma}"]'
    # resu  = fetcher.get_cql(tag)
    # resu  = fetcher.get_tag("V..ip3s*")