from scraper.verbix_tk import get_paradigm
from parse import parse_all
from ipa.tk import paradigm_to_ipa
from suffixes.tk.verbs import suffixes
from utils import flatten_paradigm

hw_verbs = 'bulmak'
turkish_suffixes = list(suffixes.values())

bulmak_paradigm = get_paradigm('bulmak')
flat_bulmak = flatten_paradigm(bulmak_paradigm)
flat_ipa = paradigm_to_ipa(flat_bulmak)
print(flat_ipa)

for ipa_word in flat_ipa.values():
    parsed = parse_all(ipa_word, turkish_suffixes)
    print(f'  {ipa_word}  ->  {parsed}')