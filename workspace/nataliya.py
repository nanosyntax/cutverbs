from ipa.ua  import paradigm_to_ipa
#from guesser import guess_paradigm
from suffixes.ua.verbs import paradigms
from suffixes.ua.verbs import join_suffixes
from ipa.ua import paradigm_to_ipa
from scraper.goroh import get_paradigm
from workspace.seed.seed_ua import get_ukranian_lemmas

from cutverbs.language import Language

from workspace.wiktionary_fetcher import download_ukr_wiktionary_verbs
download_ukr_wiktionary_verbs()

# lemmas = get_ukranian_lemmas()
# print(lemmas)


paradigm_e = {
    ('1', 'sg'): 'б’ю',
    ('2', 'sg'): 'б’єш',
    ('3', 'sg'): 'б’є',
    ('1', 'pl'): 'б’ємо',
    ('2', 'pl'): 'б’єте',
    ('3', 'pl'): 'б’ють'
}
paradigm_e_v1 = {
    ('1', 'sg'): 'бю',      #delete <'> in paradigm_to_ipa function
    ('2', 'sg'): 'бєш',
    ('3', 'sg'): 'бє',
    ('1', 'pl'): 'бємо',
    ('2', 'pl'): 'бєте',
    ('3', 'pl'): 'бють'
}
# ua = Language('ua')
# ipa_paradigm_e = ua.paradigm_to_ipa(paradigm_e_v1)
# x = ua.guess_paradigm(ipa_paradigm_e)
# print(x)


ipa_paradigm_open_e = {
    ('1', 'sg'): 'pidpešú', 
    ('2', 'sg'): 'pidpéšɛš', 
    ('3', 'sg'): 'pidpéšɛ', 
    ('1', 'pl'): 'pidpéšɛmo', 
    ('2', 'pl'): 'pidpéšɛtɛ', 
    ('3', 'pl'): 'pidpéšutʲ'
}
"pidpeš'ɛš"
"pidpeš+u"