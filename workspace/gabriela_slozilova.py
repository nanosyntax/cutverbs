from cutverbs.language  import Language
from workspace.seed.seed_sk import sk_alphabet, get_slovak_lemmas_by_letter
from workspace.wiktionary_fetcher import download_svk_wiktionary_verbs
from workspace.wiktionary_fetcher import download_cze_wiktionary_verbs

# download_cze_wiktionary_verbs(starting_with = 'ú')
# download_svk_wiktionary_verbs()


# a function that takes the finite form of the verbs and turns it into IPA
def dictcom_dict_to_ipa(one_dictcom_paradigm, lg):
    return lg.paradigm_to_ipa(one_dictcom_paradigm['finite'])

# a function that guesses a paradigm of the IPA verbs and creates a dictionary of those
def verbs_belong_to_paradigm(lemmas, lg):
    ipa_paradigms = lg.batch_get_paradigms(lemmas)
    guessed_paradigms = {paradigm_name: sk.guess_paradigm(ipa_paradigm) for paradigm_name, ipa_paradigm in ipa_paradigms.items()}

    for paradigm_name, guessed_paradigm in guessed_paradigms.items():
        if not guessed_paradigm:   # test code whether it finds a paradigm from the suffixes
            print(f'There is no paradigm for the verb {paradigm_name}.')

    return guessed_paradigms


# guessing_paradigms_for_verbs = verbs_belong_to_paradigm(sketchengine_verbs, sk)
# sketchengine_verbs = get_slovak_lemmas()
# print(sketchengine_verbs)

# list_of_verbs = {
#     'volať': 'v_volať',
#     'robiť': 'v_robiť',
#     'trhať': 'v_volať',
#     'lepiť': 'v_robiť',
#     'písať': 'v_písať',
#     'tušiť': 'v_robiť',
#     'chápať': 'v_písať',
#     'rozumieť': 'v_rozumieť',
#     'chcieť': 'v_česať',
#     'voliť': 'v_robiť',
#     'bdieť': 'v_rozumieť',
#     'študovať': 'v_česať',
#     'trieť': 'v_česať',
#     'pracovať': 'v_česať',
#     'klesať': 'v_volať',
#     'ísť': 'v_česať'
# }

# list_of_verbs = {'rásť', 'riadiť', 'riešiť', 'diať', 'snažiť', 'stať', 'slúžiť', 'starať', 'spievať', 'strašiť', 'súťažiť', 'smiať', 'meniť', 'mrzieť', 'kryť', 'klásť', 'krútiť', 'kliať', 'konať', 'bieliť', 'lámať', 'lákať', 'liezť', 'ležať', 'leňošiť', 'lietať', 'loviť', 'počúvať', 'používať', 'tkať', 'triediť', 'trhať', 'tušiť', 'túliť', 'tešiť', 'trvať', 'tvoriť', 'tráviť', 'trúbiť', 'tuhnúť', 'tvrdiť', 'trápiť', 'trpieť', 'zrkadliť', 'vyrábať', 'čakať', 'čumieť', 'čeliť', 'číhať', 'čistiť', 'ňúrať', 'ňuchať', 'ťukať', 'žasnúť', 'žehnať', 'žiadať', 'šíriť', 'škriekať', 'šiť', 'štepiť', 'škodiť', 'šibať', 'šetriť', 'štverať', 'štekať', 'fúkať', 'fajčiť'}

list_of_verbs = {'ťažiť'}

# guessing_paradigms_for_verbs = verbs_belong_to_paradigm(list_of_verbs, sk)
# guessing_paradigms_for_verbs = verbs_belong_to_paradigm(sketchengine_verbs, sk)

# for surface, expected in zip(guessing_paradigms_for_verbs.items(), list_of_verbs.items()):
#     if surface != expected:
#         print(f'The verbs "{surface[0]}" does not belong to the paradigm "{expected[1]}"')

def get_many_sk_verbs_from_sketch():
     many_verbs_guessed = {}
     for letter in sk_alphabet:
         verbs_from_sketch = get_slovak_lemmas_by_letter(letter)
         sketch_ipa_paradigms = sk.batch_get_paradigms(verbs_from_sketch)
         for verb, sketch_ipa_paradigm in sketch_ipa_paradigms.items():
            many_verbs_guessed[verb] = sk.guess_paradigm(sketch_ipa_paradigm)
     return many_verbs_guessed

sk = Language('sk')

# get_verbs = get_many_sk_verbs_from_sketch()
# print(get_verbs)

def get_many_sk_verbs_from_sketch_no_paradigm():
     many_verbs_guessed = {}
    #  list_of_letters = ['r', 'd', 's', 'm', 'k']
     list_of_letters = ['ť']
     for letter in list_of_letters:
         verbs_from_sketch = get_slovak_lemmas_by_letter(letter)
         many_verbs_guessed[letter] = verbs_from_sketch
     return many_verbs_guessed

# get_infinitives = get_many_sk_verbs_from_sketch_no_paradigm()
# print(get_infinitives)
