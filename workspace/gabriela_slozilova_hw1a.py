
#  vědec = vjedets
#  klíče = kli:tše
#  šňůra = šnju:ra
#  ticho = tjixo
#  chyba = xiba
#  nejkulaťoulinkatější = nejkulatjoulinkatjejši:


word1 = "vědec".replace('ě','je').replace('c','ts')
word2 = "klíče".replace('í','i:').replace('č','tš')
word3 = "šňůra".replace('ň','nj').replace('ů','u:')
word4 = "ticho".replace('ti','tji').replace('ch','x')
word5 = "chyba".replace('ch','x').replace('y','i')
word6 = "nejkulaťoulinkatější".replace('ť','tj').replace('ě','je').replace('í','i:')
print(f'These words are in IPA: {word1}, {word2}, {word3}, {word4}, {word5}, {word6}')