# from guesser import guess_paradigm
# from ipa.cz import paradigm_to_ipa
# from suffixes.cz.verbs import paradigms
# from scraper.prirucka import get_paradigm

# my_verb = {  # change this to some other verb
#   ('1', 'sg'): 'blika:m',
#    ('2', 'sg'): 'blika:š',
#    ('3', 'sg'): 'blika:',
#    ('1', 'pl'): 'blika:me',
#    ('2', 'pl'): 'blika:te',
#    ('3', 'pl'): 'blikaji:'}

# prirucka_paradigm = get_paradigm("skákat")
# finite_paradigm = prirucka_paradigm['finite']
# prirucka_paradigm_ipa = paradigm_to_ipa(finite_paradigm)

# guessed = guess_paradigm(prirucka_paradigm_ipa, paradigms)
# if not guessed == paradigms['v_dělat']:
#    print(f"Wrong paradigm for {prirucka_paradigm_ipa}: {guessed}")
