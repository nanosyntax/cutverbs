from cutverbs.language import Language
from scraper.prirucka import Prirucka

"""
ua = Language('ua')
print(ua.paradigm_to_ipa({('1', 'sg'): 'постелю́', ('2', 'sg'): 'посте́леш', ('3', 'sg'): 'посте́ле', ('1', 'pl'): 'посте́лемо', ('2', 'pl'): 'посте́лете', ('3', 'pl'): 'посте́лють'}))
{'meta': {'category': 'v'}, 'infinitive': 'посла́ти', 'imperative': {('1', 'pl'): 'постелі́мо', ('2', 'sg'): 'постели́', ('2', 'pl'): 'постелі́ть'}, 'finite': {('1', 'sg'): 'постелю́', ('2', 'sg'): 'посте́леш', ('3', 'sg'): 'посте́ле', ('1', 'pl'): 'посте́лемо', ('2', 'pl'): 'посте́лете', ('3', 'pl'): 'посте́лють'}, 'participle': {('masc', 'sg'): 'посла́в', ('fem', 'sg'): 'посла́ла', ('neut', 'sg'): 'посла́ло', 'pl': 'посла́ли'}, 'impersonal': 'посла́но'}
"""

cz = Language('cz')
sk = Language('sk')

#print(cz.guess_stem_and_paradigm('číst'))

verbs = ['psát', ]
#cz.set_scraper(Prirucka())
#cz.get_surface_paradigm('číst')

print(sk.to_ipa('vedieť'))