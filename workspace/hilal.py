from scraper.verbix_tk import get_paradigm
from parse import parse_all
from ipa.tk import paradigm_to_ipa
from suffixes.tk.verbs import suffixes
from utils import flatten_paradigm

test_verb = 'almak'
turkish_suffixes = list(suffixes.values())

almak_paradigm = get_paradigm(test_verb)
flat_almak = flatten_paradigm(almak_paradigm)
flat_ipa = paradigm_to_ipa(flat_almak)
print(flat_ipa)

for ipa_word in flat_ipa.values():
    parsed = parse_all(ipa_word, turkish_suffixes)
    print(f'  {ipa_word}  ->  {parsed}')