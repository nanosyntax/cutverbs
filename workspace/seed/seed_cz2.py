from scraper.sketchengine import SketchEngine
import string
# import czech_sort

alphabet = list(string.ascii_lowercase)
cz_alphabet = alphabet + ['č', 'ch', 'ř', 'š', 'ž']
# cz_alphabet = czech_sort.sorted(cz_alphabet)
# I should think about which letters are relevant (might be better to remove letters like q, w etc.)

sketch = SketchEngine('preloaded/cstenten17_mj2', 'cze')
# 1sg = k5e.a.mIp1nS , inf = k5eAa.mF


def get_czech_lemmas(letter):
    verbs = sketch.get_lemmas_letter('k5eAa.mF', letter, 30)
    return verbs


# print(get_czech_lemmas('k'))
