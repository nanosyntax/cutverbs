from scraper.sketchengine import SketchEngine
sketch = SketchEngine('preloaded/uktenten20_rft1', 'ukr')


def get_ukranian_lemmas():
    verbs = sketch.get_lemmas('V..n.*', 30)
    return verbs


# print(verbs)
# what about verbs with ся -'боротися'?
# negative verb form: не заходити (do not enter) - separate token
