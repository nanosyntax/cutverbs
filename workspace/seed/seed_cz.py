from scraper.sketchengine import SketchEngine

sketch = SketchEngine('preloaded/cstenten17_mj2', 'cze')
verbs = sketch.get_lemmas('k5eAa.mF', 30)  # 1sg = k5e.a.mIp1nS , inf = k5eAa.mF

# take away ne- from verbs. Brute force for now
for v in verbs:
    if v.startswith('ne'):
        verbs.remove(v)
        verbs.add(v[2:])

print(verbs)
