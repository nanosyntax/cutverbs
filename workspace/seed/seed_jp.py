from scraper.sketchengine import SketchEngine
sketch = SketchEngine('preloaded/jptenten11_suw_comainu_v2')


def get_jp_verbs():
    verbs = sketch.get_jp_lemmas('V.g', 30)
    return verbs


print(get_jp_verbs())
