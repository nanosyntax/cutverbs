from scraper.sketchengine import SketchEngine
import string
sketch = SketchEngine('preloaded/sktenten_2', 'svk')

def filter_verbs_with_ne(verbs):
    # take away ne- from verbs. Brute force for now
    verbs = set(verbs)
    dont_remove = ['nechať', 'nechávať']
    for v in verbs:
        if v.startswith('ne') and v not in dont_remove:
            print('  ----  stripped "ne-" from ' + v)
            verbs.remove(v)
            verbs.add(v[2:])
    return verbs

def get_slovak_lemmas():
    verbs = sketch.get_lemmas('VI.*', 30)
    verbs = filter_verbs_with_ne(verbs)
    return verbs

alphabet = list(string.ascii_lowercase)
sk_alphabet = alphabet + ['č', 'ch', 'ď', 'dz', 'dž', 'ň', 'š', 'ť', 'ž']

def get_slovak_lemmas_by_letter(letter):
    verbs = sketch.get_lemmas_letter('VI.*', letter, 30)
    verbs = filter_verbs_with_ne(verbs)
    return verbs
