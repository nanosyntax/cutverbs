
The final semester task for Japanese is:

1. The Japanese tests mix phonological changes and morphological changes. We should keep them separate: the tests should reflect pure morphology, without any phonological adjustments. There should be a comment explaining the phonological change, if that that phonology cannot be coded in python yet. For instance:

    ('aruko:'): ['aru', 'ko',  ':'], #phono 

Should become something like:

    ('aruko:'): ['aru', 'ko'], # phono: ko lengthens in context XYZ

(replacing XYZ with the right context, maybe word-final in this case.)

You can group these changes in the verb_error dictionary, but each error must be well explained. (It currently isn't)


2. The irregular verb kuru is apparently ambiguous with a regular godan verb, making the web page difficult to parse. Ideally, we want a non ambiguous verb which is irregular, so that we get as close as possible to the 30 forms of the other verbs. If that's not possible, then you will need to write down the paradigm of irregular-kuru by hand into a python dictionary, and then use that to run your tests, so we get all forms testable.
