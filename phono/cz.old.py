## redundant copy of phono.cz.phono -- delete me after seminar, checking identity

def apply_phonology(left, right, features = None):
    left, right = palatalisation(left, right)
    left, right = j_epenthesis(left, right)
    return left + right

def j_epenthesis(left, right):
    vowels = 'aeiou'
    if left[-1] in vowels and right[0] in vowels:
        left = left + 'j'
    return left, right

def palatalisation(left, right):
    if not 'ᐞ' in right.diacritics:
        return left, right
    if left[-1] in 'pbv': # add more here?
        return left + 'j', right
    elif left[-1] == 'm':
        return left + 'nj', right
    return left, right

