## I-class: -i- TV causes a weak palatalisation
- evidence: root final consonants d, t, n are always soft (ď, ť, ň)
   - dj: vidji:m (vidjetj), sedji:m (sedjetj), budi:m (budjitj)
   - tj: fotji:m (fotjitj), štji:m (štjatj)
   - nj: bra:njim (bra:njitj), menji:m (menjitj)
- 's' is not palatalised into 'š' (examples nosi:m x tuši:m), similarly 'z' is not palasalised to 'ž'(vozi:m x slu:žim)
- minimal pairs:
   - sxš: musi:m, hla:sim x tuši:m, rje:šim
   - zxž: vozi:m, mrzi:m x beži:m, tu:žim, slu:žim
   - TODO: ?cxč: uči:m, li:čim (so far I did not find any verb with -c-i:m)
   - dzxdž: obmedzi:m x erdži:m, cvendži:m


# I-class: How to treat the 3pl?
- for verbs with root final consonants d, t, n, the consonants sound the same through the whole paradigm but the way the suffixes for the i-class are set now, it is creating two different things (from 1sg to 2pl there's the ^ diacritics, but for 3pl the suffix is -ja) 
- two options:
   - 1. to have a special sign for the consonants (ď, ť, ň) in IPA
   - 2. "pal d/t/n" is "d/t/n + ^"
   - for now we do not know the answer as we cannot say 'dj' is always 'ď' etc.

## A-class: notes
- so far, I have not find a verb with final root consonant 'z', maybe 'z'is always palatalised to 'dz'?
- minimal pairs:
   - rxrj: pozera:m, hra:m x merja:m, večerja:m
   - cxcj: utra:cam x vracja:m (vracatj)
   - vxvj: trva:m, hnjeva:m x stavja:m (stavatj)
   - dzxdzj: uva:dzam x zavadzja:m (zavadzatj)
   - šxšj: sku:šam x vešja:m (vešatj)

   - txtj: hlta:m, kokta:m, či:tam x pu:štjam (pu:štjatj)
   - nxnj: pozna:m, zelena:m x klanja:m, vonja:m, mi:njam
   - d: pada:m, zabu:dam, predvi:dam

## E-class: notes
- palatalisation
   - d>dz: vla:datj > vla:dzem
   - s>š: pi:satj > pi:šem, česatj > češem, 
   - z>ž: ka:zatj > ka:žem, li:zatj > li:žem, kl:zatj > kl:žem, rezatj > režem
   - t>ts: hltatj > hltsem, rehotatj > rehotsem, pratatj > pratsem
   - r>rj: bratj > berjem, pratj > perjem

   - l>lj: meljem x melu:
   - r>rj: perjem x peru:
   - z>zj: lezjem x lezu:
   - s>sj: njesjem x njesu:
   - d>dj: idjem x idu:
   - t>tj: rastjem x rastu:
   - n>nj: ženjem x ženu:
   - ž>žj: režem x strežjem


## Length of diphthongs
- sometimes the diphthongs ja/je are long and sometimes they are short, however the ortography is the same
- long:
   - ia: vracja:m (vraciam), stavja:m (staviam), vešja:m (vešiam), zavadzja:m (zavadziam)
   - ie: spje:vam (spievam), prebje:ham (prebieham), lje:čim (liečim), rje:šim (riešim)
- short:
   - ia: robja (robia), bavja (bavia), varja (varia)
   - ie: meljem (meliem), berjem (beriem)

## Problematic verbs
- palatalisation:
   - vrieť – OK, r + e > rje
   - orať – OK, r + e > rje
   - udrieť – OK, r + e > rje 
   - brať – OK, r + e > rje 
   - umrieť – OK, r + e > rje
   - zomrieť – OK, r + e > rje
   - srať – OK, r + e > rje
   - trieť – OK, r + e > rje
   - uzavrieť – OK, r + e > rje
   - grcať – OK, ts + a > tsja
   - niesť – OK, s + e > sje
   - uniesť – OK, s + e > sje
   - pásť – OK, s + e > sje
   - triasť – OK, s + e > sje
   - viezť – OK, z + e > zje
   - liezť – OK, z + e > zje
   - hrýzť – OK, z + e > zje
   - zavadzať – OK, dz + a > dzja
   - vracať – OK, c + a > cja
   - stavať – OK, v + a > vja
   - merať – OK, r + a > rja
- problems with downloaded forms
   - vravieť – problem with 1pl which is in sketchengine 'vravme' (imperative form)
   - vyhodnotiť – problem with 1pl which is in sketchengine 'vihodnotjtje' (imperative form)
   - naučiť
   - hltať – problem with 3sg form 'hltse/onhlta:'
   - predeliť – problem with 1sg predjelim
- irregular verbs
   - povedať
   - vedieť
   - jesť
   - byť
- 3pl preserved with ďj, tj, nj
   - bdieť

## Upd 2023-07-04
-  pal function update (+ pal tests)
   - context ž^e -> žje (strjeztj: 1sg strežjem)
   - context l^e -> lje (mljetj: 1sg meljem)
- manual paradigms
   - verbs with two paradigms (e.g. hltať – hltsem/hlta:m, kecať – keca:m/kecjam, koktať – koktsem/kota:m)
      - created a special dictionary for them
   - verbs wit wrongly downloaded paradigms form sketch engine (e.g. nautšitj, predjelitj, vravjetj, vihodnotjitj)
- went through verbs from statistics
   - 52 that do not have full paradigm:
      - either verbs that are not either in dictcom nor portalsnk (e.g. hypotetizovať, nadojčiť, nadabovať, barikádovať, alkalizovať)
      - verbs with wrong infinitive (e.g. venovať_pozornosť, dávať_pozor, spáchať_samovraždu, zavidieť)
      - verbs that are downloaded from dictcom/portalsnk or are manually added but apparently do not have full paradigm from sketch engine (e.g. cumľať, hltať, kecať, ligotať, šeptať, vravieť)
- two remaining problematic verbs (cumľať, drkotať) – two forms of gerund

Todo:
- check the kecať verb – does the glide cooccurs with length? 
- in progress: report the statistics issue for downloaded verbs that still appear as problematics
- drkotať/cumľať -> check the test whether it gives sth. or anything (if yes, it'll be an error)
- in progress:open an issue on czech uj verbs from sketch engine
- in progress: update the sketch engine ______ (for verbs with -ovat infinitive to turn 1sg and 3pl uji to uju, ují to ujou)
- Czech zulíbat, spát -> look into the html folder, maybe download it again from příručka, if it does not work, put into manual

## Upd 2023-07-11
- problem with downloading verbs from příručka
- added a function pre_paradigm_for (cz) – to solve cases where verbs ending with -ovat have standard forms of 1sg and 3pl