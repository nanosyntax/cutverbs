def apply_phonology(left, right = None, features = None):
    if right:
        return apply_edge_phono(left, right, features)
    else:
        return apply_internal_phono(left, features)

def apply_edge_phono(left, right, features = None): 
    left, right = palatalise(left, right)
    left, right = j_epenthesis(left, right)
    return left + right

def j_epenthesis(left, right):
    vowels = 'aeiou'
    if left[-1] in vowels and right[0] in vowels:
        left = left + 'j'
    return left, right

def palatalise(left, right):
    if not '^' in right.diacritics:
        return left, right
    if left[-1] in 'dtn':
        return left + 'j', right
    elif left[-1] in 'šr' and right[0] in 'ae': # tuši:m, vari:m no 'j' x vešja:m, berjem, večerja:m with 'j'
        return left + 'j', right
    elif left[-1] in 'szlž' and right[0] == 'e': # nosi:m, vozi:m no 'j' x njesjem, vezjem, meljem, strežjem with 'j'
        return left + 'j', right
    elif left[-2:] in ['ts', 'dz'] and right[0] == 'a': # hltsem, obmedzi:m, vla:dzem no 'j' x grtsjam, zavadzjam with 'j'
        return left + 'j', right
    elif left[-1] == 'v' and right[0] == 'a': # bavi:m, zvem no 'j' x stavjam with 'j'
        return left + 'j', right
    return left, right

def apply_internal_phono(word, features = None):
    return word
