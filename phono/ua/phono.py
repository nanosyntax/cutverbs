def apply_phonology(left, right = None, features = None):
    if right:
        return apply_edge_phono(left, right, features)
    else:
        return apply_internal_phono(left, features)

def apply_edge_phono(left, right, features = None):   
    left, right = palatalize_strongly(left, right)
    left, right = palatalize_weakly(left, right)
    left, right = j_epenthesis(left, right)
    return left + right

pal_correspondences = {
        't': 'č',
        'z': 'ž',
        's': 'š',
        'd': 'dž',  
}

def palatalize_weakly(left, right):
    if '^' not in right.diacritics:
        return left, right
    if left[-1] in 'rnl':
        return left + 'j', right
    if left[-1] in 'pbmv':  
        return left + 'lj', right
    if left[-1] in 'dtzs':
        return left +'j', right
    return left, right
 
def palatalize_strongly(left, right):
    if 'ᐞ' not in right.diacritics:
        return left, right
    if left[-1] in 'rnl':
        return left + 'j', right
    if left[-1] in 'pbmv':  
        return left + 'lj', right
    if left[-2:] == 'st':           
        return left[:-2] + 'šč', right
    if left[-2:] == 'zd':           #added new case
        return left[:-2] + 'ždž', right
    if left[-1] in 'dtzs':
        return left[:-1] + pal_correspondences[left[-1]], right 
    return left, right

def j_epenthesis(left, right): 
    vowels = 'aɛeiou'
    rhythm = '→←'
    left1  = left[-2] if left[-1] in rhythm else left[-1] # alternatively, keep a melody-only version of the string somewhere and use that for phono tests
    if len(right) == 0:
        right1 = ''
    elif right[0] in rhythm:
        right1 = right[1]  
    else:
        right1 = right[0]
    if left1 in vowels and right1 in vowels:
        left = left + 'j'
    return left, right

##original
# def j_epenthesis(left, right): 
#     vowels = 'aɛeiou'
#     rhythm = [':→←']
#     left1  = left[-2] if left[-1] in rhythm else left[-1] # alternatively, keep a melody-only version of the string somewhere and use that for phono tests
#     right1 = right[1] if right[0] in rhythm else right[0]
#     if left1 in vowels and right1 in vowels:
#         left = left + 'j'
#     return left, right

def apply_internal_phono(word, features = None):
    return word