### 3 steps to solve an issue:
# 1. understand why the verb is not cathegorized? (suppletion, stress placement, palatalization)
# 2. write a test with an ideal option outcome
# 3. write a code to make the test pass


## GOAL Fall 2023/Spring 2024
    čet a   te
pro čet a   te   
pro čet uva te

create association or link between the forms of imperf, perf and PI
Qs:
0. are there any webdics of this kind?


1. how we want to access data? 
i) starting from unprefixed form always
ii) put any form and find other forms associated with it

2. do we want to work on infinitive or finite forms?
i) infinitive: need a dic with possible sffxes before -te ending (cut -te ending)


# notes:
in Kovalev dic for ru:
there are following entries:
četate: link pročetate (perf.)
pročetate: link pročetuvate (imperf.) 
pročetuvate: link pročetate



## palatalization notes
- class II: no roots ending in velar (g, k, h);
- no verbs with root final vowel + j + pal diacritics (^ ᐞ);
- Class I: no verbs with root final vowel + j + ← (mixed stress); 
# IS IT TRUE FOR BIG DATA?
- 3pl sffx 'jatʲ' or pal+'atʲ'?
    linguistically: no evidence that the root is with PAL and the suffx is -jatj. Therefore, 3pl: root+PAL+atj




## Qs
- mixed stress: how many verbs? to see if there are many or few which differ in stress with my dialect


# 2023-07-19
## SOLVED BUT NOT CHECKED WITH MICHAL
1. -oro/olo verbs (see issue 1.8-1.9)
SOLUTION:treated as separate class and added sffxes for them to verbs
# UPD ! adding new set of sffxes created problems for reflexives_stressed_jɛ:
stavatesja matches this new set of sffxes. WHY? TODO!
new sffxes are commented  in verbs.py

2. 'date', 'jiste' added sffx for them 

3. to deal with 3sg є́ть in reflexives, added code to  



## TODO after the vacation break:
1. stress optionality
see tab 'ukr_verbs' for 9 verbs unsolved and for issue description
all 9 verbs are added to test.data.verbs_to_conjugation_classes
- for 7 verbs I applied my previous solution(added to manual 2 infinitives with diff stress);
- for 2 verbs there are no file in manual.

note:
- 5 verbs are from class II and have the same stress alternation between sffx stress and mixed stress
- other 4 verbs (vkraste=ukraste):
1 verb class II root str or mixed str
3 verbs (= or 1 verb? because have the same root but diff prefix) class I root str or mixed str
- ! all these verbs have one of the str options as mixed stress pattern 

SOLUTION#1:
adopt stress pattern on the basis of Pohribnyj 1959
Cons: 2 verbs class II (sffx or mixed) still have optionality even in Pohribnyj 1959. What to do with them?

SOLUTION#2: 
think how to access stress and added to sffxes the following patterns:
i. class II sffx and mixed str
ii. class II root and mixed str
iii. class I root and mixed str

2. 1 infinitive but 2 options for each person 
- see tab in 'ukr_verbs'
- 2 verbs not solved + 2 (=1? because the same root) in double_forms in test

- previous solution for 2(=1?) is to add file to manual with invented infinitive for one of two forms

New SOLUTION:
if we adopt Pohribnyj 1959 solution for stress we can also adopt it for this issue; 
Pohribnyj 1959 has only 1 form for each person for 2 unsolved verbs
TODO: check 'dexate'+ prefixed verb in Pohribnyj 1959


3. to download forms from goroh
'перевдягтися'
'бути'


4. see 1.7 issue


5. apostrophe, see 1.







## TODO:
accented vowels:
у́
е́
о́
а́
и́
ю́
я́
є́


# 1. apostrophe: separate a hard consonat from -ja, ju, jɛ, ji
delete <'> ? 
# in the root (after labial C):
в'язати     v'jazate
м'яти       m'jate
кип'ятити   kep'jatete

# at the edge of prefix final consonant and the root initial -ja, ju, jɛ, ji:
роз'єднати  roz'jɛdnate
з'їхати     z'jixate    C'jV
переїхати   pɛrɛjixate  VjV




## 1.PROBLEMATIC VERBS:
# 1.7. 
обходитися 
in goroh 2 lexemes with diff morphological structure and diff meaning. We get the first one , which I do not know but not the second one which I use

- in goroh 
    #1                          #2         
1sg obx'odlj    u           obx'odžu                   
2sg obx'od      eš
3sg obx'od      etj
1pl obx'od      emo
2pl obx'od      ete
3pl obx'odlj    atj         obx'odjatj





















### SOLVED ISSUES

accented vowels:
у́
е́
о́
а́
и́
ю́
я́
є́

# 1.4. stress issues:
зостава́тиСЯ:
2sg зостає́ш 
3sg зостає́ть 

# 
# 10/07/2023: added in manual entry where in 3sg cut -tj



# 1.9.
-oro / -olo verbs + 'slate' verb
borote
1sg borj    'u                             
2sg b'or    ɛš
3sg b'or    ɛ
1pl b'or    ɛmo
2pl b'or    ɛtɛ
3pl b'orj   utj 

class II:
1sg horj    'u
1pl hor     em'o

# 1.8.
послати: in goroh there are two entry for this infinitive; same stress in both
BUT: we downloaded only the first entry

1. posl'ate = postelete (to lay smth)
1sg postɛlj     'u                             
2sg post'ɛl     ɛš
3sg post'ɛl     ɛ
1pl post'ɛl     ɛmo
2pl post'ɛl     ɛtɛ
3pl post'ɛlj    utj 

2. posl'ate (to send)
1sg pošlj   'u                             
2sg pošl    'ɛš
3sg pošl    'ɛ
1pl pošl    ɛm'o
2pl pošl    ɛt'ɛ
3pl pošlj   'utj 

to download postelete as separate entry and for poslate add in manual only forms of 2 entry 

# 1.3. 2 optional forms 
дихати
1sg 'ди́хаю, ди́шу'
2sg 'ди́хаєш, ди́шеш'
3sg 'ди́хає, ди́ше'
1pl 'ди́хаємо' #cut forms from goroh: ди́хаєм, ди́шем, ди́шемо
2pl 'ди́хаєте, ди́шете'
3pl 'ди́хають, ди́шуть'

ду́ти
'дму, ду́ю'
'дмеш, ду́єш',
'дме, ду́є',
'дмемо́', #cut forms from goroh: дмем, ду́єм, ду́ємо
'дме́те, ду́єте', 
'дмуть, ду́ють

# OTHER VERBS added to manual:
надихати (+ invented надишати)


# 1.5. reflexives of both classes: 1pl sffx is -m not -mo
up to know noticed that this occur in reflexive verbs of both classes with 
root stress or mixed stress. 
Thus, in reflexives: if 1pl sffx -mo is not stressed --> -o is deleted 
поска́ржити
1sg поска́ржу
2sg поска́ржиш
3sg поска́ржить
1pl поска́ржим
2pl поска́ржите
3pl поска́ржать
? add to scraper.goroh after line 105
# SOLUTION: added a condition if after to_IPA function in file 'language' line 60-61


## 1.2. 2 options for the stress:
пояснити
1sg 'поясню́'
2sg 'поя́сни́ш'
3sg 'поя́сни́ть'
1pl 'поя́снимо́'
2pl 'поя́сните́'
3pl 'поя́сня́ть'
# SOLUTION: two entries in manual
# Other verbs:
поясни́ти for sffx stress + пояснити́ for mixed stress
вкра́сти root str + вкрасти́ mixed str
відобрази́ти sffx відобразити́ mixed
збагати́ти sffx збагатити́ mixed


## 1.6. no stress in monosyllabic forms
-notice that:
    for ɛ-class no stress in all forms except 1pl and 2pl;
    for e-class reflexive, no stress only in 1sg
# SOLUTION: add manually a stress to the copy file with all forms in data.manual


# 2. stress
- one root which is recognized by a program as two different roots because of the different stress placement

Ideas:
- mixed pattern is not the suppletion; it is predictable based on info to which pattern the verb belongs
ipa_paradigm_open_e = {
    ('1', 'sg'): 'pidpešú', 
    ('2', 'sg'): 'pidpéšɛš', 
    ('3', 'sg'): 'pidpéšɛ', 
    ('1', 'pl'): 'pidpéšɛmo', 
    ('2', 'pl'): 'pidpéšɛtɛ', 
    ('3', 'pl'): 'pidpéšutʲ'
}
"pidpeš'ɛš"
"pidpeš+u"
