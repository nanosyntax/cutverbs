def apply_phonology(left, right = None, features = None):
    if right:
        return apply_edge_phono(left, right, features)
    else:
        return apply_internal_phono(left, features)

def apply_internal_phono(word, features = None):
    return word

def apply_edge_phono(left, right, features = None):
    left, right = j_epenthesis(left, right)
    left, right = palatalize(left, right)
    return left + right

def j_epenthesis(left, right):
    vowels = 'aeiou'
    if left[-1] in vowels and right[0] in vowels:
        left = left + 'j'
    return left, right
 
def palatalize(left, right):
    # TODO: complete it!
    if 'ᐞ' not in right.diacritics:
        return left, right
    if (c := left[-1]) in 'bpv':
        return left + 'j', right
    elif c == 'm':
        return left + 'nj', right
    return left, right
