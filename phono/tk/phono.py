def apply_phonology(left, right, features):
    ipa = j_epenthesis(left, right) 
    return ipa



def j_epenthesis(left, right):
    # Hilal homework = the code below is wrong, make it work
    epenthesized = left + right
    return epenthesized


if __name__ == '__main__':
    test_data = {
        'sajdɯ':   ['sa',  'dɯ'],
        'landɯ:':  ['lan', 'dɯ'],  # if that's a bad example of lack of j-insertion, please change the example!
    }
    for expected, [left, right] in test_data.items():
        surface = j_epenthesis(left, right)
        if surface != expected:
            print(f'Wrong epenthesis. Got {surface} instead of {expected}')